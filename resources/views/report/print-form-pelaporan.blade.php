<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Laporan B1 Masyarakat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
</head>
<style type="text/css">
    .col-md-4 p{
        text-indent: 5%;
    }
</style>
<body>

<div class="form mt-5" id="printArea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('images/logo.png')}}" width="200px">
                
                <div class="float-right">
                
                @if(Request::segment(2) <> 'detail')
                    <a class="btn btn-danger 0 text-white" style="display:none;"  data-toggle="modal" data-target="#modalid" title="VERIFIKASI" id="verifikasi"><i class="fa fa-check"></i></a>
                @elseif(Request::segment(2) == 'detail' && $form->status == 'P')
                <a class="btn btn-danger 0 text-white" data-toggle="modal" data-target="#modalid" title="VERIFIKASI" id="verifikasi"><i class="fa fa-check"></i></a>
                @elseif(Request::segment(2) == 'detail' && ($form->status == 'T' || $form->status == 'F'))
                    <a class="btn btn-danger 0 text-white" style="display:none;"  data-toggle="modal" data-target="#modalid" title="VERIFIKASI" id="verifikasi"><i class="fa fa-check"></i></a>
                @endif
            
                    <button class="btn btn-info mb-0 mt-0  " id="printpagebutton" onclick="myFunction()" title="PRINT DOCUMENT"><i id="iconButton" class="fa fa-print"></i></button>
                </div>
            </div> 
            <!-- <div class="col-md-12 text-center mt-2 mb-0">
                <h2>Laporan Hasil Pengawasan Pemilu</h2>
                <p>Nomor : XX/LHP/PM.00.XX/klt/2019</p>
                <hr>
            </div>
            <div class="col-md-12 text-center mt-0 mb-2">
                <b>Laporan Hasil Pengawasan Pemilu</b>
            </div>   -->

            <div class="col-md-12">
            <br>
            <br>
            <p>No. Laporan : <strong>{{ strtoupper($form->no_laporan) }}</strong></p>
            <p>Dilaporkan pada : <strong>{{ date('d M Y H:i:s',strtotime($form->updated_at)) }}</strong></p>
                <h5>1. Data Pelapor :</h5>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            a. Nama Lengkap
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                           {{ $form->nama }} 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                           b. Nomor Identitas (KTP/Paspor/SIM) 
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                            {{ $form->no_identitas }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            c. Tempat/Tanggal Lahir
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->tempat_lahir.' ,'. date('d M Y',strtotime($form->tanggal_lahir)) }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            d. Jenis Kelamin
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                            @if($form->jk == 'L')
                                {{ 'Laki-laki' }}
                            @else 
                                {{ 'Perempuan' }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            e. Pekerjaan
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->pekerjaan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            f. Kewarganegaraan
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->kewarganegaraan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            g. Alamat
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->alamat }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            h. No Telpon/HP
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->no_telp }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            i. Fax**
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->fax }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            j. Email**
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->email }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <h5>2. Peristiwa Yang Dilaporkan :</h5>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            a. Peristiwa
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->peristiwa }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            b. Tempat Kejadian
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->tempat_kejadian }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            c. Waktu Kejadian
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->waktu_kejadian }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                           d. Hari dan Tanggal Diketahui 
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->hari }}  
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            e. Terlapor
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->terlapor }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            f. Alamat Terlapor***   
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->alamat_terlapor }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            g. No. Telpon/HP Terlapor
                        </p>
                    </div>
                    <div class="col-md-1 col-2 text-right">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->no_telp_terlapor }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5 mb-5">
                <h5>3. Saksi-Saksi</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                                <p>1.</p>
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Nama
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_nama_1 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Alamat
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_alamat_1 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    No. Telpon/HP
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_telp_1 }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                                <p>2.</p>
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Nama
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_nama_2 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Alamat
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_alamat_2 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    No. Telpon/HP
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_telp_2 }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                                <p>3.</p>
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Nama
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_nama_3 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    Alamat
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_alamat_3 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-3">
                                <p>
                                    No. Telpon/HP
                                </p>
                            </div>
                            <div class="col-md-1 col-2 text-right">
                                <p>:</p>
                            </div>
                            <div class="col-md-7 col-10">
                                <p>
                                {{ $form->saksi_telp_3 }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5 pt-5">
                <h5>4. Bukti-Bukti :</h5>
                <div class="row">
                    <div class="col-md-12">
                        a. {{ $form->bukti_1 }}
                    </div>
                    <div class="col-md-12">
                        b. {{ $form->bukti_2 }}
                    </div>
                    <div class="col-md-12">
                        c. {{ $form->bukti_3 }}
                    </div>
                    <div class="col-md-12">
                        d. {{ $form->bukti_4 }}
                    </div>
                    <div class="col-md-12">
                        e. {{ $form->bukti_5 }}
                    </div>
                   
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <h5>5. Uraian Singkat Kejadian :</h5>
                <div class="row">
                    <div class="col-md-12">
                        {{ $form->uraian }}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-2">
                                <p>
                                    Dilaporkan di
                                </p>
                            </div>
                            <div class="col-md-9 col-10">
                                <p>
                                    : {{ $form->dilaporkan_di }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-2">
                                <p>
                                    Hari dan Tanggal
                                </p>
                            </div>
                            <div class="col-md-9 col-10">
                                <p>
                                : {{ $form->dilaporkan_hari }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 col-2 text-right">
                            </div>
                            <div class="col-md-2">
                                <p>
                                    Waktu dan Jam
                                </p>
                            </div>
                            <div class="col-md-9 col-10">
                                <p>
                                : {{ $form->waktu_jam }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="lampiran">
                    <h5>V. Lampiran :</h5>
                    <div class="row">
                        <div class="col-md-7 col-10">
                            <p>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($lampiran as $p)
                                <a href="{{ asset('uploaded/masyarakat/laporan/'.$p->file_upload) }}" target="_blank">Lampiran {{ $no++ }}</a> <br>
                            @endforeach
                            </p>
                        </div>
                    </div>
                </div>

            <div class="col-md-12">
                <p>Saya menyatakan bahwa isi laporan ini adalah yang sebenar-benarnya dan saya bersedia mempertanggungjawabkannya di hadapan hukum.</p>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <p class="mt-0">Pelapor,</p>
                        <br><br>
                        <p>.....................................</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalid" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form method="post" action="{{ url('portal/b1/verifikasi') }}">
    @csrf
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Verifikasi Laporan</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="form-group">
                <label class="col-sm-4">No Laporan</label>
                <input type="hidden" class="form-control" value="{{ $form->users_id }}" name="users_id">
                <input type="text" class="form-control" name="no_laporan" readonly value="{{ $form->no_laporan }}">
            </div>
            <div class="form-group">
                <label class="col-sm-4">Email</label>
                 
                <input type="email" class="form-control" name="email" readonly value="{{ $form->email }}">
            </div>

            <div class="form-group">
                <label class="col-sm-4">Nama</label>
                 
                <input type="email" class="form-control" name="name" readonly value="{{ $form->name }}">
            </div>


            <div class="form-group">
                <label class="col-sm-4">Verifikasi </label>
                <select class="form-control" name="status">
                    <option value="T">Terima Laporan</option>
                    <option value="F">Tolak Laporan</option>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-4">Keterangan </label>
                <textarea class="form-control" name="keterangan"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </form>
  </div>
</div>

    <!-- jquery latest version -->
    <<script src="{{ asset('js/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script>
        function myFunction() {
            var printButton = document.getElementById("printpagebutton");
            var iconButton  = document.getElementById("iconButton");
            var lampiran    = document.getElementById("verifikasi");
            printButton.style.visibility = 'hidden';
            iconButton.style.visibility = 'hidden';
            lampiran.style.visibility = 'hidden';
            window.print();
            printButton.style.visibility = 'visible';
            iconButton.style.visibility = 'visible';
            lampiran.style.visibility = 'visible';
        }
    </script>
</body>

</html>
