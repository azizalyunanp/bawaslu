<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Laporan A Kecamatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
</head>
<style type="text/css">
    .col-md-4 p{
        text-indent: 5%;
    }

</style>
<body>

<div class="form mt-5" id="printArea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('images/logo.png')}}" width="200px">
                
                <div class="float-right">
                @if($form->status == 'P' && Request::segment(2)=='detail')
                     <a class="btn btn-danger 0  text-white" data-toggle="modal" data-target="#modalid" title="VERIFIKASI" id="lampiran"><i class="fa fa-check"></i></a>
                @endif


                    <button class="btn btn-info mb-0 mt-0  " id="printpagebutton" onclick="myFunction()" title="PRINT DOCUMENT"><i id="iconButton" class="fa fa-print"></i></button>
                </div>
            </div>
            <div class="col-md-12 text-center mt-2 mb-0">
                <h2>Laporan Hasil Pengawasan Pemilu</h2>
                <p>Nomor : {{ $form->no_laporan }}</p>
                <p>No Laporan : {{ $form->report_no }} </p>
                <hr>
            </div>
            <div class="col-md-12 text-center mt-0 mb-2">
                <b>Laporan Hasil Pengawasan Pemilu</b>
            </div>
            <div class="col-md-12">
                <h5>I. Data Pengawas Pemilihan :</h5>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            a. Tahapan Yang Diawasi          
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->tahapan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            b. Nama Pelaksana Tugas Pengawasan
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->nama_pelaksana}}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            c. Jabatan
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->jabatan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            d. Nomor Surat Perintah Tugas
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->no_surat }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            e. Alamat
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->alamat }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5>II. Kegiatan Pengawasan :</h5>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            a. Bentuk
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->bentuk }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            b. Tujuan
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->tujuan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            c. Sasaran
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->sasaran }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            d. Waktu dan Tempat
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->waktu }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5>III. Uraian Singkat Hasil Pengawasan :</h5>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            {{ $form->uraian_hasil }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5>IV. Informasi Dugaan Pelanggaran :</h5>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            1. Peristiwa
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->peristiwa }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            2. Saksi
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->saksi }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            3. Alat Bukti
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->alat_bukti }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            4. Barang Bukti
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->barang_bukti }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            5. Uraian Singkat Dugaan Pelanggaran
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->uraian_dugaan }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            6. Fakta dan Keterangan
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->fakta }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <p>
                            7. Analisa
                        </p>
                    </div>
                    <div class="col-md-1 text-right col-2">
                        <p>:</p>
                    </div>
                    <div class="col-md-7 col-10">
                        <p>
                        {{ $form->analisa }}
                        </p>
                    </div>
                </div>
                <div class="col-md-12" id="lampiran">
                    <h5>V. Lampiran :</h5>
                    <div class="row">
                        <div class="col-md-7 col-10">
                            <p>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($lampiran as $p)
                                <a href="{{ asset('uploaded/kecamatan/laporan/'.$p->file_upload) }}" target="_blank">Lampiran {{ $no++ }}</a> <br>
                            @endforeach
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" id="attachment">
                    <h5>VI. Lampiran (Surat Perintah Tugas) :</h5>
                    <div class="row">
                        <div class="col-md-7 col-10">
                            <p>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($attachment as $p)
                                <a href="{{ asset('uploaded/kecamatan/laporan/'.$p->file_upload) }}" target="_blank">Attachment {{ $no++ }}</a> <br>
                            @endforeach
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" id="attachment">
                    <h5>VII. Lampiran (Scan Hardcopy Form A Pengawasan) :</h5>
                    <div class="row">
                        <div class="col-md-7 col-10">
                            <p>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($scan as $p)
                                <a href="{{ asset('uploaded/kecamatan/laporan/'.$p->file_upload) }}" target="_blank">Scan {{ $no++ }}</a> <br>
                            @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <p class="mb-0">...............................................................</p>
                        <p class="mt-0">Pelaksana Tugas,</p>
                        <br><br>
                        <p>(...................................................)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalid" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form method="post" action="{{ url('portal/a/verifikasi') }}">
    @csrf
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Verifikasi Laporan</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-4">No Laporan</label>
                <input type="hidden" class="form-control" value="{{ $form->users_id }}" name="users_id">
                <input type="text" class="form-control" name="no_laporan" readonly value="{{ $form->no_laporan }}">
            </div>


            <div class="form-group">
                <label class="col-sm-4">Verifikasi </label>
                <select class="form-control" name="verifikasi">
                    <option value="T">Terima Laporan</option>
                    <option value="F">Tolak Laporan</option>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-4">Keterangan </label>
                <textarea class="form-control" name="keterangan"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </form>
  </div>
</div>

      <!-- jquery latest version -->
    <script src="{{ asset('js/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        function myFunction() {
            var printButton = document.getElementById("printpagebutton");
            var iconButton  = document.getElementById("iconButton");
            var lampiran    = document.getElementById("lampiran");
            printButton.style.visibility = 'hidden';
            iconButton.style.visibility = 'hidden';
            lampiran.style.visibility = 'hidden';
            window.print();
            printButton.style.visibility = 'visible';
            iconButton.style.visibility = 'visible';
            lampiran.style.visibility = 'visible';
        }
    </script>
</body>

</html>
