@extends('layouts.app_kec')
@section('content')

 <div class="main-content-inner">
    <div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row">
                <!-- Textual inputs start -->
                <div class="col-12 mt-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">{{ $breadcrumb }}  </h4>                                    
                            <b class="text-muted mb-3 mt-4 d-block">Filter Berdasarkan:</b>
                                <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="cR1" name="theName" class="custom-control-input" checked="">
                                        <label class="custom-control-label" for="cR1">Tanggal</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio"   id="cR2"  name="theName"  class="custom-control-input"  >
                                        <label class="custom-control-label" for="cR2">No Laporan</label>
                                </div>
                            
                                <br>

                                <div id="form-1">
                                    <form method="post" action="{{ URL('kecamatan/rekap-tanggal') }}">
                                    @csrf
                                    <div class="form-row align-items-center">
                                        <div class="col-sm-3 my-1">
                                            <label class="sr-only" for="inlineFormInputName">From</label>
                                            <input type="date" class="form-control"  name="form">
                                        </div>
                                        <div class="col-sm-3 my-1">
                                            <label class="sr-only" for="inlineFormInputGroupUsername">To</label>
                                            <div class="input-group">
                                            <input type="date" class="form-control" name="to">
                                            </div>
                                        </div>

                                        <div class="col-auto my-1">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>


                                <div id="form-2">
                                <form method="post" action="{{ URL('kecamatan/rekap-nolaporan') }}">
                                    @csrf
                                    <div class="form-row align-items-center">
                                        <div class="col-sm-3 my-1">
                                            <label class="sr-only" for="inlineFormInputName">No Laporan</label>
                                            <input type="text" class="form-control"  name="no_laporan" placeholder="Cari No Laporan">
                                        </div>


                                        <div class="col-auto my-1">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>

                      
                            <br><br>


                            
                            <div class="single-table">
                                <div class="table-responsive">
                                    <table class="table text-center">
                                        <thead class="text-uppercase bg-primary">
                                            <tr class="text-white">
                                                <th scope="col">No</th>
                                                <th scope="col">NO Laporan</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Tangggal Upload</th>
                                                <th scope="col">Lihat Detail</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        @php 
                                            $no = 1;
                                        @endphp
                                        @foreach($form as $p)
                                       
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{!! $p->no_laporan !!} </td>
                                                <td>{!! $p->status !!} </td>
                                                <td>{!! date('d M Y',strtotime($p->updated_at)) !!}  </td>
                                                <td><a class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table> <br>

                                    {{ $form->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Textual inputs end -->
            </div>
        </div>
    </div>
</div>
<!-- main content area end -->

 
@endsection