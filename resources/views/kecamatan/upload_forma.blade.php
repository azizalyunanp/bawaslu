@extends('layouts.app_kec')
@section('content')
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if(session('notifikasi')) 
                                        <div class="alert alert-success"> {{ session('notifikasi') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <form method="post" action="{{ URL('kecamatan/ajukan-laporan') }}" enctype="multipart/form-data" id="myform">
                                        @csrf
                                        <h2>Form Laporan Hasil Pengawasan Pemilihan Umum</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label for="" class="col-form-label">No Laporan </label>
                                                    <input type="text" name="report_no" class="form-control" >
                                            </div>
                                            </div>
                                        </div>

                                        <h4 class="header-title mb-0 mt-3">I. Data Pengawas Pemilihan</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang Data Pengawas Pemilihan</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Tahapan Yang Diawasi</label>
                                                    <input type="hidden" name="no_laporan" value="{{ $no_laporan }}">
                                                    <input class="form-control" type="text" required name="tahapan" id="tahapan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Nama Pelaksana Tugas Pengawasan</label>
                                                    <input class="form-control" type="text" required name="nama_pelaksana" id="nama_pelaksana">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Jabatan</label>
                                                    <input class="form-control" type="text" required name="jabatan" id="jabatan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Nomor Surat Perintah Tugas</label>
                                                    <input class="form-control" type="text" required name="no_surat" id="no_surat">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Alamat</label>
                                                    <input class="form-control" type="text" required name="alamat" id="alamat">
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">II. Kegiatan Pengawasan</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang Data Kegiatan Pengawas</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Bentuk</label>
                                                    <input class="form-control" type="text" required name="bentuk" id="bentuk">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Tujuan</label>
                                                    <input class="form-control" type="text" required name="tujuan" id="tujuan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Sasaran</label>
                                                    <input class="form-control" type="text" required name="sasaran" id="sasaran">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Waktu dan Tempat</label>
                                                    <input class="form-control" type="date" name="waktu" id="waktu">
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-2 mt-3">III. Uraian Singkat Hasil Pengawasan</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" aria-label="" name="uraian_hasil" id="uraian_hasil"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">IV. Informasi Dugaan Pelanggaran</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang informasi dugaan pelanggaran</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Peristiwa</label>
                                                    <input class="form-control" type="text" required name="peristiwa" id="peristiwa">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Saksi</label>
                                                    <input class="form-control" type="text" required name="saksi" id="saksi">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Alat Bukti</label>
                                                    <input class="form-control" type="text" required name="alat_bukti" id="alat_bukti">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Barang Bukti</label>
                                                    <input class="form-control" type="text" required name="barang_bukti" id="barang_bukti">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Uraian Singkat Dugaan Pelanggaran</label>
                                                    <textarea class="form-control" name="uraian_dugaan" id="uraian_dugaan"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Fakta dan Keterangan</label>
                                                    <textarea class="form-control" aria-label="" name="fakta" id="fakta"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Analisis</label>
                                                    <textarea class="form-control" aria-label="" name="analisa" id="analisa"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">V. Lampiran</h4>
                                        <p class="text-muted font-14 mb-2">Silahkan unggah file lampiran</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input class="form-control" multiple type="file" accept="image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/x-rar-compressed,
            application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.presentationml.presentation" name="file_upload[]"  required>
                                                </div>
                                            </div>
                                        </div>

                                        <h4 class="header-title mb-0 mt-3">VI. Attachment (seperti Surat Perintah Tugas *.pdf atau *.word)</h4>
                                        <p class="text-muted font-14 mb-2">Silahkan unggah file lampiran</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input class="form-control" multiple type="file" accept="application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document" name="attachment[]"   required>
                                                </div>
                                            </div>
                                        </div>


                                        <h4 class="header-title mb-0 mt-3">VII. Lampiran (seperti SCAN HARDCOPY FORM A PENGAWASAN *.pdf)</h4>
                                        <p class="text-muted font-14 mb-2">Silahkan unggah file lampiran</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input class="form-control" multiple type="file" accept="application/pdf" name="scan[]"   required>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-xl mb-3 float-right bg-primary-color">Simpan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                        </div>
                    </div>
                </div>
            </div>
        
@endsection