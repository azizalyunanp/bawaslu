@extends('layouts.app_kec')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-lg-12 col-sm-12 mt-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4>Daftar Riwayat Laporan Terakhir</h4>
                                        <div class="table-responsive"> 
                                            <table class="table">
                                                <thead class="bg-primary-color">
                                                    <tr class="text-white">
                                                        <td>No</td>
                                                        <td>No Laporan</td>
                                                        <td>Tanggal Lapor</td>
                                                        <td>Status</td>
                                                        <td>Aksi</td>                                                
                                                    </tr>
                                                </thead>
                                            
                                                <tbody>
                                                @php
                                                $no=1;
                                                @endphp

                                                @foreach($form as $p)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $p->no_laporan }}</td>
                                                        <td>{{ date('d M Y',strtotime($p->updated_at)) }}</td>
                                                        <td>
                                                            @if($p->status == 'T')
                                                            <span class="badge badge-pill badge-success">Diterima</span>
                                                            @elseif($p->status == 'P')
                                                            <span class="badge badge-pill badge-warning">Pending</span>
                                                            @else
                                                            <span class="badge badge-pill badge-danger">Ditolak</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('kecamatan/report/'.$p->no_laporan) }}"  target="_blank" class="btn btn-primary btn-sm bg-primary-color text-white"><i class="fa fa-eye text-white"></i> Detail
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table> 
                                        </div>
                                        {{ $form->links() }}
                                    </div>
                                </div>
                            </div>                                                                                        
                    </div>
            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection