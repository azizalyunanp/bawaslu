@extends('layouts.app_kec')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
  
                                        <br><br>
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">Nama Materi</th>
                                                            <th scope="col">Tgl Upload</th>
                                                            <th scope="col">Opsi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                    @foreach($materi as $p)
                                                    
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->nama !!}</td>
                                                            <td>{!! date('d M Y H:i:s',strtotime($p->updated_at)) !!} </td>
                                                            <td>
                                                            <a href="{{ url('kecamatan/materi/'.$p->id) }}" target="_blank" class="btn bg-primary-color text-white"><i class="fa fa-eye "></i> </a>
                                                            <a href="{{ asset('uploaded/admin/materi/'.$p->file_upload) }}" target="_blank" class="btn btn-primary"><i class="fa fa-download "></i> </a> </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>                                                
                                                </table> <br><br>

                                                {{ $materi->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection