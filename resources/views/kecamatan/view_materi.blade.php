@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($materi as $p)
                                    <form method="post"  action="{{ route('portal.materi.update',$p->id) }}" enctype="multipart/form-data">  
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Judul Materi</label>
                                            <p>{!! $p->nama !!}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Deskripsi</label>
                                            <p>{!! $p->deskripsi !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-sm-5 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Preview Pdf</label> <br>
                                            <a href="{{ asset('uploaded/admin/materi/'.$p->file_upload) }}" target="_blank" classn="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Preview PDF</a>
                                        </div>
                                        <div class="form-group">
                                    </div>
                                </div>
                            
                            </div>

                            </form>
                            @endforeach
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection