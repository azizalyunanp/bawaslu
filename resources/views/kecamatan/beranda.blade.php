@extends('layouts.app_kec')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-lg-8 col-sm-12 mt-12 ">
                                <div class="card">
                                    <div class="card-body">

                                        <h4>List Laporan Terakhir</h4>
                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead class="bg-primary-color">
                                                    <tr class="text-white">
                                                        <td>No</td>
                                                        <td>No Laporan</td>
                                                        <td>Status</td>
                                                        <td>Detail</td>                                                
                                                    </tr>
                                                </thead>
                                            
                                                <tbody>
                                                @php 
                                                $no = 1;
                                                @endphp
                                                @foreach($laporan as $lap)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $lap->no_laporan }}</td>
                                                        <td>
                                                            @if($lap->status == 'T')
                                                            <span class="badge badge-pill badge-success">Diterima</span>
                                                            @elseif($lap->status == 'P')
                                                            <span class="badge badge-pill badge-warning">Pending</span>
                                                            @else
                                                            <span class="badge badge-pill badge-danger">Ditolak</span>
                                                            @endif
                                                        </td>
                                                        <td><a href="{{ url('kecamatan/report/'.$lap->no_laporan) }}" class="btn btn-primary" target="_blank"><i class="fa fa-eye"></i> Lihat</td>
                                                
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table> 
                                        </div>
                                        <br>
                                        <br>

 
                                    </div>
                                </div>

                                <br><br>
                                <div class="card">
                                    <div class="card-body">
                                    <h4>Materi</h4>
                                    <div class="table-responsive">
                                        <table class="table bg-primary-color">
                                            <thead class="bg-primary-color">
                                                <tr class="text-white">
                                                    <th scope="col">No</th>
                                                    <th scope="col">Nama Materi</th>
                                                    <th scope="col">Tgl Upload</th>
                                                    <th scope="col">Unduh</th>   
                                                                                
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                            @php 
                                            $no = 1;
                                            @endphp
                                            @foreach($materi as $p)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{!! $p->nama !!}</td>
                                                    <td>{!! date('d M Y H:i:s',strtotime($p->updated_at)) !!} </td>
                                                    <td>
                                                    <a href="{{ url('kecamatan/materi/'.$p->id) }}" target="_blank" class="btn bg-primary-color text-white"><i class="fa fa-eye "></i> </a>
                                                    <a href="{{ asset('uploaded/admin/materi/'.$p->file_upload) }}" target="_blank" class="btn btn-primary"><i class="fa fa-download "></i> 
                                                    </a> </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                            
                            
                                
                            <div class="col-sm-4 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @foreach($users as $p)
                                            <div class="form-group">
                                                <label for="example-search-input" class="col-form-label"></strong>Foto Profil</strong></label> <br>
                                                <img src="{{ asset('uploaded/kecamatan/profile/'.$p->images) }}" width="300">
                                            </div>
                                   
                                        <h4 class="header-title"> User Kecamatan </h4>
                                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label"><strong>Nip</strong></label>
                                            <p> {{ $p->nip }}</p>
                                             
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label"><strong>Nama</strong></label>
                                            <p>{{ $p->name }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label"><strong>Email</strong></label>
                                            <p>{{ $p->email }}</p>
                            
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection