@extends('layouts.app_kec')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if(session('notifikasi'))
                                            <div class="alert alert-{{ session('alert') }}">
                                            
                                                {{ session('notifikasi') }}
                                            </div>
                                        @endif

                                        
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif
                                        <h4>Kelola Data User</h4>
                                        <form method="post" action="{{ URL('kecamatan/update') }}" enctype="multipart/form-data">
                                        @csrf
                                         
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Nama Lengkap</label>
                                                        <input class="form-control" value="{{ $users->name }}" type="text" name="nama">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Foto Profil</label> <br>
                                                        <img src="{{ asset('uploaded/kecamatan/profile/'.$users->images) }}" width="200"> <br><br>
                                                        <input class="form-control"  type="file" name="photo">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn bg-primary-color text-white float-right" type="submit">Simpan</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-5 mt-12">
                            <div class="card">
                                    <div class="card-body">
                                        
                                        <h4>Kelola Password</h4>
                                        <form method="post" action="{{ URL('kecamatan/ganti-password/'.Auth::guard('kecamatan')->user()->id) }}">
                                        @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Password Lama</label>
                                                        <input class="form-control" type="password" name="password_old">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Password Baru</label>
                                                        <input class="form-control" type="password" name="password">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Konfirmasi Password</label>
                                                        <input class="form-control" type="password" name="password_confirmation">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn bg-primary-color text-white float-right" type="submit">Perbarui</button>
                                        </form>
                                            

                                    </div>
                                </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection