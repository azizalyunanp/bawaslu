@extends('layouts.app_kec')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-12">
                                <div class="card">
                                    <div class="card-body">
 

                                    @foreach($materi as $p)
                                    <form>  
                                    @csrf
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Judul Materi</label>
                                            <p>{!! $p->nama !!}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Deskripsi</label>
                                            <p>{!! $p->deskripsi !!}</p>
                                        </div>

                                    
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-sm-5 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">File Preview</label> <br>
                                            <a href="{{ asset('uploaded/admin/materi/'.$p->file_upload) }}" target="_blank" class="btn btn-warning"><i class="fa fa-download"></i>Unduh File</a>
                                        </div>
 
                                    </div>
                                </div>
                            
                            </div>

                            </form>
                            @endforeach
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection