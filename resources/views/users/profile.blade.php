@extends('layouts.app_user')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-lg-7 col-sm-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if(session('notifikasi'))
                                            <div class="alert alert-{{ session('alert') }}">
                                            
                                                {{ session('notifikasi') }}
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif
                                        
                                        <h4>Kelola Data User</h4>
                                        <form method="post" action="{{ url('user/update') }}" enctype="multipart/form-data" id="myform">
                                        @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Nama Lengkap</label>
                                                        <input class="form-control" type="text" name="nama" value="{{ $users->name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="" class="col-form-label">Foto Profil</label> <br>
                                                        <img src="{{ asset('uploaded/masyarakat/foto/'.$users->images) }}" width="200"> <br><br>
                                                        <input class="form-control"  type="file" name="photo" accept="images/*">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn bg-primary-color text-white float-right" type="submit">Simpan</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            
                            
                                
                            <div class="col-lg-5 col-sm-12 mt-12">
                            <div class="card">
                                    <div class="card-body">
                                        
                                        <h4>Kelola Password</h4>
                                        <form method="post" action="{{ URL('user/ganti-password/'.Auth::guard('masyarakat')->user()->id) }}">
                                        @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Password Lama</label>
                                                        <input class="form-control" type="password" name="password_old">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Password Baru</label>
                                                        <input class="form-control" type="password" name="password">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Konfirmasi Password</label>
                                                        <input class="form-control" type="password" name="password_confirmation">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn bg-primary-color text-white float-right" type="submit">Perbarui</button>
                                        </form>
                                            

                                    </div>
                                </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection