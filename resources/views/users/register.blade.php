<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pendaftaran User</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/icon/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default-css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    
    <script src="{{ asset('jsfile/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('jsfile/vendor/modernizr-2.8.3.min.js') }}"></script>
    <!-- JQUERY VALIDATE -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    <!-- JQUERY VALIDATE -->

    <noscript>
        <meta http-equiv='refresh' content="0;/noscript">
    
    </noscript>
</head>

<body>
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100">

                        <form method="POST" action='{{ url("register/user") }}'  enctype="multipart/form-data" id="registerform">
                        @csrf
                    <div class="login-form-head bg-primary-color">
                        <img src="{{ asset('images/logo.png') }}" alt="logo"></a>
                        <!-- <h4>Masuk</h4> -->
                    </div>
                    <div class="login-form-body ">
                        <div class="row">
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible fade show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                    </button>
                                </div>
                            @endif <br>

                            @if(session('notifikasi'))
                                <div class="alert alert-danger alert-dismissible fade show">
                                        {{ session('notifikasi')}}

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                        </button>
                                    </div>
                            @endif

                            <div class="col-md-12">
                                <div class="form-gp">
                                    <label for="">NO KTP</label>
                                    <input type="text"  class="form-control" name="nik"  required>
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-gp">
                                    <label for="">Nama Lengkap</label>
                                    <input type="text"  class="form-control" name="nama"  required>
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-gp">
                                    <label for="">Email address</label>
                                    <input type="email"  class="form-control" name="email"  required >
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-gp">
                                    <label for="">Kata Sandi</label>
                                    <input type="password"  class="form-control" name="password"  required s>
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-gp">
                                    <label for="">Konfirmasi Kata Sandi</label>
                                    <input type="password"  class="form-control" name="password_confirmation"  required>
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-gp">
                                    <label style="z-index:1;">Foto User</label>
                                    <br>
                                    <input type="file"  class="form-control col-sm-12" name="photo"  required autofocus>
                                    <i class="ti-email"></i>
                                </div>
                            </div>
                        </div>
                        <div class="submit-btn-area">
                            <button  type="submit" class="btn btn-danger">Daftar <i class="ti-arrow-right"></i></button>
                        </div>
                        <div class="form-footer text-center mt-5">
                            <p class="text-muted"> <a href="{{ url('masyarakat') }}">Masuk</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

   <!-- jquery latest version -->
   <script src="{{ asset('jsfile/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('jsfile/popper.min.js') }}"></script>
    <script src="{{ asset('jsfile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('jsfile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('jsfile/metisMenu.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slicknav.min.js') }}"></script>
    
    <!-- others plugins -->
    <script src="{{ asset('jsfile/plugins.js') }}"></script>
    <script src="{{ asset('jsfile/scripts.js') }}"></script>

    <script>
        $( "#registerform" ).validate({
        rules: {
            field: {
            required: true,
            accept: "image/*"
            }
        }
    });
    </script>
</body>

</html>
