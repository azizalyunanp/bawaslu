@extends('layouts.app_user')
@section('content')
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if(session('notifikasi')) 
                                        <div class="alert alert-success"> {{ session('notifikasi') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif                                        
                                        <form method="post" enctype="multipart/form-data" action="{{ URL('user/ajukan-laporan') }}" id="myform">
                                        @csrf
                                        <input type="hidden" name="no_laporan" value="{{ $no_laporan }}">
                                        <h2>Form Pelaporan</h2>
                                        <h4 class="header-title mb-0 mt-3">1. Pelapor</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang Data Terkait Pelapor</p>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Email**</label>
                                                    <input class="form-control" required type="email" name="email">
                                                </div>
                                            </div> -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Tempat Lahir</label>
                                                    <input class="form-control" required type="text" name="tempat_lahir">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Tanggal Lahir</label>
                                                    <input class="form-control" required type="date" name="tanggal_lahir">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Jenis Kelamin</label>
                                                    <select class="form-control" required name="jk">
                                                        <option value="L">Laki-laki</option>
                                                        <option value="P">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Pekerjaan</label>
                                                    <input class="form-control" required type="text" name="pekerjaan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Kewarganegaraan</label>
                                                    <input class="form-control" required name="kewarganegaraan">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Alamat</label>
                                                    <input class="form-control" required type="text" name="alamat">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">No.Telp/Hp</label>
                                                    <input class="form-control" required type="text" name="no_telp" id="no_hp1" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Fax**</label>
                                                    <input class="form-control"   type="text" name="fax">
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">2. Peristiwa Yang Dilaporkan</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang Data Peristiwa</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Peristiwa</label>
                                                    <input class="form-control" required type="text" name="peristiwa">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Tempat Kejadian</label>
                                                    <input class="form-control" required type="text" name="tempat_kejadian">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Waktu Kejadian</label>
                                                    <input class="form-control" required type="time" name="waktu_kejadian" value="12:00:00">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Hari dan Tanggal Diketahui</label>
                                                    <input class="form-control" required type="date" name="hari">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Terlapor</label>
                                                    <input class="form-control" required type="text"  name="terlapor">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">No.Telp/Hp Terlapor</label>
                                                    <input class="form-control" required type="text" name="no_telp_terlapor"  id="no_hp2" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Alamat Terlapor***</label>
                                                    <input class="form-control" required type="text" name="alamat_terlapor">
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">3. Saksi-Saksi</h4>
                                        <p class="text-muted font-14 mb-2">Isikan tentang Data Para Saksi</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6 class="mb-2 mt-3">Saksi 1</h6>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Nama</label>
                                                                    <input type="text" name="saksi_nama_1" class="form-control" required >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">No.Telp/HP</label>
                                                                    <input type="text" name="saksi_telp_1" class="form-control" id="no_hp3" required  onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Alamat***</label>
                                                                    <input type="text" name="saksi_alamat_1" class="form-control" required >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6 class="mb-2 mt-3">Saksi 2</h6>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Nama</label>
                                                                    <input type="text" name="saksi_nama_2" class="form-control"   >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">No.Telp/HP</label>
                                                                    <input type="text" name="saksi_telp_2" class="form-control" id="no_hp4"   onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Alamat***</label>
                                                                    <input type="text" name="saksi_alamat_2" class="form-control"   >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6 class="mb-2 mt-3">Saksi 3</h6>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Nama</label>
                                                                    <input type="text" name="saksi_nama_3" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">No.Telp/HP</label>
                                                                    <input type="text" name="saksi_telp_3" class="form-control" id="no_hp5"  onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="" class="col-form-label">Alamat***</label>
                                                                    <input type="text" name="saksi_alamat_3" class="form-control"   >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">4. Bukti</h4>
                                        <p class="text-muted font-14 mb-2">Isikan Bukti</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" required name="bukti_1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control"   name="bukti_2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control"   name="bukti_3">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control"   name="bukti_4">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control"  name="bukti_5">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <h4 class="header-title mb-2 mt-3">5. Uraian Singkat Kejadian</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <textarea class="form-control" required name="uraian"></textarea> 
                                            </div>                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Dilaporkan di </label>
                                                    <input type="text" name="dilaporkan_di" class="form-control" required >
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Hari dan Tanggal </label>
                                                    <input type="date" name="dilaporkan_hari" class="form-control" required >
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Waktu / Jam </label>
                                                    <input type="time" name="waktu_jam" class="form-control" required  value="12:00:00" >
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="header-title mb-0 mt-3">6. Lampiran</h4>
                                        <p class="text-muted font-14 mb-2">Silahkan unggah file lampiran</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input class="form-control" required type="file" id="file_upload" accept="image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/x-rar-compressed,
            application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.presentationml.presentation" name="file_upload[]" id="attachment" multiple>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-xl mb-3 float-right bg-primary-color text-white">Simpan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                        </div>
                    </div>
                </div>
            </div>
        
@endsection