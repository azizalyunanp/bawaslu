<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login - srtdash</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/icon/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default-css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <script src="{{ asset('jsfile/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100">
                <form method="post" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                    <div class="login-form-head bg-primary-color">
                        <img src="{{ asset('images/logo.png') }}" alt="logo"></a>
                        <!-- <h4>Masuk</h4> -->
                    </div>
                    <div class="login-form-body ">
                        <div class="form-gp">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            <i class="ti-email"></i>
                        </div>
                        <div class="form-gp">
                            <label for="exampleInputPassword1">Password</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <i class="ti-lock"></i>
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6 text-right">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </div>
                        <div class="submit-btn-area">
                            <button  type="submit" class="btn btn-danger bg-primary-color">Submit <i class="ti-arrow-right"></i></button>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

   <!-- jquery latest version -->
   <script src="{{ asset('jsfile/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('jsfile/popper.min.js') }}"></script>
    <script src="{{ asset('jsfile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('jsfile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('jsfile/metisMenu.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slicknav.min.js') }}"></script>
    
    <!-- others plugins -->
    <script src="{{ asset('jsfile/plugins.js') }}"></script>
    <script src="{{ asset('jsfile/scripts.js') }}"></script>
</body>

</html>