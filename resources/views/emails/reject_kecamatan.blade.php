@component('mail::message')
Kepada # {{ $name }}

Mohon maaf laporan anda dengan nomor #{{ $no_laporan }} telah ditolak 
oleh admin Bawaslu Klaten. 

Dengan alasan sebagai berikut: <br>
{{ $keterangan }}

Terima kasih,<br>
{{ config('app.name') }}
@endcomponent
