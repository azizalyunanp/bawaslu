@component('mail::message')
Kepada #{{ $name }} <br>

Selamat laporan anda dengan nomor # {{ $no_laporan }} telah disetujui oleh admin Bawaslu Klaten. <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
