@component('mail::message')
Klik disini untuk reset password user anda

@component('mail::button', ['url' => $url])
Klik disini
@endcomponent

Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
