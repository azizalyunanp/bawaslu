@component('mail::message')
Kepada # {{ $username }} <br>

Anda diundang untuk menjadi user kecamatan di portal bawaslu klaten,<br>

Berikut username dan password anda:<br>
Username: {{ $username }}<br>
Password: {{ $password }}<br>

@component('mail::button', ['url' => $url ])
Klik disini untuk login<br>
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
