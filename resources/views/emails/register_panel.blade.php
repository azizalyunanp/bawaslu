@component('mail::message')
Kepada #{{ $username }}

Anda diundang untuk menjadi super admin di portal bawaslu klaten, <br><br>

Berikut username dan password anda: <br>
Username: {{ $username }} <br>
Password: {{ $password }} <br>

@component('mail::button', ['url' => $url ])
Klik disini untuk login
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
