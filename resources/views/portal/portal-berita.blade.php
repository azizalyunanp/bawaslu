@extends('layouts.app_home')
@section('content')
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0">{{ $title }}</h2>
                                <p class="text-center">{{ $title }} dari Bawaslu Kabupaten Klaten</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="{{ url('')}}">Home</a></li>
                <li><a href="{{ url('berita')}}" class="active">{{ $title }}</a></li>
            </ol>
        </div>

        <!-- Start Sejarah Singkat -->
        <div class="container">
            <div class="edufair-about-university">
                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <div class="row">
                            <div class="dropcaps col-md-12">
                                <h2 class="edufair-main-title berita-mobile" style="margin-bottom: 45px;">{{ $title }}</h2>
								@foreach($articles as $b)
                                <div class="card shadow mb-10">
									<div class="content-isi">
										<div class="row">
											<div class="col-md-12">
												<a href="{{ url('berita/'.$b->link)}}">
													<h3 class="mt-0 h3-mt0">{{ $b->title }}</h3>
												</a>
											</div>
											<div class="col-md-5">
												<img src="{{ asset('uploaded/portal/'.$b->images) }}" alt="{{ $b->title }}" title="{{ $b->title }}" class="img-berita-list">
											</div>
											<div class="col-md-7 pl-0 mb-0">
												<div class="hidden-desktop"><br></div>
												<p class="pull-right mb-0">
													<small>Diposting oleh <i class="fa fa-user"></i> <b>admin</b></small>
												</p>
												<p class="pull-left mb-0">
													<small><i class="fa fa-history"></i> {{ date('d M Y ',strtotime($b->updated_at)) }}</small>
													&ensp;
													 
												</p>
												<div class="mt-30 mb-0 text-justify">
												<p>{!! substr($b->content,0,200)!!} . . . </p>
												</div>
												<p class="mt-0 mb-0 pull-right">
													<a href="{{ url('berita/'.$b->link)}}" class="edufair-btn-larg events-all-button mt-0 pull-right">Baca Selengkapnya</a>
												</p>
											</div>
										</div>
									</div>
								</div>
								@endforeach
								<div class="row">
									<div class="col-md-12">
					                    <div class="text-center padding-top-small padding-bottom-large">
					                    	{{ $articles->links() }}
					                    </div>
					                </div>
								</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="latest-post-right-sidebar">
                        	<h3 class="mb-20">Berita Terbaru</h3>
                            <div class="card shadow content-isi">
							@foreach($sidebar as $p)
	                            <div class="single-latest-post">
	                                <div class="media">
	                                    <div class="isi-limit-judul media-body">
	                                        <a href="{{ url('berita/'.$p->link)}}" class="">
	                                        {{ $p->title }}
	                                    </a>
	                                        <br>
	                                    </div>
	                                    <span><i class="fa fa-clock-o"></i> {{ date('d M Y',strtotime($p->updated_at))}}</span>
	                                </div>
	                            </div>
							@endforeach
	                            <a href="{{ url('berita') }}" class="latest-post-read-more"> Lihat Semua Berita <i class="fa fa-angle-right"></i></a>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Sejarah Singkat -->
		@endsection