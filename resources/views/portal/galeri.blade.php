@extends('layouts.app_home')
@section('content')
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0">Galeri</h2>
                                <p class="text-center galeri-hp">Kumpulan Foto Kegiatan oleh Bawaslu Kabupaten Klaten</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="{{ url('') }}">Home</a></li>
                <li><a href="{{ url('galeri')}}" class="active">Galeri</a></li>
            </ol>
        </div>

		<!-- Start Gallery -->
		<div class="container">
		    <div class="row">
		        <div class="col-md-8 mt-40 mb-0">
		            <h2 class="edufair-main-title mb-40">Gallery Foto</h2>
		        </div>
		        <div class="col-md-4">
		            
		        </div>
		    </div>
		</div>
		<div class="edufair-portfolio wow fadeIn padding-top-small mb-20" id="portfolio" style="visibility: visible;">
        <div id="edufair-portfolio">
			@foreach($galeri as $g)
		        <div class="tile scale-anm web img-responsive" style="display: inline-block;" data-bound="">
                    <div class="hovereffect4">
                        <img src="{{ $g->images->standard_resolution->url }}" alt="">
                        <div class="overlay">
                            <p>
                                <!-- <a href="{{ $g->link }}" data-toggle="lightbox" data-gallery="example-gallery"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" data-fancybox="gallery"><i class="fa fa-search"></i></a> -->
                                <a data-fancybox="gallery" data-animation-duration="700" data-src="#animatedModal-{{ $g->id }}" href="javascript:;"><i class="fa fa-search"></i></a>
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" target="_blank" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->link }}" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                            </p>
                        </div>
                    </div>
                </div>
                <div style="display: none;" id="animatedModal-{{ $g->id }}" class="animated-modal">
                    <a href="{{ $g->link }}" target="_blank"><img src="{{ $g->images->standard_resolution->url }}" width="100%"></a>
                </div>
			@endforeach
		    </div>
		</div>

		<!-- End Gallery -->
		@endsection