@extends('layouts.app_home')
@section('content')
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0 pengumuman">Pengumuman</h2>
                                <p class="text-center">Pengumuman oleh Bawaslu Kabupaten Klaten</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="{{ url('') }}">Home</a></li>
                <li><a href="{{ url('pengumuman') }}">Pengumuman</a></li>
                <li><a href="#" class="active">Pemilihan Umum Pesta Demokrasi 14 April 2019</a></li>
            </ol>
        </div>


        <!-- Start Sejarah Singkat -->
        <div class="container">
            <div class="edufair-about-university pd-50">
                <div class="row">
                @foreach($pengumuman as $p)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="dropcaps col-md-12 padding-bottom-large">
								<div class="card shadow mb-10">
									<img src="{{asset('img/slide/slide-5.jpg')}}" alt="Berita 1" title="Berita 1" class="img-berita-list">
									<div class="pt-10 pl-20 pr-20">
										<h3>{{ $p->title ++</h3>
										<p>
											<small>{{ date('d M Y',strtotime($p->updated_at) }}</small>
										</p>
										<p style="text-indent: 15px;">{{ $p->body }}</p>
										<a href="{{ url('pengumuman' )}}" class="edufair-btn-larg events-all-button float-right"> Kembali </a>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
			@endsection