@extends('layouts.app_home')
@section('content')
<style type="text/css">
    table{
        width: auto!important;
    }
    td,th{
        padding-right: 5px;
    }
</style>
<div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner edufair-inner single-inner" role="listbox">
        <div class="edufair-overlay">
            <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
            <div class="carousel-caption edufair-caption edufair-single-caption">
                <div class="edufair-slider-middle">
                    <div class="container edufair-slider-text">
                        <h2 class="fadeInLeft text-center pb-0 mb-0">Profil Pimpinan</h2>
                        <p class="text-center">Profil Pimpinan Bawaslu Kabupaten Klaten</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main slider -->
    <ol class="breadcrumb edufair-single-breadcrumbs">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profil</a></li>
        <li><a href="#" class="active">Profil Pimpinan</a></li>
    </ol>
</div>
<div class="edufair-blog-details-1x visi-misi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 blog-description dropcaps card shadow margin-bottom-large">
                        <h2>Profil Pimpinan</h2>
                        @foreach($pimpinan as $p)
                        <div class="row mb-3">
                            <div class="col-md-2">
                                <img src="{{ asset('uploaded/portal/'.$p->images)}}" class="img-pimpinan">
                            </div>
                            <div class="col-md-10">
                                <table border="0" align="center">
                                <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td><b>{{ $p->nama }}</b></td>
                                </tr>
                                <tr>
                                    <td>Tempat,tanggal lahir</td>
                                    <td>:</td>
                                    <td> {{ $p->ttl }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>:</td>
                                    <td>{{ $p->jabatan }}</td>
                                </tr>
                                <!-- <tr>
                                    <td></td>
                                    <td>:</td>
                                    <td>Koordinator Divisi Hukum dan Data Informasi</td>
                                </tr> -->
                                <tr>
                                    <td>Agama</td>
                                    <td>:</td>
                                    <td>{{ $p->agama }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Kantor</td>
                                    <td>:</td>
                                    <td>{{ $p->kantor }}</td>
                                </tr>
                                <tr>
                                    <td>Telp/fax</td>
                                    <td>:</td>
                                    <td>{{ $p->telp }}</td>
                                </tr>
                                <tr>
                                    <td>E-mail</td>
                                    <td>:</td>
                                    <td>{{ $p->email }}</td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <hr class="hrhrhr">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection