@extends('layouts.app_home')
@section('content')
<div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner edufair-inner single-inner" role="listbox">
        <div class="edufair-overlay">
            <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
            <div class="carousel-caption edufair-caption edufair-single-caption">
                <div class="edufair-slider-middle">
                    <div class="container edufair-slider-text">
                        <h2 class="fadeInLeft text-center pb-0 mb-0">Tentang Kami</h2>
                        <p class="text-center">Tentang Bawaslu Kabupaten Klaten</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main slider -->
    <ol class="breadcrumb edufair-single-breadcrumbs">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profil</a></li>
        <li><a href="#" class="active">Tentang Kami</a></li>
    </ol>
</div>
<div class="edufair-blog-details-1x tentang">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 blog-description dropcaps card shadow margin-bottom-large">
                        <h2>Tentang Bawaslu Kabupaten Klaten</h2>
                        <div>
                        	<h5>
                        	Tentang Kami
	                    	</h5>
	                    	<p>
	                    		<img src="{{asset('img/logo.png')}}" class="img-tentang">
		                         {!! $about_us->body !!}
		                    </p>
	                    	<div class="edufair-footer-address mb-20">
                            <h5> Kontak Kami </h5>
                            <p class="mb-0"> <i class="fa fa-phone"></i>{{ $about_us->telephone }}</p>
                            <p class="mb-0"> <i class="fa fa-envelope"></i>{{ $about_us->email }}</p>
                            <p class="mb-0"> <i class="fa fa-map-marker"></i>{!! $about_us->alamat !!}</p>
                        </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection