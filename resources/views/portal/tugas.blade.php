@extends('layouts.app_home')
@section('content')
<div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner edufair-inner single-inner" role="listbox">
        <div class="edufair-overlay">
            <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
            <div class="carousel-caption edufair-caption edufair-single-caption">
                <div class="edufair-slider-middle">
                    <div class="container edufair-slider-text">
                        <h2 class="fadeInLeft text-center pb-0 mb-0">Tugas dan Wewenang</h2>
                        <p class="text-center">Tugas, Wewenang, dan Kewajiban</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main slider -->
    <ol class="breadcrumb edufair-single-breadcrumbs">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profil</a></li>
        <li><a href="#" class="active">Tugas, dan Wewenang</a></li>
    </ol>
</div>
<div class="edufair-blog-details-1x visi-misi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 blog-description dropcaps card shadow margin-bottom-large">
                        <h2>{{ $tugas->title }}</h2>
                            <p>{!! $tugas->content !!}</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection