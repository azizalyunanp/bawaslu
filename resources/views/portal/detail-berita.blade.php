@extends('layouts.app_home')
@section('content')
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0">Detail Berita</h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="{{ url('') }}">Home</a></li>
                <li><a href="{{ url('berita') }}">Berita</a></li>
               
            </ol>
        </div>


        <!-- Start Sejarah Singkat -->
        <div class="container">
            <div class="edufair-about-university pd-50">
                <div class="row">
                    <div class="col-md-8">
						@foreach($berita as $b)
                        <div class="row">
                            <div class="dropcaps col-md-12 padding-bottom-large">
								<div class="card shadow mb-10">
									<img src="{{ url('uploaded/portal/'.$b->images) }}" alt="{{ $b->title }}" title="{{ $b->title }}" class="img-berita-list">
									<div class="pt-10 pl-20 pr-20">
										<h3>{{ $b->title }}</h3>
										<p>
											<small>diupload oleh <i class="fa fa-user"></i> Admin Web, pada <i class="fa fa-history"></i> {{ date('d M Y',strtotime($b->updated_at)) }}</small>
											<!-- <small class="float-right"><i class="fa fa-eye"></i>7x Dilihat,  </small> -->
										</p>
                                        <br>
                                        {!! $b->content !!}
                                        <br>
                                        Tags : {!! $b->tags !!}
										<a href="{{ url('berita') }}" class="edufair-btn-larg events-all-button float-right"> Kembali </a>
									</div>
								</div>
                            </div>
                        </div>
						@endforeach
                    </div>
                    <div class="col-md-4">
                        <div class="latest-post-right-sidebar">
                        	<h3 class="mb-20">Berita Terbaru</h3>
                            <div class="card shadow content-isi">
							@foreach($sidebar as $p)
	                            <div class="single-latest-post">
	                                <div class="media">
	                                    <div class="isi-limit-judul media-body">
	                                        <a href="{{ url('berita/'.$p->link)}}" class="">
	                                        {{ $p->title }}
	                                    </a>
	                                        <br>
	                                    </div>
	                                    <span><i class="fa fa-clock-o"></i> {{ date('d M Y',strtotime($p->updated_at))}}</span>
	                                </div>
	                            </div>
							@endforeach
	                            <a href="{{ url('berita') }}" class="latest-post-read-more"> Lihat Semua Berita <i class="fa fa-angle-right"></i></a>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection