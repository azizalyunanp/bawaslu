@extends('layouts.app_home')
@section('content')
<div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner edufair-inner single-inner" role="listbox">
        <div class="edufair-overlay">
            <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
            <div class="carousel-caption edufair-caption edufair-single-caption">
                <div class="edufair-slider-middle">
                    <div class="container edufair-slider-text">
                        <h2 class="fadeInLeft text-center pb-0 mb-0">Sejarah</h2>
                        <p class="text-center">Sejarah Bawaslu Kabupaten Klaten</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main slider -->
    <ol class="breadcrumb edufair-single-breadcrumbs">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profil</a></li>
        <li><a href="#" class="active">Sejarah</a></li>
    </ol>
</div>
<div class="edufair-blog-details-1x visi-misi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 blog-description dropcaps card shadow margin-bottom-large">
                        <h2>Sejarah</h2>
                            Organisasi pengawas pemilu baru muncul pada Pemilu 1982, walaupun pemilu pertama di Indonesia sudah dilaksanakan pada tahun 1955. Pembentukan Panwaslak Pemilu pada Pemilu 1982 dilatari oleh protes protes atas banyaknya pelanggaran dan manipulasi penghituan suara yang dilakukan oleh para petugas pemilu pada Pemilu 1971.</p>
                            <p>Karena pelanggaran dan kecurangan pemilu yang terjadi pada Pemilu 1977 jauh lebih masif. Protes-protes ini lantas direspons pemerintah dan DPR yang didominasi Golkar dan ABRI. Akhirnya muncullah gagasan memperbaiki undang-undang yang bertujuan meningkatkan ‘kualitas’ Pemilu 1982.</p>
                            <p>Pada pemilu 1982 pemerintah mengintroduksi adanya badan baru yang akan terlibat dalam urusan pemilu untuk mendampingi Lembaga Pemilihan Umum (LPU). Badan baru ini bernama Panitia Pengawas Pelaksanaan Pemilihan Umum (Panwaslak Pemilu) yang bertugas mengawasi pelaksanaan pemilu</p>
                            <p>Dengan struktur, fungsi, dan mekanisme kerja yang baru, pengawas pemilu tetap diaktifkan untuk Pemilu 1999. Namanya pun diubah dari Panitia Pengawas Pelaksana Pemilihan Umum (Panwaslak Pemilu) menjadi Panitia Pengawas Pemilihan Umum (Panwaslu).</p>
                            <p>Perubahan terhadap pengawas pemilu baru dilakukan lewat UU No. 12/2003. UU ini menegaskan, untuk melakukan pengawasan Pemilu, dibentuk Panitia Pengawas Pemilu, Panitia Pengawas Pemilu Provinsi, Panitia Pengawas Pemilu Kabupaten/Kota, dan Panitia Pengawas Pemilu Kecamatan.</p>
                            <p>Selanjutnya kelembagaan pengawas Pemilu dikuatkan melalui Undang Undang nomor 22 tahun 2007 tentang penyelenggara pemilu dengan dibentuk sebuah lembaga yang dinamakan Badan Pengawas Pemilu (Bawaslu). Adapun lingkup pengawasan Bawaslu yakni terkait kepatuhan KPU sebagai Penyelenggara Pemilu dan Pilkada. Dalam perjalanannya Bawaslu mengalami penguatan secara bertahap, Pertama melalui Undang Undang No. 12 Tahun  2003, Undang Undang ini mengamanatkan pembentukan lembaga <em>ad hoc</em> yang terlepas dari struktur kelembagaan KPU yang bertugas untuk melaksanakan pengawasan pemilu. Kedua melalui Undang Undang No. 22 Tahun 2007, dalam Undang Undang ini Pengawas Pemilu ditingkat pusat dipermanenkan menjadi Badan Pengawas Pemilu (Bawaslu). Ketiga melalui Undang Undang No. 15 Tahun 2011 dalam Undang Undang ini kelembagaan Bawaslu kembali diperkuat dengan dipermanenkannya Panitia Pengawas Pemilu ditingkat Provinsi menjadi Bawaslu Provinsi. Keempat Undang Undang No. 7 Tahun 2017, Undang Undang ini memberikan kewenangan yang besar dan signifikan. Secara kelembagaan, Panitia Pengawas Pemilu di tingkat Kabupaten/Kota dipermanenkan menjadi Bawaslu Kabupaten/Kota. Bawaslu sebagai lembaga pengawas pemilu diberi kewenangan yang cukup kuat yakni sebagai lembaga yang memiliki kewenangan untuk menerima, memeriksa, mengkaji dan memutus pelanggaran administrasi pemilu melalui proses sidang adjudikasi. Bawaslu bukan hanya sebagai lembaga pengawas, tetapi juga lembaga peradilan dalam penegakan hukum penyelesaian pelanggaran administrasi pemilu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection