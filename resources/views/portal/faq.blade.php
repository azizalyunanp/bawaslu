@extends('layouts.app_home')
@section('content')
<div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner edufair-inner single-inner" role="listbox">
        <div class="edufair-overlay">
            <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
            <div class="carousel-caption edufair-caption edufair-single-caption">
                <div class="edufair-slider-middle">
                    <div class="container edufair-slider-text">
                        <h2 class="fadeInLeft text-center pb-0 mb-0">FAQ</h2>
                        <p class="text-center galeri-hp">Portal Bantuan Bawaslu Kabupaten Klaten</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main slider -->
    <ol class="breadcrumb edufair-single-breadcrumbs">
        <li><a href="{{ url('') }}">Home</a></li>
        <li><a href="{{ url('faq') }}">FAQ</a></li>
    </ol>
    </div>

    <div class="edufair-blog-details-1x margin-bottom-large">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a data-toggle="pill" href="#menu">Pertanyaan Umum</a></li>
                        </ul>
                    </div>
                    <div class="col-md-9 card shadow">
                        <div class="tab-content">
                        <h3>Pertanyaan Umum</h3>
                        @foreach($faqs as $f)
                            <div id="menu" class="tab-pane fade in active">
                                
                                <div class="panel-body">
                                    <h5>{!! $f->question !!}</h5>
                                    <p>{!! $f->answer !!} </p>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection