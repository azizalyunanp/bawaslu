@extends('layouts.app_home')
@section('content')
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0">Agenda</h2>
                                <p class="text-center">Kumpulan Agenda oleh Bawaslu Kabupaten Klaten</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#" class="active">Agenda</a></li>
            </ol>
        </div>

<div class="edufair-events-1x agenda">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-0">
                <h2 class="edufair-main-title agenda berita-mobile">Agenda Bawaslu Kabupaten Klaten</h2>
            </div>
            <div class="col-md-4">
            </div>

            @foreach($agenda as $a)
            <div class="col-md-12">
                <div class="event-container-1x card shadow content-isi pt-20 pb-20">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="single-events-1x">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <b>{{ date('d',strtotime($a->updated_at)) }}</b>
                                        <span>Feb</span>
                                    </div>
                                    <div class="media-body">
                                        <a href="{{ url('agenda/'.$a->link) }}"><h4 class="media-heading mt-10">{{ $a->title }}</h4></a>
                                        <!-- <div class="event-schedule">
                                            <i class="fa fa-clock-o"></i>18 - 22 Februari 2018 &nbsp;&nbsp;&nbsp;&nbsp;
                                            <i class="fa fa-map-marker"></i> Kota Depok, Jawa Barat
                                        </div> -->
                                    </div>
                                    <p>{!! substr($a->content,0,200)!!} . . . </p>
                                    <a href="{{ url('agenda/'.$a->link) }}" class="edufair-btn-larg events-all-button"> Lihat Detail </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-events-image-1x">
                                <div class="edufair-course-overlay">
                                    <img src="{{ asset('uploaded/portal/'.$a->images) }}" alt="Avatar" class="edufair-course-image edufair-event-image">
                                    <div class="edufair-course-middle">
                                        <a href="{{ url('agenda/'.$a->link) }}" class="edufair-course-text">Lihat Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                	</div>
                </div>  
            </div>

            @endforeach
            <div class="row">
				<div class="col-md-12">
                    <div class="text-center padding-top-small padding-bottom-large">
                         {{ $agenda->links() }}
                    </div>
                </div>
			</div>

        </div>
    </div>
</div>
@endsection