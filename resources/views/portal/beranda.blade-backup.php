@extends('layouts.app_home')
@section('content')
            <!-- Start main slider -->
            <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner edufair-inner " role="listbox">
                @php 
                $no = 1;
                @endphp
                
                @foreach($articles as $news)
                @php
                    $p = $no++;
                @endphp
                <div class="item {{ ($p ==1) ? 'active' : ''}} edufair-overlay">
                        <div class="edufair-overlay">
                            <img src="{{ asset('uploaded/portal/'.$news->images) }}" alt="slide 2" class="edufair-slider-image">
                            <div class="carousel-caption edufair-caption">
                                <div class="edufair-slider-middle home-mobile">
                                    <div class="container edufair-slider-text">
                                        <h4 class="fadeInLeft">{!! strip_tags(substr($news->content,0,50)) !!} . . .</h4>
                                        <h2 class="fadeInDown animated dipc">{{ $news->title }}</h2>
                                        <h2 class="fadeInDown animated dihp">{{ substr($news->title,0,33)  }} ...</h2>
                                        <a href="{{ url('berita/'.$news->link)}}" class="edufair-btn-larg-round fadeInUp animated"> Baca Selengkapnya </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control edufair-control" href="#carousel-example-generic" role="button" data-slide="prev"></a>
                <a class="right carousel-control edufair-control" href="#carousel-example-generic" role="button" data-slide="next"></a>
            </div>
            <!-- End main slider -->
        </div>
        <!--End Edufair nav-->

        <!-- Start Sejarah Singkat -->
        <div class="container">
            <div class="edufair-about-university">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="dropcaps col-md-12">
                                <h2 class="edufair-main-title berita-mobile" style="margin-bottom: 45px;">Berita Utama</h2>
                                @foreach($articles as $news)
                                <div class="card shadow mb-10">
									<div class="content-isi">
										<div class="row">
											<div class="col-md-12">
												<a href="{{ url('berita/'.$news->link)}}">
													<h3 class="mt-0 h3-mt0">{{ $news->title }}</h3>
												</a>
											</div>
											<div class="col-md-5">
												<img src="{{ asset('uploaded/portal/'.$news->images)  }}" alt="{{ $news->title }}" title="{{ $news->title }}" class="img-berita-list">
											</div>
											<div class="col-md-7 pl-0 mb-0">
                                                <div class="hidden-desktop"><br></div>
												<p class="pull-right mb-0">
													<small>Diposting oleh <i class="fa fa-user"></i> <b>admin</b></small>
												</p>
												<p class="pull-left mb-0">
													<small><i class="fa fa-history"></i> {{ date('d M Y',strtotime($news->updated_at))}}</small>
													&ensp;
													<small><i class="fa fa-eye"></i> 77</small>
												</p>
												<div class="mt-30 mb-0 text-justify">
													<p>{!! strip_tags(substr($news->content,0,200)) !!} . . .</p>
 												</div>
												<p class="mt-0 mb-0 pull-right">
													<a href="{{ url('berita/'.$news->link )}}" class="edufair-btn-larg events-all-button mt-0 pull-right">Baca Selengkapnya</a>
												</p>
											</div>
										</div>
									</div>
								</div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ $articles->links() }}
        </div>
        
        <!-- BANNER -->
        <div class="edufair-events-1x">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <a href="{{ url('user') }}" target="_blank"><img src="{{ asset('images/banner.jpg')}}" class="img-responsive" ></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- end BANNER -->


        <!-- Start Agenda-->
        <div class="edufair-events-1x">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mb-0 mt-40">
                        <h2 class="edufair-main-title mt-40">Agenda Bawaslu Kabupaten Klaten</h2>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('agenda') }}" class="edufair-btn-larg events-all-button"> Semua Agenda </a>
                    </div>

                    @foreach($agenda as $agen)
                    <div class="col-md-12">
                        <div class="event-container-1x card shadow content-isi pt-20 pb-20">
                            <div class="row">
    	                        <div class="col-md-8">
    	                            <div class="single-events-1x">
    	                                <div class="media">
    	                                    <div class="media-left media-middle">
    	                                        <b>{{ date('d',strtotime($agen->updated_at)) }}</b>
    	                                        <span>{{ date('M',strtotime($agen->updated_at)) }}</span>
    	                                    </div>
    	                                    <div class="media-body">
    	                                        <a href="{{ URL('agenda/'.$agen->link) }}"><h4 class="media-heading mt-10">{{ $agen->title }}</h4></a>
    	                                        <!-- <div class="event-schedule">
    	                                            <i class="fa fa-clock-o"></i>18 - 22 Februari 2018 &nbsp;&nbsp;&nbsp;&nbsp;
    	                                            <i class="fa fa-map-marker"></i> Kota Depok, Jawa Barat
    	                                        </div> -->
    	                                    </div>
    	                                    <p>{!! $agen->content !!}</p>
    	                                    <a href="{{ URL('agenda/'.$agen->link) }}" class="edufair-btn-larg events-all-button"> Lihat Detail </a>
    	                                </div>
    	                            </div>
    	                        </div>
    	                        <div class="col-md-4">
    	                            <div class="single-events-image-1x">
    	                                <div class="edufair-course-overlay">
    	                                    <img src="{{ asset('uploaded/portal/'.$agen->images) }}" alt="Avatar" class="edufair-course-image edufair-event-image">
    	                                    <div class="edufair-course-middle">
    	                                        <a href="{{ URL('agenda/'.$agen->link) }}" class="edufair-course-text">Lihat Detail</a>
    	                                    </div>
    	                                </div>
    	                            </div>
    	                        </div>
                        	</div>
                        </div>
                    </div>
                    @endforeach
                   
                </div>
            </div>
        </div>
        <!-- End Agenda -->
        <!-- Start Gallery -->
        <div class="container">
            <div class="row">
                <div class="col-md-8 mt-40 mb-0">
                    <h2 class="edufair-main-title mb-40">Gallery Foto</h2>
                </div>
                <div class="col-md-4">
                    <a href="{{ url('galeri')}}" class="edufair-btn-larg events-all-button"> Semua Foto </a>
                </div>
            </div>
        </div>
        <!-- <div class="edufair-portfolio wow fadeIn padding-top-small" id="portfolio" style="visibility: visible;">
            <div id="edufair-portfolio">
            @foreach($galeri as $g)
                <div class="tile scale-anm web img-responsive" style="display: inline-block;" data-bound="">
                    <div class="hovereffect4">
                        <img src="{{ $g->images->standard_resolution->url }}" alt="">
                        <div class="overlay">
                            <p> -->
                                <!-- <a href="{{ $g->link }}" data-toggle="lightbox" data-gallery="example-gallery"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" data-fancybox="gallery"><i class="fa fa-search"></i></a> -->
                                <!-- <a data-fancybox="gallery" data-animation-duration="700" data-src="#animatedModal-{{ $g->id }}" href="javascript:;"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->link }}" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                                <!-- <a href="{{ $g->images->standard_resolution->url }}" target="_blank" data-lightbox="example-set"><i class="fa fa-search"></i></a> -->
                             <!-- </p>
                        </div>
                    </div>
                </div>
                <div style="display: none;" id="animatedModal-{{ $g->id }}" class="animated-modal">
                    <a href="{{ $g->link }}" target="_blank"><img src="{{ $g->images->standard_resolution->url }}" width="100%"></a>
                </div>
                @endforeach
            </div>
        </div> -->
        <!-- End Gallery -->
        <!-- Start Edufair Become Teacher -->
        <div class="online-become-techer-1x mt-100">
            <div class="container">
                <div class="become-techer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="become-techer-right text-center">
                                <h3>Iklan Pengawasan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="video margin-top-middle embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{ $iklan->video_1 }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="video margin-top-middle embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{ $iklan->video_2 }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="video margin-top-middle embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{ $iklan->video_3 }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <!-- End Edufair Become Teacher -->
        <!-- Start edufair features -->
        <div class="online-course-category-1x  margin-bottom-middle">
            <div class="container">
                <div class="row online-course-dark-text">
                    <h2>Pengumuman</h2>
                    <div class="edufair-features">
                    @foreach($pengumuman as $peng)
                        <div class="col-md-12 wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.3s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.3s;">
                            <div class="media edufair-single-features edufair-single-features-1x wow fadeInDown" style="visibility: visible;">
                                <div class="media-left">
                                    <img src="{{ asset('img/icon1.png') }}" alt="">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading edufair-subtitle">{{ $peng->title }}</h4>
                                    <a href="{{ url('pengumuman/'.$peng->link)}}" class="">Lihat Lebih Lanjut</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
@endsection