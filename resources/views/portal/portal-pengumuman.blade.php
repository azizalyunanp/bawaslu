@extends('layouts.app_home')
@section('content')
<style type="text/css">
    @media only screen and (max-width: 768px) { 
        .edufair-slider-middle{
            margin-top: -85px !important;
            position: relative;
            left: -75px !important;
            transform: none !important;
            -ms-transform: none !important;
            width: 400px;
        }
    }
</style>
        <div id="carousel-example-generic" class="carousel slide edufair-slide carousel-fade" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner edufair-inner single-inner" role="listbox">
                <div class="edufair-overlay">
                    <img src="{{ asset('img/header-files.jpg') }}" alt="slide 1" class="edufair-slider-image edufair-single-image">
                    <div class="carousel-caption edufair-caption edufair-single-caption">
                        <div class="edufair-slider-middle">
                            <div class="container edufair-slider-text">
                                <h2 class="fadeInLeft text-center pb-0 mb-0 pengumuman">Pengumuman</h2>
                                <p class="text-center">Pengumuman oleh Bawaslu Kabupaten Klaten</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End main slider -->
            <ol class="breadcrumb edufair-single-breadcrumbs">
                <li><a href="{{ url('')}}">Home</a></li>
                <li><a href="{{ url('pengumuman')}}" class="active">Pengumuman</a></li>
            </ol>
        </div>

<div class="edufair-events-1x pengumuman">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-0">
                <h2 class="edufair-main-title mb-20">Pengumuman Bawaslu Kabupaten Klaten</h2>
            </div>
            <div class="col-md-4">
            </div>

            <div class="col-md-12">
                @foreach($pengumuman as $p)
                <div class="edufair-features mb-0">
                    <div class="col-md-12 wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.3s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.3s;">
                        <div class="media edufair-single-features edufair-single-features-1x wow fadeInDown mb-0" style="visibility: visible;">
                            <div class="media-left">
                                <img src="{{asset('img/icon1.png')}}" alt="">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading edufair-subtitle">{{ $p->title }}</h4>
                                <a href="{{ URL('pengumuman/'.$p->link) }}" class="">Lihat Lebih Lanjut</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="row">
				<div class="col-md-12">
                    <div class="text-center padding-top-small padding-bottom-large">
                        {{ $pengumuman->links() }}
                    </div>
                </div>
			</div>

        </div>
    </div>
</div>
@endsection