@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
                                        @if(session('verifikasi'))
                                        <div class="alert alert-success">
                                            {{ session('verifikasi') }}
                                        </div>
                                        @endif
                                        @isset($notifikasi)
                                            <p><strong>{{ $notifikasi }}</strong></p>
                                        @endisset
                                        <br>


                                        <div class="single-table">
                                            <div class="table-responsive">
                                            <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">No Laporan</th>
                                                            <th scope="col">NO KTP</th>
                                                            <th scope="col">Nama</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Tangggal Upload</th>
                                                            <th scope="col">Lihat Detail</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                    @foreach($form as $p)
                                                   
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->no_laporan !!} </td>
                                                            <td>{!! $p->nik !!} </td>
                                                            <td>{!! $p->nama !!} </td>
                                                            <td>
                                                                @if($p->status == 'T')
                                                                <span class="badge badge-pill badge-success">Diterima</span>
                                                                @elseif($p->status == 'P')
                                                                <span class="badge badge-pill badge-warning">Pending</span>
                                                                @else
                                                                <span class="badge badge-pill badge-danger">Ditolak</span>
                                                                @endif
                                                            </td>
                                                            <td>{!! date('d M Y',strtotime($p->updated_at)) !!}  </td>
                                                            <td><a  target="_blank" class="btn btn btn-primary" href="{{ url('portal/detail/laporanformb1/'.$p->no_laporan) }}">
                                                             Lihat Detail</a> </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <br>
                                                {{ $form->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->

 
@endsection