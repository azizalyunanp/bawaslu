@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                    @if(session('notifikasi')) 

                                    <div class="alert alert-success"> {{ session('notifikasi') }}

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                        </button>
                                    </div>
                                    @endif
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
                                        <br>
                                        
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">Judul</th>
                                                            <th scope="col">Link</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @foreach($cvisimisi as $p)
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->title !!} </td>
                                                            <td>{!! $p->link !!} </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection