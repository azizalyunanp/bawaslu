@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                    @if(session('notifikasi')) 

                                    <div class="alert alert-success"> {{ session('notifikasi') }}

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                        </button>
                                    </div>
                                    @endif
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
                                        <div class="float-right">
                                            <a href="{{ url('faq') }}" class="btn bg-primary text-white" target="_blank" title="Preview"><i class="fa fa-eye "></i> Preview</a>
                                        </div>
                                        <br>
                                        <a href="{{ route($add_url) }}" class="btn btn-success">Tambah</a>
                                        <br><br>
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">Pertanyaan</th>
                                                            <th scope="col">Jawaban</th>
                                                            <th scope="col">Opsi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                    @foreach($cfaqs as $p)
                                                    
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->question !!} </td>
                                                            <td>{!! substr($p->answer,0,50) !!} </td>
                                                            
                                                            <td>
                                                            <form method="post" action="{{ route('portal.faqs.destroy',$p->id) }}">
                                                                <a class="btn btn-primary" href="{{ url('portal/faqs/'.$p->id.'/edit') }}"><i class="fa fa-pencil"></i></a>
                                                                
                                                                    {{ csrf_field() }}
				                                                    {{ method_field('DELETE') }}

                                                                    <button onclick="return confirm('Hapus Data ??') " type="submit" class="btn btn-danger" href="#"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                
                                                
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection