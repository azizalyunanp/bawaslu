@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-8 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                    @foreach($users as $p)
                                    <form method="post"  enctype="multipart/form-data">
                                   
                                        <h4 class="header-title"> User Masyarakat </h4>
                                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label"><strong>Nik</strong></label>
                                            <p> {{ $p->nik }}</p>
                                             
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label"><strong>Nama</strong></label>
                                            <p>{{ $p->name }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label"><strong>Email</strong></label>
                                            <p>{{ $p->email }}</p>
                            
                                        </div>
 
                                    </div>
                                </div>
                            </div>
                            
                            </form>
                                
                            <div class="col-4 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                            <div class="form-group">
                                                <label for="example-search-input" class="col-form-label"></strong>Foto Profil</strong></label> <br>
                                                <img src="{{ asset('uploaded/masyarakat/foto/'.$p->images) }}" width="300">
                                            </div>
                                        </div>
                                    </div>
               
                            @endforeach
                        </div>
                    </div>

            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection