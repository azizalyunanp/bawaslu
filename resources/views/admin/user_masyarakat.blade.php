@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
                                        <br>
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">Nik</th>
                                                            <th scope="col">Nama</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Opsi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                    @foreach($users as $p)
                                                    
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->nik !!} </td>
                                                            <td>{!! $p->name !!} </td>
                                                            <td>{!! $p->email !!} </td>
                                                            
                                                            <td>
                                                                <a  target="_blank" href="{{ url('portal/user-masyarakat/'.$p->id) }}" class="btn btn-primary"> Detail</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection