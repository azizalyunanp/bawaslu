@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
    <div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row">
                <!-- Textual inputs start -->
                <div class="col-6 mt-6">
                    <div class="card">
                        <div class="card-body">
                        @if(session('notifikasi')) 

                        <div class="alert alert-success"> {{ session('notifikasi') }}

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                            </button>
                        </div>
                        @endif
                            <h4 class="header-title">{{ $breadcrumb }} </h4>
                            <br>
                            <a href="{{ URL('portal/galeri/tambah') }}" class="btn btn-success">Tambah</a>
                            <br><br>
                                <div class="table-responsive-sm">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Foto</th>
                                            <th scope="col">Nama File</th>
                                            <th scope="col">Hapus</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($photos as $photo)
                                            <tr>
                                                <td><img src="{{ asset('uploaded/portal/'.$photo->filename) }}" width="150"></td>
                                                <td>{{ $photo->filename }}</td>
                                                <td>
                                                    <form method="post" action="{{ url('portal/galeri/post-hapus') }}">
                                                    {{ Form::hidden('id', $photo->original) }}
                                                    {{ csrf_field() }}
                                                        
                                                    <button type="submit" onclick="return confirm('Hapus Data ??')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            
                            </div>
                        </div>
                    </div>
            

                    <div class="col-6 mt-6">
                        <div class="card">
                            <div class="card-body">
                                <form method="post" action="{{ URL('portal/change-header') }}" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-group">
                                    @foreach($headfoto as $h)
                                        <img src="{{ asset('uploaded/header/'.$h->header) }}"  class="img-responsive">
                                    @endforeach
                                        <br>
                                        <input type="file" class="form-control" name="header">

                                    </div>
                                
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload Header</button>  

                                    </div>
                                </form>

                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
@endsection
     
