@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>

                                    @if(session('verifikasi'))
                                        <div class="alert alert-success">
                                            {{ session('verifikasi') }}
                                        </div>
                                    @endif

                                    @if(Request::segment(1) == 'portal')  
                                    <form method="get" action="{{ URL('portal/rekap/laporanformb1/filter-all') }}"> 
                                   
                                        <b class="text-muted mb-3 mt-4 d-block">Filter Berdasarkan:</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="cR1" name="theName" class="custom-control-input" checked="">
                                                    <label class="custom-control-label" for="cR1">Tanggal</label>
                                            </div>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"   id="cR2"  name="theName"  class="custom-control-input"  >
                                                    <label class="custom-control-label" for="cR2">No Laporan</label>
                                            </div>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  id="cR3" name="theName"   class="custom-control-input" >
                                                    <label class="custom-control-label" for="cR3">Status</label>
                                            </div>
                                            <br>

                                            <div id="form-1">
                                                <div class="form-row align-items-center">
                                                    <div class="col-sm-3 my-1">
                                                        <label class="sr-only" for="inlineFormInputName">From</label>
                                                        <input type="date" class="form-control"  name="from" required value="{{ date('Y-m-01') }}" >
                                                    </div>
                                                    <div class="col-sm-3 my-1">
                                                        <label class="sr-only" for="inlineFormInputGroupUsername">To</label>
                                                        <div class="input-group">
                                                        <input type="date" class="form-control" name="to" required value="{{ date('Y-m-t') }}">
                                                        </div>
                                                    </div>
            
                                                     
                                                </div>
                                                
                                            </div>


                                            <div id="form-2">
                                                <div class="form-row align-items-center">
                                                    <div class="col-sm-3 my-1">
                                                        <label class="sr-only" for="inlineFormInputName">No Laporan</label>
                                                        <input type="text" class="form-control"  name="no_laporan" placeholder="Cari No Laporan">
                                                    </div>
 
                                                </div>
                                                 
                                            </div>

                                            <div id="form-3">
                                                <div class="form-row align-items-center">
                                                    <div class="col-sm-3 my-1">
                                                        <label class="sr-only" for="inlineFormInputName">Status</label>
                                                        <select class="form-control" name="status">
                                                            <option value="%">Semua</option>
                                                            <option value="T">Diterima</option>
                                                            <option value="F">Ditolak</option>
                                                        </select>
                                                    </div>
        
                                                </div>      
                                            </div>
                                            <button type="submit" class="btn bg-primary-color text-white">Filter</button>
                                    </form>
                                    @endif
                                        <br>
                                        @isset($notifikasi)
                                            <p><strong>{{ $notifikasi }}</strong></p>
                                        @endisset
                                        <br>
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead class="text-uppercase bg-primary-color">
                                                        <tr class="text-white">
                                                            <th scope="col">No</th>
                                                            <th scope="col">No Laporan</th>
                                                            <th scope="col">NO KTP</th>
                                                            <th scope="col">Nama</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Keterangan</th>
                                                            <th scope="col">Tangggal Upload</th>
                                                            <th scope="col">Lihat Detail</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    @php 
                                                        $no = 1;
                                                    @endphp
                                                    @foreach($form as $p)
                                                   
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{!! $p->no_laporan !!} </td>
                                                            <th scope="col">{!! $p->nik !!}</th>
                                                            <td>{!! $p->nama !!} </td>
                                                            <td>
                                                                @if($p->status == 'T')
                                                                <span class="badge badge-pill badge-success">Diterima</span>
                                                                @elseif($p->status == 'P')
                                                                <span class="badge badge-pill badge-warning">Pending</span>
                                                                @else
                                                                <span class="badge badge-pill badge-danger">Ditolak</span>
                                                                @endif
                                                            </td>
                                                            <td>{!! $p->keterangan !!}  </td>
                                                            <td>{!! date('d M Y',strtotime($p->updated_at)) !!}  </td>
                                                            <td><a  target="_blank" class="btn btn btn-primary" href="{{ url('portal/detail/laporanformb1/'.$p->no_laporan) }}">
                                                             Lihat Detail</a> </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <br>
                                                jhj
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            
                           
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->

 
@endsection