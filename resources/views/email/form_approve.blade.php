@component('mail::message')
 
Kepada # {{ $name }} <br>
Selamat laporan anda dengan nomor # {{ $no_laporan }} telah disetujui oleh admin Bawaslu Klaten.<br>
<br>
Petunjuk Berikutnya: <br>
• Bawa Screenshoot dan Nomor Laporan ke kantor Bawaslu Klaten. <br>
• Sertakan bukti-bukti pendukung seperti gambar atau video kepada <br>
petugas kami. <br> <br>
Catatan: <br>
Alamat Kantor Bawaslu: JL. Bali No 32, Pandanrejo, Klaten Tengah, Kabupaten Klaten (57413) <br>
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
