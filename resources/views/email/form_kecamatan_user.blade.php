@component('mail::message')

Kepada # {{ $name }} <br>
Terima kasih telah melaporkan Form A kepada Bawaslu Klaten. <br>
Berikut nomor laporan anda # {{ $no_laporan }}  . Klik Disini untuk melihat detail laporan anda.<br>
Tunggu informasi lanjutan dari kami.<br>
Terima Kasih<br>
Bawaslu Klaten<br>

@component('mail::button', ['url' =>  $url  ])
Klik Disini
@endcomponent

@endcomponent
