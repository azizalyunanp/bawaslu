@component('mail::message')
 

Kepada  {{ $name }}<br>
Mohon maaf laporan anda dengan nomor  {{ $no_laporan }} telah ditolak oleh admin kami dengan alasan sebagai berikut. <br><br>
Alasan: {{ $alasan }} <br>
 
Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
