@component('mail::message')

Kepada Super Admin Bawaslu Klaten <br>
Laporan baru form B1 dengan nomor # {{ $no_laporan }}  telah dilaporkan oleh masyarakat. <br>
Berikut untuk melihat detail laporan. <br>

@component('mail::button', ['url' =>  $url  ])
Klik Disini
@endcomponent

Terima Kasih,<br>
Klaten Bawaslu
@endcomponent
