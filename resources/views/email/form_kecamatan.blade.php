@component('mail::message')

Kepada Super Admin Bawaslu Klaten <br>
Laporan baru form A dengan nomor # {{ $no_laporan }}  telah dilaporkan oleh kecamatan. <br>
Berikut untuk melihat detail laporan. <br>

@component('mail::button', ['url' =>  $url  ])
Klik Disini
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
