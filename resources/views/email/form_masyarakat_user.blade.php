@component('mail::message')

Kepada # {{ $name }}
Terima kasih telah melaporkan Form B1 kepada Bawaslu Klaten. <br>
Berikut  nomor laporan anda # {{ $no_laporan }}  . <br><br>
Klik Disini untuk melihat detail laporan anda.<br>
Tunggu informasi lanjutan dari kami.<br>

@component('mail::button', ['url' =>  $url  ])
Klik Disini
@endcomponent

Terima Kasih<br>
Bawaslu Klaten<br>
@endcomponent
