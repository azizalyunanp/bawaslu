@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($sejarah as $p)
                                    <form method="post" action="{{ route($action,$id) }}" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Isi</label>
                                            <textarea class="form-control" id="my-editor" name="body"> {!! $p->content !!}  </textarea>
                                        </div>
                                       
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            
                            <div class="col-5 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Tanggal Publikasi</label>
                                            <input type="date" name="date" value="{{ date('Y-m-d',strtotime($p->updated_at)) }}"  class="form-control col-sm-10" >
                                        </div>
                                        <div class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </form>
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection