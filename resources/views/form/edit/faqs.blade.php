@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($cfaqs as $p)
                                    <form method="post" action="{{ route($action,$id) }}" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Judul </label>
                                            <input class="form-control  col-sm-5" type="text"  id="example-text-input" name="title" value="{!! $p->question !!} ">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Isi</label>
                                            <textarea class="form-control" id="my-editor" name="body"> {!! $p->answer !!}  </textarea>
                                        </div>
                                       
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            </form>
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection