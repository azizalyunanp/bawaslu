@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($materi as $p)
                                    <form method="post"  action="{{ route('portal.materi.update',$p->id) }}" enctype="multipart/form-data">  
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Judul Materi</label>
                                            <input class="form-control  col-sm-9" value="{!! $p->nama !!}" type="text" name="title">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Deskripsi</label>
                                            <textarea class="form-control" id="my-editor" name="body">{!! $p->deskripsi !!}</textarea>
                                        </div>
                                        
                                       

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-sm-5 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">File Preview</label> <br>
                                            <a href="{{ asset('uploaded/admin/materi/'.$p->file_upload) }}" target="_blank" class="btn btn-warning"><i class="fa fa-download"></i>Unduh File</a>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">File Upload</label>
                                            <input class="form-control  col-sm-9" type="file"   name="upload"  accept="application/pdf">
                                        </div>
                                    </div>
                                </div>
                            
                            </div>

                            </form>
                            @endforeach
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection