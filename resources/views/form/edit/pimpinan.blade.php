@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-sm-8 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($pimpinan as $p)
                                    @php
                                        $agama = $p->agama
                                    @endphp
                                    <form method="post" action="{{ route($action,$id) }}" enctype="multipart/form-data">  
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nama</label>
                                            <input class="form-control  col-sm-9" type="text"  name="nama" value="{{ $p->nama }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Tempat, Tanggal Lahir</label>
                                            <input class="form-control  col-sm-9" type="text"  name="ttl" value="{{ $p->ttl }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Agama</label>
                                            <select class="form-control col-sm-7" name="agama">
                                                <option {{ ($agama=='Islam') ? 'selected' : '' }}>Islam</option>
                                                <option {{ ($agama=='Kristen') ? 'selected' : '' }}>Kristen</option>
                                                <option {{ ($agama=='Katholik') ? 'selected' : '' }}>Katholik</option>
                                                <option {{ ($agama=='Hindu') ? 'selected' : '' }}>Hindu</option>
                                                <option {{ ($agama=='Budha') ? 'selected' : '' }}>Budha</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Jabatan</label>
                                            <input class="form-control  col-sm-9" type="text"  name="jabatan" value="{{ $p->jabatan }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Kantor</label>
                                            <input class="form-control  col-sm-9" type="text"  name="kantor" value="{{ $p->kantor }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">No Telp/ Fax</label>
                                            <input class="form-control  col-sm-9" type="text"  name="telp" value="{{ $p->telp }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Email</label>
                                            <input class="form-control  col-sm-9" type="text"  name="email" value="{{ $p->email }}">
                                        </div>


                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <img src="{{ asset('uploaded/portal/'.$p->images) }}" width="250"> <br><br>
                                                <label for="example-text-input" class="col-form-label">Foto</label>
                                                <input class="form-control  col-sm-9" type="file"  name="photo">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            </div>

                            </form>
                            @endforeach
                                
                        </div>
                    </div>

            </div>
        <!-- main content area end -->
@endsection