@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-sm-12 mt-4">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                        @if(session('notifikasi'))
                                            <div class="alert alert-success alert-dismissible fade show">
                                               {{ session('notifikasi') }}

                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                                </button>
                                            </div>
                                        @endif

                                    @foreach($caboutus as $p)
                                    <form method="post" action="{{ route($action,$id) }}" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Email</label>
                                            <input class="form-control  col-sm-5" type="email"  id="email" name="email" value="{!! $p->email !!} " >
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Telepon</label>
                                            <input class="form-control  col-sm-5" type="text"  id="telephone" name="telephone" value="{!! $p->telephone !!} "  onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Alamat</label>
                                            <textarea class="form-control" id="alamat" name="alamat" >{!! $p->alamat !!}</textarea>
                                        </div>



                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Deskripsi Singkat</label>
                                            <textarea class="form-control" id="my-editor" name="body" >{!! $p->body !!}</textarea>
                                        </div>
                                       
                                        <div class="form-group">
                                            <!-- <button type="button" class="btn btn-primary" onclick="enabledAbout()"><i class="fa fa-edit"></i> Edit </button> -->
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                            </form>
                            @endforeach
 
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection