@extends('layouts.app2')
@section('content')
@php
    use App\Kecamatan;
    use App\MasterKec;
@endphp
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-7 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                        @if (session('notifikasi'))
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                               
                                                    <li>{{ session('notifikasi') }}</li>
                                               
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    @foreach($users as $p)
                                    <form method="post" action="{{ route($action,$id) }}" enctype="multipart/form-data">
                                    @csrf
                                    {{ method_field('PUT') }}
                                        <h4 class="header-title"> {{ $pages }} </h4>
                                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nip</label>
                                            <input class="form-control  col-sm-5" type="text" value=" {{ $p->nip }}" name="nip" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Nama</label>
                                            <input class="form-control  col-sm-5" type="text" value="{{ $p->name }}"  name="nama">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Email</label>
                                            <input class="form-control  col-sm-5" type="email" value="{{ $p->email }}"   name="email">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Kecamatan</label>
                                            <select class="form-control col-sm-5" name="kecamatan">
                                            <option value="{{ $p->id_kec }}">{{ $p->kecamatan }}</option>
                                                @php 
                                                    $kecamatan = MasterKec::where('id','!=',$p->id_kec)->get();
                                                @endphp

                                                
                                                @foreach($kecamatan as $k)
                                                    <option value="{{ $k->id }}">{{ $k->kecamatan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Status</label>
                                                <select class="form-control col-sm-5" name="active">
                                                    @if($p->active == 'T')
                                                        <option value='T'>AKTIF</option>
                                                        <option value='F'>NONAKTIF</option>
                                                    @else
                                                        <option value='F'>NONAKTIF</option>
                                                        <option value='T'>AKTIF</option>
                                                    @endif
                                                </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Foto Profil</label> <br>
                                            <img src="{{ asset('uploaded/kecamatan/profile/'.$p->images) }}" width="300" accept="image/*">
                                            <br><br>
                                            <input class="form-control col-sm-5" type="file" name="photo">
                                        </div>
                                        
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>          
                            </form>

                            <div class="col-5 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        
                                        <h4>Reset Password</h4>
                                        <form method="post" action="{{ url('password/reset-kecamatan') }}">
                                        @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Email</label>
                                                        <input class="form-control" type="email" name="email" readonly="true" value="{{ $p->email }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn bg-primary-color text-white float-right" type="submit" onclick="return confirm('Apakah anda yakin ingin reset password ??')">Reset Password</button>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>

                            @endforeach
                    </div>

            </div>
        </div>
    </div>
        <!-- main content area end -->
@endsection