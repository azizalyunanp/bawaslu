@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-8 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    <form method="post" action="{{ route($action) }}" enctype="multipart/form-data">  
                                    @csrf
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Judul </label>
                                            <input class="form-control  col-sm-9" type="text" value="" id="example-text-input" name="title">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Isi</label>
                                            <textarea class="form-control" id="my-editor" name="body"></textarea>
                                        </div>
                                        

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-4 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @php
                                        $segment = Request::segment(2);
                                    @endphp 

                                    @if(Request::segment(2) <> 'visimisi')
                                    <div class="form-group">
                                        <label for="example-search-input" class="col-form-label">{{ ($segment == 'bulletin' ) ? 'File Upload' : 'Gambar' }}</label>
                                        <input type="file" class="form-control col-sm-12" name="photo" accept="{{ ($segment == 'bulletin' ) ? 'application/pdf,application/msword' : 'image/*' }}">
                                    </div>
                                    @endif
                                    @if(Request::segment(2) == 'articles')
                                    <div class="form-group">
                                        <label for="example-search-input" class="col-form-label">Kategori</label>
                                        <select class="form-control" name="kategori">
                                            <option value='artikel'>Artikel</option>
                                            <option value='penanganan_pelanggaran'>Putusan - Penanganan Pelanggaran</option>
                                            <option value='penyelesaian_sengketa'>Putusan - Penyelesaian Sengketa</option>
                                            <option value='hasil_pengawasan'>Informasi -  Hasil Pengawasan</option>
                                            <option value='jadwal_tahapan'>Informasi - Jadwal Tahapan</option>
                                        </select>
                                    </div>

                                    @endif
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Tanggal Publikasi </label>
                                            <input type="date" name="date"  class="form-control col-sm-12" value="{{ date('Y-m-d',strtotime(now())) }}">
                                        </div>                                
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Tags </label>
                                            <input type="text"   id="input-tags" name="tags">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection