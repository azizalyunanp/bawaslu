@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if(Session::get('notifikasi')) 

                                            <div class="alert alert-success"> {{ Session::get('notifikasi') }}

                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                                </button>
                                            </div>
                                        @endif
                                        <h4 class="header-title">{{ $breadcrumb }} </h4>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-10 offset-sm-1">
                                                <h2 class="page-heading">Upload your Images <span id="counter"></span></h2>
                                                <form method="post" action="{{ url('portal/galeri/upload') }}"
                                                    enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="dz-message">
                                                        <div class="col-xs-8">
                                                            <div class="message">
                                                                <p>Drop files here or Click to Upload</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fallback">
                                                        <input type="file" name="file" multiple>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    
                                            {{--Dropzone Preview Template--}}
                                            <div id="preview" style="display: none;">
                                        
                                                <div class="dz-preview dz-file-preview">
                                                    <div class="dz-image"><img data-dz-thumbnail /></div>
                                        
                                                    <div class="dz-details">
                                                        <div class="dz-size"><span data-dz-size></span></div>
                                                        <div class="dz-filename"><span data-dz-name></span></div>
                                                    </div>
                                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                        
                                                </div>
                                            </div>
                                            {{--End of Dropzone Preview Template--}}

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Textual inputs end -->
                                                
                                                
                                                
                                            </div>
                                        </div>

                                    </div>
                                    </div>
        <!-- main content area end -->
@endsection