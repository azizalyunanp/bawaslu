@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-8 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    <form method="post" action="{{ route($action) }}" enctype="multipart/form-data">  
                                    @csrf
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nama</label>
                                            <input class="form-control  col-sm-9" type="text"  name="nama">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Tempat, Tanggal Lahir</label>
                                            <input class="form-control  col-sm-9" type="text"  name="ttl">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Agama</label>
                                            <select class="form-control col-sm-7" name="agama">
                                                <option>Islam</option>
                                                <option>Kristen</option>
                                                <option>Katholik</option>
                                                <option>Hindu</option>
                                                <option>Budha</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Jabatan</label>
                                            <input class="form-control  col-sm-9" type="text"  name="jabatan">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Kantor</label>
                                            <input class="form-control  col-sm-9" type="text"  name="kantor">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">No Telp/ Fax</label>
                                            <input class="form-control  col-sm-9" type="text"  name="telp">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Email</label>
                                            <input class="form-control  col-sm-9" type="text"  name="email">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Foto</label>
                                            <input class="form-control  col-sm-9" type="file"  name="photo">
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>

                            </form>
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection