@extends('layouts.app2')
@section('content')
 <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-12">
                                <div class="card">
                                    <div class="card-body">
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                        @if (session('notifikasi'))
                                        <div class="alert alert-danger alert-dismissible fade show">
                                            <ul>
                                               
                                                    <li>{{ session('notifikasi') }}</li>
                                               
                                            </ul>

                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
                                        @endif

                                    <form method="post" action="{{ route($action) }}" enctype="multipart/form-data">
                                    @csrf
                                        <h4 class="header-title"> {{ $pages }} </h4>
                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nip</label>
                                            <input class="form-control  col-sm-5" type="text"   name="nip">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Nama</label>
                                            <input class="form-control  col-sm-5" type="text"   name="nama">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Email</label>
                                            <input class="form-control  col-sm-5" type="email"   name="email">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Role</label>
                                            <select class="form-control col-sm-5" name="role">
                                                <option value="superadmin">SUPER ADMIN</option>
                                                <option value="komisioner">KOMISIONER</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Foto Profil</label>
                                            <input class="form-control  col-sm-5" type="file" name="photo">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Password</label>
                                            <input class="form-control  col-sm-5" type="password"   name="password">
                                        </div>

                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Confirm Password</label>
                                            <input class="form-control  col-sm-5" type="password" name="password_confirmation">
                                        </div>
                                        

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Post</button>
                                            <button type="reset" class="btn btn-danger"><i class="fa fa-close"></i> Reset</button>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            
                            </form>
                                
                        </div>
                    </div>

            </div>
        </div>
        <!-- main content area end -->
@endsection