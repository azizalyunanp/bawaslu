<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Badan Pengawas Pemilu - Klaten</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="http://jateng.bawaslu.go.id/wp-content/uploads/2018/03/logo-bawaslu.png" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css') }}">
 
    <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default-css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/selectize.css') }}">

    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom-dropzone.css') }}">
    <link rel="stylesheet" href="maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <noscript>
        <meta http-equiv='refresh' content="0;/noscript">
    
    </noscript>
    
<style type="text/css">
    /*body{
        height: 100%;
        position: relative;
    }*/
    .main-content{
        min-height: 100vh!important;
        height: 100%!important;
        width: 100%;
        position: relative;
    }
    footer{
        bottom: 0!important;
        position: absolute!important;
        width: 100%;
        width: -moz-available!important;
        width: -webkit-fill-available!important;
    }
</style>
</head>

<body>
 
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header bg-primary-color">
                <div class="logo">
                    <a href="#"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            @if(Auth::guard('admin')->user()->roles == 'komisioner')

                            <li>
                                <a href="{{ URL('portal/materi') }}"><i class="fa fa-cube"></i><span>Materi
                                </span></a>

                            </li>

                            <li>
                            <a   href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Log Out</a>

                            </li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            
                            
                            @else

                            <li>
                                <a href="#"><i class="fa fa-cube"></i><span>Portal
                                </span></a>
                                
                                <ul class="collapse">
                                    <li>
                                        <a href="{{ url('portal/articles') }}"><i class="fa fa-newspaper-o"></i><span>Berita & Artikel</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/bulletin') }}"><i class="fa fa-bell"></i><span>Pengumuman</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/agenda') }}"><i class="fa fa-calendar"></i><span>Agenda</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/pimpinan') }}"><i class="fa fa-users"></i><span>Profil Pimpinan</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/tugas') }}"><i class="fa fa-book"></i><span>Tugas & Wewenang</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/faqs') }}"><i class="fa fa-book"></i><span>FAQ</span></a>
                                    </li>
                                     
                                    <li>
                                        <a href="{{ url('portal/visimisi') }}"><i class="fa fa-cogs"></i><span>Visi & Misi</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/sejarah') }}"><i class="fa fa-cogs"></i><span>Sejarah</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/video') }}"><i class="fa fa-camera"></i><span>Iklan Pengawasan</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('portal/aboutus') }}"><i class="fa fa-users"></i><span>About Us</span></a>
                                    </li>
                                </ul>
                            </li>   

                            

                            <li>
                                <a href="#"><i class="fa fa-cube"></i><span>Inbox
                                </span></a>

                                <ul class="collapse">
                                    <li>
                                        <a href="{{ URL('portal/inbox/laporanformb1') }}"><i class="fa fa-cube"></i><span>Lap Form B1 Masyarakat 
                                       </span></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL('portal/inbox/laporanforma') }}"><i class="fa  fa-cube"></i><span>Lap Form A Kecamatan
                                        
                                        </span></a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="{{ URL('portal/materi') }}"><i class="fa fa-cube"></i><span>Materi
                                </span></a>

                            </li>

                            <li>
                                <a href="{{ URL('portal/masterkec') }}"><i class="fa fa-cube"></i><span>Master Kecamatan
                                </span></a>

                            </li>

                            <li>
                                <a href="{{ URL('portal/materi') }}"><i class="fa fa-cube"></i><span>Rekap Laporan
                                </span></a>

                                <ul class="collapse">
                                    <li>
                                        <a href="{{ URL('portal/rekap/laporanformb1') }}"><i class="fa fa-cube"></i><span>Rekap Laporan B1 Masyarakat</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL('portal/rekap/laporanforma') }}"><i class="fa  fa-cube"></i><span>Rekap Laporan A Kecamatan</span></a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="fa fa-users"></i><span>Manajemen Users
                                </span></a>

                                <ul class="collapse">
                                    <li>
                                        <a href="{{ URL('portal/user-masyarakat') }}"><i class="fa fa-cube"></i><span>User Masyarakat</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL('portal/user-kecamatan') }}"><i class="fa  fa-cube"></i><span>User Kecamatan</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL('portal/user-panel') }}"><i class="fa  fa-cube"></i><span>User Panel</span></a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                            <a   href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Log Out</a>

                            </li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-6 col-sm-4 clearfix">
                        <ul class="notification-area pull-right">
                            <li id="full-view"><i class="ti-fullscreen"></i></li>
                            <li id="full-view-exit"><i class="ti-zoom-out"></i></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">{{ $breadcrumb }}</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="#">Home</a></li>
                                <li><span>{{ $breadcrumb }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <img class="avatar user-thumb" src="{{ asset('images/author/avatar.png') }}" alt="avatar">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{  Auth::guard('admin')->user()->name }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        <br>
        @yield('content')
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <p>© Copyright {{ date('Y') }}. All right reserved </p>
            </div>
        </footer>
        <!-- footer area end-->
    </div>
    <!-- jquery latest version -->
    <script src="{{ asset('jsfile/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('jsfile/popper.min.js') }}"></script>
    <script src="{{ asset('jsfile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('jsfile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('jsfile/metisMenu.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slicknav.min.js') }}"></script>
    <!-- others plugins -->
    <script src="{{ asset('jsfile/plugins.js') }}"></script>
    <script src="{{ asset('jsfile/scripts.js') }}"></script>
    <script src="{{ asset('jsfile/standalone/selectize.js') }}"></script>
    <script src="{{ asset('jsfile/dropzone.js') }}"></script>

    <script src="{{ asset('jsfile/dropzone-config.js') }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=e1kort2kvblb0o4oyltzxateksio139hvt4cmgjch0hg2ot1"></script> 
    <script>
    var editor_config = {
    path_absolute : "{{ asset('') }}",
    selector: "#my-editor",
    height: "500",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
    function get_editor_content() {
        var selectContent = tinymce.get('textarea').getContent();
    }
</script>

    <script>
    $(document).ready(function() {
      $.ajaxSetup({
        headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('#input-tags').selectize({
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

        $('#form-2').hide();
        $('#form-3').hide();
        $('#form-4').hide();

        $('input[name="theName"]').change(function(){
            if($('#cR1').prop('checked')){
                $('#form-1').show();
                $('#form-2').hide();
                $('#form-3').hide();
                $('#form-4').hide();
            } else if($('#cR2').prop('checked')){
                $('#form-1').hide();
                $('#form-2').show();
                $('#form-3').hide();
                $('#form-4').hide();
            } else if($('#cR3').prop('checked')) {
                $('#form-1').hide();
                $('#form-2').hide();
                $('#form-3').show();
                $('#form-4').hide();
            } else {
                $('#form-1').hide();
                $('#form-2').hide();
                $('#form-3').hide();
                $('#form-4').show();
            }
        });
    })

    function enabledAbout() { 
        $("#email").prop('disabled', false);
        $("#telephone").prop('disabled', false);
        $("#alamat").prop('disabled', false);
        $("#deskripsii").prop('disabled', false);
        $("#youtube").prop('disabled', false);
    }

    </script>
</body>

</html>
