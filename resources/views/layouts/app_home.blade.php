<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Badan Pengawas Pemilihan Umum Kabupaten Klaten</title>
    <!-- Favicon icon -->
    <link rel="shortcut icon" type="image/png" href="http://jateng.bawaslu.go.id/wp-content/uploads/2018/03/logo-bawaslu.png" />
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,400,500,700" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{ asset('css/home/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Fontawsome -->
    <link href="{{ asset('css/font-awesome47.min.css') }}" rel="stylesheet">
    <!-- Animate CSS-->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <!-- Preloader CSS-->
    <link href="{{ asset('css/fakeLoader.css') }}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ asset('css/style_home.css') }}" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <!-- FancyBox -->
 
    <link rel="stylesheet" href="{{ asset('inc/lightbox/css/jquery.fancybox.css') }}" />
    <noscript>
        <meta http-equiv='refresh' content="0;/noscript">
    
    </noscript>
</head>

<body class="online-course-1x">
    <!-- Preloader -->
    <div id="fakeloader"></div>
    <div class="header"></div>
    <div class="edufair-container">
        <!-- Start Main Nav -->
        <div class="edufair-header edufair-course-header">
            <nav class="navbar navbar-default edufair-nav">
                <div class="header-top">
                    <div class="container">
                        <p class="pull-left">Butuh bantuan hubungi kami : <strong>{{ $about_us->telephone }}</strong> atau <strong>{{ $about_us->email }}</strong></p>
                    </div>
                </div>
                <div class="container">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-list fa-2"></i>
                            </button>
                            <a class="navbar-brand edufair-brand edufair-light" href="{{ url('') }}"><img src="{{ asset('img/logo-light.png') }}" alt="Logo"></a>
                            <a class="navbar-brand edufair-brand edufair-dark" href="{{ url('') }}"><img src="{{ asset('img/logo-light.png') }}" alt="Logo"></a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">berita<i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('berita') }}">Berita</a></li>
                                        <li><a href="{{ url('penanganan-pelanggaran')}}">Putusan - Penanganan Pelanggaran</a></li>
                                        <li><a href="{{ url('penyelesaian-sengketa')}}">Putusan - Penyelesaian Sengketa</a></li>
                                        <li><a href="{{ url('hasil-pengawasan') }}">Informasi -  Hasil Pengawasan</a></li>
                                        <li><a href="{{ url('jadwal-tahapan') }}">Informasi - Jadwal Tahapan</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">profil<i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('visi-misi') }}">Visi dan Misi</a></li>
                                        <li><a href="{{ url('tentang-kami') }}">Tentang Kami</a></li>
                                        <li><a href="{{ url('sejarah') }}">Sejarah</a></li>
                                        <li><a href="{{ url('profil-pimpinan') }}">Profil Pimpinan</a></li>
                                        <li><a href="{{ url('tugas-dan-wewenang') }}">Tugas, Wewenang, dan Kewajiban</a></li>
                                    </ul>
                                </li>

                                <li><a href="{{ url('agenda') }}">agenda</a></li>
                                <li><a href="{{ url('pengumuman') }}">pengumuman</a></li>
                                <!-- <li><a href="{{ url('galeri') }}">galeri</a></li> -->
                                <li><a href="{{ url('faq') }}">faqs</a></li>
                                <li><a href="http://jateng.bawaslu.go.id/undang-undang/" target="_blank">Regulasi Pemilu</a></li>
                                <li class="dihp"><a href="{{ url('user') }}" target="_blank" class="ddropdown-toggle" >Laporkan Pelanggaran</a></li>
                                <div class="edufair-cart hidden-xs">
                                    <div class="box">
                                        <i class="fa fa-user-o"></i>
                                        <a href="{{ url('user') }}" target="_blank" class="ddropdown-toggle" >Laporkan Pelanggaran</a>
                                        
                                        <div class="container-2">
                                            <span class="icon"><i class="fa fa-search"></i></span>
                                            <span class="icon"><i class="fa fa-search"></i></span>
                                            <input type="search" id="search" placeholder="Search..." />
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
            </nav>
            <!-- End Main Menu -->
			
			@yield('content')
			
			
			        <!-- Start Footer -->
        <div class="edufair-footer-1x padding-bottom-middle">
            <div class="edufair-footer-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 edufair-footer-subscription">
                        </div>
                        <div class="col-md-5 edufair-footer-address">
                            <a href="#" class=""><img src="{{asset('img/logo-light.png')}}" alt="Logo" class="mb-0"></a>
                            <br>
                            <p class="text-justify mt-10">
                            {{ $about_us->body }}
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h5> Navigasi </h5>
                            <ul>
                                <li><a href="{{ url('') }}"> Home </a></li>
                                <li>
                                    <a href="{{ url('visi-misi') }}"> Visi dan Misi </a>
                                </li>
                                <li>
                                    <a href="{{ url('tentang-kami') }}"> Tentang Kami </a>
                                </li>
                                <li><a href="{{ url('agenda') }}"> Agenda </a></li>
                                <li><a href="{{ url('pengumuman') }}"> Pengumuman </a></li>
                                <li><a href="{{ url('galeri') }}"> Galeri </a></li>
                                <li>
                                    <a href="{{ url('faq') }}"> FAQ </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 edufair-footer-address">
                            <h5> Kontak Kami </h5>
                            <p class="mb-0"> <i class="fa fa-phone"></i>{{ $about_us->telephone }}</p>
                            <p class="mb-0"> <i class="fa fa-envelope"></i>{{ $about_us->email }}</p>
                            <p class="mb-0"> <i class="fa fa-map-marker"></i>{{ $about_us->alamat }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer -->
        <div class="footer-bottom">
            <div class="container">
                <div class="col-md-12">
                    <div class="footer-bootom-center">
                        <p>Copyright © {{ date('Y') }} Bawaslu Kabupaten Klaten</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" ></script>
    <script src="{{ asset('inc/lightbox/js/jquery.fancybox.pack.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('jsfile/home/bootstrap.min.js') }}"></script>
    <!-- Preloader js -->
    <script src="{{ asset('jsfile/fakeLoader.min.js') }}"></script>
    <!-- Masonry Portfolio Script -->
    <script src="{{ asset('jsfile/jquery.mixitup.min.js') }}"></script>    
    <!-- Wow js -->
    <script src="{{ asset('jsfile/wow.min.js') }}"></script>
    <!-- Counter Script -->
    <script src="{{ asset('jsfile/waypoints.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.counterup.min.js') }}"></script>
    <!-- Scroll bottom to top -->
    <script src="{{ asset('jsfile/scrolltopcontrol.js') }}"></script>
    <!-- Stiky menu -->
    <script src="{{ asset('jsfile/jquery.sticky.js') }}"></script>
    <!-- Stiky Sidebar -->
    <script src="{{ asset('jsfile/theia-sticky-sidebar.min.js') }}"></script>
    <!-- Custom script -->
    <script src="{{ asset('jsfile/custom.js') }}"></script>
    <!-- Lightbox js -->
    <!-- <script src="{{ asset('inc/lightbox/jsfile/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('inc/lightbox/jsfile/lightbox.js') }}"></script> -->
    <!-- <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                

            });
        });
    </script> -->

    <script>
    $(document).ready(function () {
        window.scrollTo(0,1.4);
    });
    </script>
</body>

</html>
