<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Badan Pengawas Pemilu - Klaten</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="images/icon/favicon.ico">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css') }}">
        <!-- Favicon icon -->
        <link rel="shortcut icon" type="image/png" href="http://jateng.bawaslu.go.id/wp-content/uploads/2018/03/logo-bawaslu.png" />
    <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default-css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/selectize.css') }}">

    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom-dropzone.css') }}">

    
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css">

    <noscript>
        <meta http-equiv='refresh' content="0;/noscript">
    
    </noscript>

<style type="text/css">
    /*body{
        height: 100%;
        position: relative;
    }*/
    .main-content{
        min-height: 100vh!important;
        height: 100%!important;
        width: 100%;
        position: relative;
    }
    footer{
        bottom: 0!important;
        position: absolute!important;
        width: 100%;
        width: -moz-available!important;
        width: -webkit-fill-available!important;
    }
</style>
</head>

<body>
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header bg-primary-color">
                <div class="logo">
                    <a href="#"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">

                            <li>
                                <a href="{{ URL('') }}" target="_blank"><i class="fa fa-cube"></i><span>Home
                                </span></a>
                            </li>

                            <li>
                                <a href="{{ URL('user/profil') }}"><i class="fa fa-users"></i><span>Profil
                                </span></a>
                            </li>

                            <li>
                                <a href="{{ URL('user/ajukan-laporan') }}"><i class="fa fa-cube"></i><span>Ajukan Laporan Form B1
                                </span></a>

                            </li>

                            <li>
                                <a href="{{ URL('user/histori-laporan') }}"><i class="fa fa-cube"></i><span>History Laporan
                                </span></a>
                            </li>

                            <li>
                            
                            <a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Log Out</a>

                            </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-6 col-sm-4 clearfix">
                        <ul class="notification-area pull-right">
                            <li id="full-view"><i class="ti-fullscreen"></i></li>
                            <li id="full-view-exit"><i class="ti-zoom-out"></i></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- header area end -->

            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">X</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="#">Home</a></li>
                                <li><span>{{ $breadcrumb }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <img class="avatar user-thumb" src="{{ asset('images/author/avatar.png') }}" alt="avatar">
                            <h4 class="user-name dropdown-toggle"  > {{  Auth::guard('masyarakat')->user()->name }} </h4>
                        </div>
                    </div>
                </div>
            </div>
        <br>
        @yield('content')
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <p>© Copyright {{ date('Y') }}. All right reserved </p>
            </div>
        </footer>
        <!-- footer area end-->
    </div>
    <script src="{{ asset('jsfile/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('jsfile/popper.min.js') }}"></script>
    <script src="{{ asset('jsfile/bootstrap.min.js') }}"></script>
    <script src="{{ asset('jsfile/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('jsfile/metisMenu.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('jsfile/jquery.slicknav.min.js') }}"></script>
    <!-- others plugins -->
    <script src="{{ asset('jsfile/plugins.js') }}"></script>
    <script src="{{ asset('jsfile/scripts.js') }}"></script>
    <script src="{{ asset('jsfile/standalone/selectize.js') }}"></script>

    <!-- JQUERY VALIDATE -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    <!-- JQUERY VALIDATE -->

    <script>
    $(document).ready(function() {
      $.ajaxSetup({
        headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

        $('#form-2').hide();
        $('input[name="theName"]').change(function(){
            if($('#cR1').prop('checked')){
                $('#form-1').show();
                $('#form-2').hide();
                $('#form-3').hide();
            } else if($('#cR2').prop('checked')){
                $('#form-1').hide();
                $('#form-2').show();
                $('#form-3').hide();
            } 
        });

        $("#file_upload").on("change", function() {
            if($("#file_upload")[0].files.length > 3) {
                alert("You can select only 3 files");
                $('#file_upload').val('');
            }  
        });

        for (let i = 0; i < 6; i++) {
            $('#no_hp' + i).bind('copy paste cut',function(e) { 
                e.preventDefault(); //disable cut,copy,paste
                alert('cut,copy & paste options are disabled !!');
            });
            
        }
        

        


    });

    $( "#myform" ).validate({
        rules: {
            field: {
            required: true,
            accept: "image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/x-rar-compressed,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.presentationml.presentation"
            }
        }
    });


    </script>
</body>

</html>
