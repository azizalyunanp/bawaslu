// Bugs
1. Gap space di home berita. *Urgent -> done
2. Email submit masyarakat masih belum muncul Nama. *Urgent -> done
-----------------------------------------------------------------
Kepada # 

Terima kasih telah melaporkan Form B1 kepada Bawaslu Klaten. 
Berikut  nomor laporan anda # bawaslu-klaten-28-04-2019-000001. 

Klik Disini untuk melihat detail laporan anda.

<button>

Tunggu informasi lanjutan dari kami.

Terima Kasih
Bawaslu Klaten
-----------------------------------------------------------------

3. Email disetujui ke masyarakat masih belum muncul nama. *Urgent -> done
-----------------------------------------------------------------
Kepada #

Selamat laporan anda dengan nomor # bawaslu-klaten-28-04-2019-000002 telah disetujui 
oleh admin Bawaslu Klaten. 

Pentunjuk Berikutnya: 
• Bawa Screenshoot dan Nomor Laporan ke kantor Bawaslu Klaten. 
• Sertakan bukti-bukti pendukung seperti gambar atau video kepada 
petugas kami. 

Catatan: 
Alamat Kantor Bawaslu: JL. Bali No 32, Pandanrejo, Klaten Tengah, 
Kabupaten Klaten (57413) 

Terima Kasih,
Klaten Bawaslu
-----------------------------------------------------------------

4. Detail laporan masyarakat setelah di setujui tidak bisa di print. *Urgent -> done

5. Button menu Halaman profile masyarakat masih belum responsive. -> done

6. Ada gap putih di header portal saat pertama kali di load. -> done

7. Rekap laporan di form masyarakat masih ada yang double. *Urgent -> done

8. favicon nya belum muncul.-> done

9. DI halaman materi kecamatan Halaman detail materi belum bisa di buka. -> done

10. submit laporan dari masyarakat, admin panel masih belum mendapat email. *Send notikasi pengajuan laporan kepada semua admin panel. -> done

11. Email submit kecamatan masih belum muncul nama. -> done
-----------------------------------------------------------------
Kepada # 

Terima kasih telah melaporkan Form A kepada Bawaslu Klaten. 
Berikut nomor laporan anda # bawaslu-klaten-28-04-2019-000002 . 

Klik Disini untuk melihat detail laporan anda.

<button>

Tunggu informasi lanjutan dari kami.

Terima Kasih
Bawaslu Klaten
-----------------------------------------------------------------

12. Inbox form laporan kecamatan nama submitter masih belum, keluar.  -> done

13. User reset password masih belum bisa. -> done




// Adjustment
1. Nomor telfon masih bisa huruf di form laporan b1 masyarakat
2. Alert setelah submit laporan masyarakat berbentuk modal saja.
3. Upload bisa pptx, docx, pdf, zip, dan excell. dan erro handling ketika bukan format yang di maksud.
4. Ganti alert di inbox bahwa laporan sudah anda terima atau anda tolak.
5. Tambahan value nomor laporan di detail form A kecamatan.
6. Tambah kolom status di list laporan terakhir kecamatan.
7. Delete filter by keterangan,
8. Filter date pake >= dan <=.
9. Add feature reset password di user kecamatan.
10.Ubah change password di user kecamatan di ganti dari, form menjadi button reset tanpa masukin email, tetapi ada pop up konfirmasi.
11.Ubah change password di user panel di ganti dari, form menjadi button reset tanpa masukin email, tetapi ada pop up konfirmasi.
12. List agenda masih memumculkan semua text.



// Adjusment Email
1. Add Email Kepada user panel yang baru saja di buat.
----------------------------------------------------------------- 
To: User panel yang baru saja di tambahkan
Format:
Kepada #userbaru

Anda diundang untuk menjadi super admin di portal bawaslu klaten,

Berikut username dan password anda:
Username: .....
Password: .....

Klik disini untuk login

<button>

Terima Kasih,
Bawaslu Klaten.
-----------------------------------------------------------------


2. Add Email Kepada user kecamatan setalah laporannya di terima atau di tolak.
- Diterima
-----------------------------------------------------------------
Kepada #

Selamat laporan anda dengan nomor # bawaslu-klaten-28-04-2019-000002 telah disetujui 
oleh admin Bawaslu Klaten. 

Terima Kasih,
Klaten Bawaslu
-----------------------------------------------------------------

- Ditolak
-----------------------------------------------------------------
Kepada #

Mohon maaf laporan anda dengan nomor # bawaslu-klaten-28-04-2019-000002 telah ditolak 
oleh admin Bawaslu Klaten. 

Dengan alasan sebagai berikut:
{REMARKS}

Terima Kasih,
Klaten Bawaslu
-----------------------------------------------------------------

3. Add Email Kepada user kecamatan yang baru saja di buat.
----------------------------------------------------------------- 
To: User kecamatan yang baru saja di tambahkan
Format:
Kepada #userbaru

Anda diundang untuk menjadi user kecamatan di portal bawaslu klaten,

Berikut username dan password anda:
Username: .....
Password: .....

Klik disini untuk login

<button>

Terima Kasih,
Bawaslu Klaten.
-----------------------------------------------------------------




// Tambahan Non Prioritas.
-----------------------------------------------------------------
1. Tugas dan wewenang, masuk di kategori profil. reference: http://jateng.bawaslu.go.id/tugas-wewenang-dan-kewajiban/
2. Laporkan Pelanggaran ganti image dengan size 1170 * 243.
3. Video dibuat menjadi 3 slot
4. Header di buat animasi ngebukaknya pelan".
5. Submit form a kecamatan tambah field attachment *lampiran spt *Surat Perintah Tugas,, berupa pdf.

// Tambahan Spesial Kecamatan 
-----------------------------------------------------------------
1. Tambah Master Data Kecamatan.
2. Tambah field dropdown kecamatan di create user kecamatan.
3. Add column dan FIlter by kecamatan di list form A kecamatan.


// Nice to have
1. Text area dengan autosize.


