<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','PortalController@home');
## NOSCRIPT
Route::get('noscript',function() {
    return view('noscript');
});
################## PORTAL BERITA
Route::get('berita','PortalController@showberita');
Route::get('berita/{pages}','PortalController@berita');
Route::get('pengumuman','PortalController@showpengumuman');
Route::get('pengumuman/{pages}','PortalController@pengumuman');
Route::get('agenda','PortalController@showagenda');
Route::get('agenda/{pages}','PortalController@agenda');
Route::get('visi-misi','PortalController@visi_misi');
Route::get('tentang-kami','PortalController@tentang_kami');
Route::get('faq','PortalController@faqs');
// Route::get('galeri','PortalController@galeri');
Route::redirect('/galeri','/');
Route::get('sejarah','PortalController@sejarah');
Route::get('profil-pimpinan','PortalController@pimpinan');
Route::get('penanganan-pelanggaran','PortalController@penanganan_pelanggaran');
Route::get('penyelesaian-sengketa','PortalController@penyelesaian_sengketa');
Route::get('hasil-pengawasan','PortalController@hasil_pengawasan');
Route::get('jadwal-tahapan','PortalController@jadwal_tahapan');
Route::get('tugas-dan-wewenang','PortalController@tugas');
Auth::routes();

########## LOGIN 
Route::get('/login/portal', 'Auth\LoginController@showadminLogin');
Route::get('/login/kecamatan', 'Auth\LoginController@showkecamatanLogin');
Route::get('/login/masyarakat', 'Auth\LoginController@showmasyarakatLogin');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegister');
Route::get('/register/masyarakat', 'Auth\RegisterController@showMasyarakatRegister');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/masyarakat', 'Auth\LoginController@masyarakatLogin');
Route::post('/login/kecamatan', 'Auth\LoginController@kecamatanLogin');
Route::post('/register/admin', 'Auth\RegisterController@create');
// Route::post('/register/masyarakat', 'Auth\RegisterController@createMasyarakat');
Route::get('/home', 'HomeController@index')->name('home');


################ ADMIN PORTAL 
Route::get('portal','ArticlesController@index')->middleware('assign.guard:admin,login/portal');
Route::resource('portal/articles', 'ArticlesController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/agenda', 'AgendaController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/bulletin', 'BulletinController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/faqs', 'FaqsController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/visimisi', 'VisimisiController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/sejarah', 'SejarahController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/masterkec', 'MasterKecController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/aboutus', 'AboutusController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/materi', 'MateriController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/video', 'VideoController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/tugas', 'TugasController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/user-kecamatan','UsersKController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/pimpinan','PimpinanController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::post('portal/user-kecamatan/ganti-password/{pages}','UsersKController@change_password')->middleware('assign.guard:admin,login/portal');
Route::post('portal/user-panel/ganti-password/{pages}','UsersPController@change_password')->middleware('assign.guard:admin,login/portal');
Route::resource('portal/user-masyarakat','UsersMController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
Route::resource('portal/user-panel','UsersPController',['as' => 'portal'])->middleware('assign.guard:admin,login/portal');
 

########## LAPORAN FORM B1 MASYARAKAT DAN FORM A KECAMATAN  ADMIN PORTAL
Route::get('portal/rekap/laporanformb1','FormB1Controller@index')->middleware('assign.guard:admin,login/portal');
Route::get('portal/rekap/laporanformb1/filter-all','FormB1Controller@filter')->middleware('assign.guard:admin,login/portal');

// Route::post('portal/rekap/laporanformb1/filter-tanggal','FormB1Controller@tanggal')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanformb1/filter-nolaporan','FormB1Controller@no_laporan')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanformb1/filter-status','FormB1Controller@status')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanformb1/filter-keterangan','FormB1Controller@keterangan')->middleware('assign.guard:admin,login/portal');


Route::get('portal/rekap/laporanforma','FormAController@index')->middleware('assign.guard:admin,login/portal');
Route::get('portal/rekap/laporanforma/filter-all','FormAController@filter')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanforma/filter-tanggal','FormAController@tanggal')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanforma/filter-nolaporan','FormAController@no_laporan')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanforma/filter-status','FormAController@status')->middleware('assign.guard:admin,login/portal');
// Route::post('portal/rekap/laporanforma/filter-kecamatan','FormAController@kecamatan')->middleware('assign.guard:admin,login/portal');

Route::get('portal/inbox/laporanformb1','FormB1Controller@inbox')->middleware('assign.guard:admin,login/portal');
Route::get('portal/detail/laporanformb1/{pages}','FormB1Controller@inbox_detail')->middleware('assign.guard:admin,login/portal');
Route::post('portal/inbox/laporanformb1/filter-status','FormB1Controller@status_inb')->middleware('assign.guard:admin,login/portal');
Route::get('portal/inbox/laporanforma','FormAController@inbox')->middleware('assign.guard:admin,login/portal');
Route::get('portal/detail/laporanforma/{pages}','FormAController@inbox_detail')->middleware('assign.guard:admin,login/portal'); 
Route::post('portal/inbox/laporanforma/filter-status','FormAController@status_inb')->middleware('assign.guard:admin,login/portal');


Route::post('portal/b1/verifikasi','FormB1Controller@verifikasi')->middleware('assign.guard:admin,login/portal');
Route::post('portal/a/verifikasi','FormAController@verifikasi')->middleware('assign.guard:admin,login/portal');
############################ END ADMIN PORTAL

############### KECAMATAN
Route::get('kecamatan','KecamatanController@index');
Route::get('kecamatan/dashboard','KecamatanController@dashboard')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/materi','KecamatanController@materi')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/ajukan-laporan','KecamatanController@ajukan_laporan')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/history-laporan','KecamatanController@history_laporan')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/profil','KecamatanController@profile')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/ajukan-laporan','KecamatanController@upload_forma')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::post('kecamatan/ajukan-laporan','KecamatanController@ajukan_forma')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::post('kecamatan/update','KecamatanController@update')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::post('kecamatan/ganti-password/{pages}','KecamatanController@change_password')->middleware('assign.guard:kecamatan,login/kecamatan');
Route::get('kecamatan/report/{pages}','KecamatanController@report');
Route::get('kecamatan/materi/{pages}','KecamatanController@lihat_materi');

############### USER ROUTE
Route::get('user','MasyarakatController@index');
Route::get('masyarakat','MasyarakatController@index');
Route::get('user/register', 'MasyarakatController@register');
Route::post('register/user','MasyarakatController@store');
Route::get('user/profil','MasyarakatController@profile')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::get('user/ajukan-laporan','MasyarakatController@ajukan_laporan')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::post('user/ajukan-laporan','MasyarakatController@ajukan_form')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::get('user/histori-laporan','MasyarakatController@histori_laporan')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::post('user/update','MasyarakatController@update')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::post('user/ganti-password/{pages}','MasyarakatController@change_password')->middleware('assign.guard:masyarakat,login/masyarakat');
Route::get('user/report/{pages}','MasyarakatController@report');

################ FORGET PASSWORD
Route::get('password/reset-user','ResetPasswordUser@showLinkRequestForm');
Route::post('password/reset-user','ResetPasswordUser@sendPasswordResetToken');
Route::get('user/password-reset/{pages}','ResetPasswordUser@showPasswordResetForm');
Route::post('user/password-reset/{pages}','ResetPasswordUser@resetPassword');

Route::get('password/reset-kecamatan','ResetPasswordKec@showLinkRequestForm');
Route::post('password/reset-kecamatan','ResetPasswordKec@sendPasswordResetToken');
Route::get('kecamatan/password-reset/{pages}','ResetPasswordKec@showPasswordResetForm');
Route::post('kecamatan/password-reset/{pages}','ResetPasswordKec@resetPassword');

Route::get('password/reset-portal','ResetPasswordPortal@showLinkRequestForm');
Route::post('password/reset-portal','ResetPasswordPortal@sendPasswordResetToken');
Route::get('portal/password-reset/{pages}','ResetPasswordPortal@showPasswordResetForm');
Route::post('portal/password-reset/{pages}','ResetPasswordPortal@resetPassword');