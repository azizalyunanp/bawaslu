<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormAsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_as', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_laporan')->nullable();
            $table->string('tahapan')->nullable();
            $table->string('nama_pelaksana')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('no_surat')->nullable();
            $table->string('alamat')->nullable();
            $table->string('bentuk')->nullable();
            $table->string('tujuan')->nullable();
            $table->string('sasaran')->nullable();
            $table->string('waktu')->nullable();
            $table->string('uraian_hasil')->nullable();
            $table->string('peristiwa')->nullable();
            $table->string('saksi')->nullable();
            $table->string('alat_bukti')->nullable();
            $table->string('barang_bukti')->nullable();
            $table->string('uraian_dugaan')->nullable();
            $table->string('fakta')->nullable();
            $table->string('analisa')->nullable();
            $table->integer('users_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_as');
    }
}
