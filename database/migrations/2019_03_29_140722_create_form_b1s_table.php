<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormB1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_b1s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_laporan')->nullable();
            $table->string('nama')->nullable();
            $table->string('no_identitas')->nullable();
            $table->string('jk')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('peristiwa')->nullable();
            $table->string('tempat_kejadian')->nullable();
            $table->string('waktu_kejadian')->nullable();
            $table->string('hari')->nullable();
            $table->string('terlapor')->nullable();
            $table->string('alamat_terlapor')->nullable();
            $table->string('no_telp_terlapor')->nullable();
            $table->string('saksi_nama_1')->nullable();
            $table->string('saksi_alamat_1')->nullable();
            $table->string('saksi_telp_1')->nullable();
            $table->string('saksi_nama_2')->nullable();
            $table->string('saksi_alamat_2')->nullable();
            $table->string('saksi_telp_2')->nullable();
            $table->string('saksi_nama_3')->nullable();
            $table->string('saksi_alamat_3')->nullable();
            $table->string('saksi_telp_3')->nullable();
            $table->string('bukti')->nullable();
            $table->string('dilaporkan_di')->nullable();
            $table->string('dilaporkan_hari')->nullable();
            $table->string('waktu_jam')->nullable();
            $table->integer('users_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_b1s');
    }
}
