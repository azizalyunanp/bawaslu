<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Masyarakat extends Authenticatable
{
    use Notifiable;

    protected $guard = 'masyarakat';

    protected $fillable = [
        'name', 'email', 'password','nik'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
