<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadFoto extends Model
{
    //
    protected $fillable = ['header'];
}
