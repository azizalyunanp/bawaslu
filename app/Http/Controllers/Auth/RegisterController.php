<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
 

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
            $this->middleware('guest:admin');
            $this->middleware('guest:writer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function showadminRegister() {
        return view('auth.register',['url' => 'admin']);
    }

    public function showkecamatanRegister() {
        return view('auth.register',['url' => 'kecamatan']);
    }

    public function showmasyarakatRegister() {
        return view('auth.register',['url' => 'masyarakat']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->validator($request->all())->validate();
        return User::create([
            'nip'       => $data['nip'],
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
            'updated_at'=> date('Y-m-d'),
            'created_at'=> date('Y-m-d')
        ]);
    }

    protected function createMasyarakat(array $data)
    {
        $validator = Validator::make($request->all(),[
            'nik'       => 'required',
            'name'      => 'required',
            'email'     => 'required',
            'password'  => 'required',
            'photo'             => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Masyarakat::where('nik',$data['nik'])->orWhere('email',$data['email'])->count();
            if($count == 0) :
                return Masyarakat::create([
                    'nik'       => $data['nik'],
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'password'  => Hash::make($data['password']),
                    'updated_at'=> date('Y-m-d'),
                    'created_at'=> date('Y-m-d')
                ]);
                return redirect('login/masyarakat')->with([
                    'messages'  => 'User berhasil dibuat . Silahkan login',
                    'alert'     => 'success',
                    
                ]);
            else: 
                return redirect('login/masyarakat')->with([
                    'messages'  => 'NIK / Email sudah pernah dibuat . Silahkan login kembali',
                    'alert'     => 'danger',
                   
                ]);
            endif;
        }
    }

    protected function createKecamatan(array $data)
    {
        $this->validator($request->all())->validate();
        return Kecamatan::create([
            'nip'       => $data['nip'],
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
            'updated_at'=> date('Y-m-d'),
            'created_at'=> date('Y-m-d')
        ]);
    }
}
