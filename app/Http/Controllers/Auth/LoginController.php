<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
 
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:kecamatan')->except('logout');
        $this->middleware('guest:masyarakat')->except('logout');
    }

    public function showadminLogin() {
        return view('auth.login',['url' => 'admin']);
    }

    public function showkecamatanLogin() {
        return view('auth.login',['url' => 'kecamatan']);
    }

    public function showmasyarakatLogin() {
        return view('auth.login',['url' => 'masyarakat']);
    }

    public function adminLogin(Request $request) {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/portal');
        }
        return back()->withInput($request->only('email', 'remember'))->with([
            'messages'  => 'Email / Password tidak cocok',
            'alert'     => 'danger'
        ]);
    }

    public function kecamatanLogin(Request $request) {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('kecamatan')->attempt(['email' => $request->email, 'password' => $request->password])) {

            return redirect()->intended('/kecamatan/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'))->with([
            'messages' => 'Email / Password tidak cocok',
            'alert'     => 'danger'
        ]);
    }

    public function masyarakatLogin(Request $request) {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('masyarakat')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/user');
        }
        return back()->withInput($request->only('email', 'remember'))->with([
            'messages' => 'Email / Password tidak cocok',
            'alert'     => 'danger'
        ]);
    }

    
}
