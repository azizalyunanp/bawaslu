<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visimisi;
use Validator;

class VisimisiController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        return redirect('portal/visimisi/1/edit');
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Visi Misi',
            'action'    => 'portal.visimisi.store',
            'breadcrumb'=> 'Visi Misi'
        ];
        return view('form.add_new')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('portal/visimisi/create')->withErrors($validator)->withInput();
        } else {
            Visimisi::insert([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/visimisi')->with('Sukses Menambah Data','notifikasi');
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Visi Misi',
            'id'        => $id,
            'action'    => 'portal.visimisi.update',
            'breadcrumb'=> 'Visi Misi',
            'cvisimisi'    => Visimisi::where('id',$id)->get()
        ];
        return view('form.edit.visimisi')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('visimisi/create')->withErrors($validator)->withInput();
        } else {

            Visimisi::where('id',$id)->update([
                'title'         => $request->title,
                'content'       => $request->body,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/visimisi')->with('notifikasi','Sukses Menambah Data');
        }
    }

  
}
