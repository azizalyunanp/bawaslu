<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormA;
use App\AttachmentFormA;
use App\Kecamatan;
use App\Materi;
use App\FileUploadFormA;
use App\AttachScanFormA;
use App\User;
use Illuminate\Support\Facades\Auth;
use Hash;
use Validator;
use Mail;
use App\Mail\FormKecamatan;
use App\Mail\FormKecamatanUser;

class KecamatanController extends Controller
{
    private $no_laporan;
    public function __construct()
    {
        $this->upload_path = public_path('/uploaded/kecamatan/laporan');
        
        $this->photos_path = public_path('/uploaded/kecamatan/profile');
        
    }

    function no_laporan() {
        $count = FormA::count();
        if($count == 0) :
            $auto_number    = "bawaslu-klaten-".date('d-m-Y').'-000001';
        else :
            $latest = FormA::latest()->first()->no_laporan;
            $len    = strlen($latest);
            $last   = substr($latest,26,6);
            $auto_number    =  "bawaslu-klaten-".date('d-m-Y').'-'.str_pad($last  + 1, 6, 0, STR_PAD_LEFT);
        endif;

        return $auto_number;
    }
    function check_auth() {
        if(Auth::guard('kecamatan')->check()) :
            return redirect('kecamatan/dashboard');
        endif;
       
    }

    public function index() {
        
        return redirect('kecamatan/dashboard');
    }

    function dashboard() {
        $this->check_auth();
            $data = [
                'breadcrumb'    => 'Beranda',
                'laporan'       => FormA::where('users_id',Auth::guard('kecamatan')->user()->id)->take(5)->orderBy('id','DESC')->get(),
                'materi'        => Materi::take(5)->orderBy('updated_at','DESC')->get(),
                'users'         => Kecamatan::where('id',Auth::guard('kecamatan')->user()->id)->get()
            ];
            
            return view('kecamatan.beranda')->with($data);
    }

    function materi() {
        $this->check_auth();
        $data = [
            'breadcrumb'    => 'List Materi',
            'materi' => Materi::orderBy('updated_at','DESC')->paginate(10)
        ];
        return view('kecamatan.materi')->with($data);
    }

    function lihat_materi($id) {
        $data = [
            'breadcrumb'    => 'List Materi',
            'id'            => $id,
            'materi'        => Materi::where('id',$id)->get(),
            'pages'         => 'List Materi'
        ];
        
        return view('view_materi')->with($data);
    }

    function history_laporan() {
        $this->check_auth();
        $data = [
            'breadcrumb'    => 'List Laporan',
            'form' => FormA::where('users_id',Auth::guard('kecamatan')->user()->id)->orderBy('id','DESC')->paginate(10)
        ];
        return view('kecamatan.histori-laporan')->with($data);
    }

    function upload_forma() {
        $this->check_auth();

        $data = [
            'no_laporan'   => $this->no_laporan(),
            'breadcrumb'    => 'Upload Form A',
            
        ];
        return view('kecamatan.upload_forma')->with($data);
    }


    function ajukan_forma(Request $request) {
        $files      = $request->file('file_upload');
        $attachment = $request->file('attachment');
        $scan       = $request->file('scan');

        ######################### UPLOAD LAMPIRAN MULTIPLE
        $c_upload   = count((array)$files);

        $exist      = FileUploadFormA::where('no_laporan',$request->no_laporan)->count();
        $exist2     = FormA::where('no_laporan',$this->no_laporan())->count();
        if($exist > 0 || $exist2 > 0): 
            FileUploadFormA::where('no_laporan',$request->no_laporan)->delete();
            FormA::where('no_laporan',$this->no_laporan())->delete();
        endif;

        if($c_upload == 0) {
            
            $filename       = $this->no_laporan().'-1'.$files->getClientOriginalExtension();
                $files->move($this->upload_path, $attachments);
                    FileUploadFormA::insert([
                        'no_laporan'    => $request->no_laporan,
                        'file_upload'   => $filename,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ]);
       
 
        } else { 
             
            $i = 1;
            foreach($files as $file) { 
                $filename   = $this->no_laporan().'-'.$i++.'.'.$file->getClientOriginalExtension();
                $file->move($this->upload_path,$filename);

                FileUploadFormA::insert([
                    'no_laporan'    => $request->no_laporan,
                    'file_upload'   => $filename,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            }
        }

        ############## UPLOAD MULTIPLE ATTACHMENT LAPORAN SURAT TUGAS

        $c_attach   = count((array)$attachment);

        if($c_attach == 0) {
            $filename       = $this->no_laporan().'-1'.$attachment->getClientOriginalExtension();
                $files->move($this->upload_path, $filename);

                AttachmentFormA::insert([
                    'no_laporan'    => $request->no_laporan,
                    'file_upload'   => $filename,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);

        } else { 
             
            $i = 1;
            foreach($attachment as $file) { 
                $filename   = $this->no_laporan().'-'.$i++.'.'.$file->getClientOriginalExtension();
                $file->move($this->upload_path,$filename);

                AttachmentFormA::insert([
                    'no_laporan'    => $request->no_laporan,
                    'file_upload'   => $filename,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            }
        }

        ############## UPLOAD MULTIPLE SCAN HARDCOPY

        $c_scan   = count((array)$scan);

        if($c_attach == 0) {
            $filename       = $this->no_laporan().'-1'.$scan->getClientOriginalExtension();
                $files->move($this->upload_path, $filename);

                AttachScanFormA::insert([
                    'no_laporan'    => $request->no_laporan,
                    'file_upload'   => $filename,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);

        } else { 
             
            $i = 1;
            foreach($scan as $file) { 
                $filename   = $this->no_laporan().'-'.$i++.'.'.$file->getClientOriginalExtension();
                $file->move($this->upload_path,$filename);

                AttachScanFormA::insert([
                    'no_laporan'    => $request->no_laporan,
                    'file_upload'   => $filename,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            }
        }


        $data = [
            'no_laporan'    => $request->no_laporan,
            'report_no'     => $request->report_no,
            'tahapan'       => $request->tahapan,
            'nama_pelaksana'=>  $request->nama_pelaksana,
            'jabatan'       =>  $request->jabatan,
            'no_surat'      =>  $request->no_surat,
            'alamat'        =>  $request->alamat,
            'bentuk'        =>  $request->bentuk,
            'tujuan'        =>  $request->tujuan,
            'sasaran'       =>  $request->sasaran,
            'waktu'         =>  $request->waktu,
            'uraian_hasil'  =>  $request->uraian_hasil,
            'peristiwa'     =>  $request->peristiwa,
            'saksi'         =>  $request->saksi,
            'alat_bukti'    =>  $request->alat_bukti,
            'barang_bukti'  =>  $request->barang_bukti,
            'uraian_dugaan' =>  $request->uraian_dugaan,
            'fakta'         =>  $request->fakta,
            'analisa'       =>  $request->analisa,
            'users_id'      => Auth::guard('kecamatan')->user()->id,
            'status'        => 'P',
            'updated_at'    => date('Y-m-d H:i:s'),
            'created_at'    => date('Y-m-d H:i:s')
        ];
       
        Mail::to( Auth::guard('kecamatan')->user()->email)->send(new FormKecamatanUser($this->no_laporan(),
        Auth::guard('kecamatan')->user()->name));

        foreach(User::all() as $user):
            Mail::to($user->email)->send(new FormKecamatan($this->no_laporan()));
        endforeach;

        $saved = FormA::insert($data);
       
        return redirect()->back()->with('notifikasi',"Pengajuan Laporan Berhasil.Silahkan cek spam/inbox di email anda. Terimakasih");
        exit();
    }

    public function forma_tanggal(Request $request) {
        $from    = date('Y-m-d',strtotime($request->from));
        $to      = date('Y-m-d',strtotime($request->to));

        $data = [
            'form'          => FormA::whereBetween('updated_at', [$from, $to])->paginate(10),
            'breadcrumb'    => 'Rekap List Laporan A Kecamatan',
            'notifikasi'    => 'Filter Berdasarkan pada tanggal= '.date('d M Y',strtotime($request->from)).' sampai pada tanggal= '.date('d M Y',strtotime($request->to))
        ];
        return view('kecamatan.form_a')->with($data);
    }

    public function forma_no_laporan(Request $request) {
        $no_laporan = $request->no_laporan;
 

        $data = [
            'form'          => FormA::where('id_laporan',$no_laporan)->paginate(10),
            'breadcrumb'    => 'Rekap List Laporan A Kecamatan',
            'notifikasi'    => 'Filter Berdasarkan no laporan= '. $no_laporan
        ];
        return view('kecamatan.form_a')->with($data);
    }

    function profile() {
        $data = [
            'breadcrumb'    => 'Profil User',
            'users'         => Kecamatan::where('id',Auth::guard('kecamatan')->user()->id)->first()
        ];

        return view('kecamatan.profile')->with($data);
    }

    public function update(Request $request) {
        $photo =  $request->photo;
        $id = Auth::guard('kecamatan')->user()->id;
        if ($request->hasFile('photo')) {
            if ($request->file('photo')->isValid()) {
                $name = sha1(date('YmdHis') . str_random(30));
                $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                $photo->move($this->photos_path, $photos);

                ### GET FIRST FOTO
                $cusersk = Kecamatan::where('id',$id)->first();
                $gfoto = $cusersk->images;
                $uploaded = $this->photos_path.'/'.$gfoto;

                if (file_exists($uploaded)) {
                     
                    unlink($uploaded);
                } else {
                    Kecamatan::where('id', $id)->update([
             
                        'name'          => $request->nama,
                        'images'        =>  $photos,
                        'updated_at'    => date('Y-m-d')
                    ]);
                }
            }

        } else {
            $cusersk = Kecamatan::where('id',  $id)->first();
            $photos = $cusersk->images;

        }
        
        Kecamatan::where('id', $id)->update([
             
            'name'          => $request->nama,
            'images'        =>  $photos,
            'updated_at'    => date('Y-m-d')
        ]);

        return redirect('kecamatan/profil')->with(
            [
                'notifikasi' => 'Sukses Mengupdate Data',
                'alert'      => 'success'
            ]
            
        );
    }

    function change_password(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $old_password   = Kecamatan::where('id',$id)->first()->password;
  
            if (Hash::check($request->password_old, $old_password )) {
                Kecamatan::where('id',$id)->update([
                    'password'  => Hash::make($request->password),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
    
                return redirect()->back()->with([
                    'notifikasi'    => 'Password Berhasil Dirubah',
                    'alert'         => 'success' 
                ]);
            } else {
                return redirect()->back()->with([
                    'notifikasi'    => 'Password lama tidak cocok',
                    'alert'         => 'danger' 
                ]);
            }

        }
    }

    function report($id) {
 
        return view('report.print-forma',[
            'form'      => FormA::where('no_laporan',$id)->first(),
            'lampiran'  => FileUploadFormA::where('no_laporan',$id)->get(),
            'attachment'  => AttachmentFormA::where('no_laporan',$id)->get(),
            'scan'        => AttachScanFormA::where('no_laporan',$id)->get()
        ]);
    }
}
