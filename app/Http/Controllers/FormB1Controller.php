<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormB1;
use App\FileUploadFormB1;
use App\VerifB1;
use Auth;
use Mail;
use App\Mail\FormApprove;
use App\Mail\FormReject;

class FormB1Controller extends Controller
{

 
    public function index()
    {
        $data = [
            'breadcrumb' => 'Rekap List Laporan B1 Masyarakat',
            'form'       => FormB1::select('form_b1s.no_laporan','nik','form_b1s.nama','form_b1s.status',
            'form_b1s.updated_at','masyarakats.nik','keterangan')->join('masyarakats','form_b1s.users_id','=','masyarakats.id')->
            join('verif_b1s','verif_b1s.no_laporan','=','form_b1s.no_laporan', 'left outer')->where('form_b1s.status','!=','P')->orderBy('form_b1s.id','DESC')->paginate(10)
        ];
           
        return view('admin.form_b1')->with($data);
    }

    public function filter(Request $request) {
        #################### GET REQUEST ALL INPUT #######################
        $from    = date('Y-m-d',strtotime($request->from));
        $to      = date('Y-m-d',strtotime($request->to));
        $no_laporan = $request->no_laporan;
        $status = $request->status;
        if($status =='T') {
            $s = 'Diterima';
        } elseif($status== 'F'){
            $s = 'Ditolak';
        } elseif($status== 'P'){ 
            $s = 'Pending';
        } else {
            $s = 'Semua';
        }
        $id = $request->kecamatan;
         
        if($no_laporan == '') :
            $no_laporans = 'Semua No Laporan';
        else: 
            $no_laporans = $request->no_laporan;
        endif;

        $data = [
            'form'          => FormB1::select('form_b1s.no_laporan','nik','form_b1s.nama','form_b1s.status',
            'form_b1s.updated_at','masyarakats.nik','keterangan')->join('masyarakats','form_b1s.users_id','=','masyarakats.id')->
            join('verif_b1s','verif_b1s.no_laporan','=','form_b1s.no_laporan', 'left outer')->where('form_b1s.updated_at','>=',date('Y-m-d',strtotime($from)))->
            where('form_b1s.updated_at','<=',date('Y-m-d',strtotime($to)))->where('form_b1s.no_laporan','like','%'.$no_laporan.'%')->where('form_b1s.status','like','%'.$status.'%')->
            where('form_b1s.status','!=','P')->orderBy('form_b1s.id','DESC')->paginate(10),
            'breadcrumb'    => 'Rekap List Laporan B1 Masyarakat',
            'notifikasi'    => 'Filter Berdasarkan pada tanggal= '.date('d M Y',strtotime($request->from)).' sampai pada tanggal= '.date('d M Y',strtotime($request->to)).' dan
            no laporan= '. $no_laporans . ' dan status= '. $s
        ];
        return view('admin.form_b1')->with($data);
    }


    public function status_inb(Request $request) {
        $status = $request->status;
        if($status =='T') {
            $s = 'Diterima';
        } elseif($status== 'F'){
            $s = 'Ditolak';
        } else {
            $s = 'Pending';
        }
  
        $data = [
            'form'          => FormB1::where('status',$status)->orderBy('id','DESC')->paginate(10),
            'breadcrumb'    => 'Rekap List Laporan B1 Masyarakat',
            'notifikasi'    => 'Filter Berdasarkan status= '. $s 
        ];

        return view('admin.filter_form_b1')->with($data);
    }

    function inbox() {
        $data = [
            'breadcrumb' => 'Inbox List Laporan B1 Masyarakat',
            'form'       => FormB1::select('form_b1s.no_laporan','nik','form_b1s.nama','form_b1s.status',
            'form_b1s.updated_at','masyarakats.nik','keterangan')->join('masyarakats','form_b1s.users_id','=','masyarakats.id')->
            join('verif_b1s','verif_b1s.no_laporan','=','form_b1s.no_laporan', 'left outer')->where('form_b1s.status','P')->orderBy('form_b1s.updated_at','DESC')->paginate(10)
        ];
           
        return view('admin.filter_form_b1')->with($data);
    }

    function inbox_detail($id){
        return view('report.print-form-pelaporan',[
            'form'  => FormB1::join('masyarakats','masyarakats.id','=','form_b1s.users_id')->where('no_laporan',$id)->first(), 
            'lampiran'  => FileUploadFormB1::where('no_laporan',$id)->get()           
        ]
        );
    }

    function verifikasi(Request $request) {
        ######### UPDATE STATUS 

        if($request->status =='T'){
            Mail::to( $request->email )->send(new FormApprove($request->no_laporan,
            $request->name));
        } else  {
            $data = [
                'no_laporan' => $request->no_laporan,
                'name'      => $request->name,
                'keterangan' => $request->keterangan
            ];
            Mail::to( $request->email )->send(new FormReject($data)
        );
        }



        FormB1::where('no_laporan',$request->no_laporan)->update([
            'status'        => $request->status,
            'users_confirm' => Auth::guard('admin')->user()->id,
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        VerifB1::insert([
            'no_laporan'    => $request->no_laporan,
            'status'        => $request->status,
            'keterangan'    => $request->keterangan,
            'users_verif'   => Auth::guard('admin')->user()->id,
            'users_id'      => $request->users_id,
            'updated_at'    => date('Y-m-d H:i:s')
        ]); 

        if($request->status == 'T'):
            $status = 'Diterima';
        else:
            $status = 'Ditolak';
        endif;
        return redirect('portal/rekap/laporanformb1')->with('verifikasi','Laporan '.$request->no_laporan.' telah '.$status);

    }
}
