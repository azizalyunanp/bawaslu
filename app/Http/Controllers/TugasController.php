<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tugas;
use Validator;

class TugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('portal/tugas/1/edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'pages'     => 'Tugas dan Wewenang',
            'id'        => $id,
            'action'    => 'portal.tugas.update',
            'breadcrumb'=> 'Tugas dan Wewenang',
            'tugas'    => Tugas::where('id',$id)->get()
        ];
        return view('form.edit.tugas')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Tugas::where('id',$id)->update([
            'title'   => $request->title,
            'content'   => $request->body,
            'updated_at'=> date('Y-m-d H:i:s'),
            'created_at'=> date('Y-m-d H:i:s')
        ]);

        return redirect()->back()->with([
            'notifikasi' => 'Berhasil merubah data',
            'alert'     => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
