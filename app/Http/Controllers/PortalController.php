<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Articles;
use App\Aboutus;
use App\Bulletin;
use App\Faqs;
use App\Visimisi;
use App\Pengumuman;
use App\Agenda;
use App\Pimpinan;
use App\Video;
use App\Tugas;
class PortalController extends Controller
{

    function getInstagramFeeds($limit = 1){
        try {
            $api_url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=6226852794.1677ed0.99844b251f3044669b1e5d70524f5cb8&count=".$limit;
            $connection_c = curl_init(); // initializing
            curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
            curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // Return the result, do not print
            curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
            $json_return = curl_exec( $connection_c ); // Connect and get json data
            curl_close( $connection_c ); // Close connection
            $insta = json_decode( $json_return ); // Decode and return

            return $insta;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

 

    function content() {

        ############# CURL INSTAGRAM
       

        $data = [
            'about_us'  => Aboutus::first(),
            'agenda'    => Agenda::orderBy('id','DESC')->paginate(5),
            'pengumuman'=> Bulletin::orderBy('id','DESC')->take(3)->paginate(5),
            'faqs'      => Faqs::orderBy('id','DESC')->paginate(5),
            'visimisi'  => Visimisi::orderBy('id','DESC'),
            // 'headfoto'  => HeadFoto::latest()->first(),
            'iklan'     => Video::first(),
            'tugas'     => Tugas::first()
            
        ];
        
        return $data;
    }


    function home() {


        // $content = $this->content();
        $content['about_us']    = Aboutus::first();
        $content['articles']    = Articles::orderBy('id','DESC')->paginate(5);
        $content['agenda']      = Agenda::orderBy('id','DESC')->paginate(5,['*'],'agenda');
        $content['pengumuman']  = Bulletin::orderBy('id','DESC')->paginate(5,['*'],'pengumuman');
        $content['iklan']       = Video::first();
        $content['galeri']      = $this->getInstagramFeeds(8);

        return view('portal.beranda')->with($content);
    }

    function showberita() {
        $content = $this->content();
        $content['title']       = 'Berita';
        $content['articles']    = Articles::where('category','artikel')->orderBy('id','DESC')->paginate(5);
        $content['sidebar']     = Articles::where('category','artikel')->orderBy('id','DESC')->paginate(5);
        return view('portal.portal-berita')->with( $content);
    }

    function berita($link) {
        $content            = $this->content();

        // Get article data
        $articleData = Articles::where('link', $link)->first();

        // Handling result
        if (empty($articleData)) {
            return abort(404);
        }

        // Update view counter
        $articleData->views = $articleData->views + 1;
        $articleData->save();

        // push to collection
        $content['berita']  =  collect([$articleData]);
        $content['sidebar']     = Articles::where('category','artikel')->orderBy('id','DESC')->paginate(5);
        return view('portal.detail-berita', $content );
    }

    function showpengumuman() {
        return view('portal.portal-pengumuman')->with($this->content());
    }

    function pengumuman($link) {
        return view('portal.detail-pengumuman',[
            'pengumuman'    => Bulletin::where('link',$link)->get(),
            'about_us'      => Aboutus::first()
        ]
        );
    }

    function showagenda() {
        return view('portal.portal-agenda')->with($this->content());
    }

    function agenda($link) {
        return view('portal.detail-agenda',[
            'agenda' => Agenda::where('link',$link)->get(),
            'about_us'  => Aboutus::first()
        ]);
    }


    function visi_misi() {
        return view('portal.visi-misi')->with($this->content());
    }

    function tentang_kami() {
        return view('portal.tentang')->with($this->content());
    }

    function faqs() {
        return view('portal.faq')->with($this->content());
    }

    function galeri() {

        return view('portal.galeri',[
            'galeri'    => $this->getInstagramFeeds(80),
            'about_us'  => Aboutus::first()
        ]);
    }

    function sejarah() {
        return view('portal.sejarah')->with($this->content());
    }

    function pimpinan() {
        $data               = $this->content();
        $data['pimpinan']   = Pimpinan::all();
        return view('portal.pimpinan')->with($data);
    }

    function penanganan_pelanggaran() {
        $content = $this->content();
        $content['title']       = 'Penanganan Pelanggaran';
        $content['sidebar']     = Articles::where('category','penanganan_pelanggaran')->orderBy('id','DESC')->paginate(5);
        $content['articles']    = Articles::where('category','penanganan_pelanggaran')->orderBy('id','DESC')->paginate(5);
        return view('portal.portal-berita')->with($content);
    }

    function penyelesaian_sengketa() {
        $content = $this->content();
        $content['title']       = 'Penyelesaian Sengketa';
        $content['sidebar']     = Articles::where('category','penyelesaian_sengketa')->orderBy('id','DESC')->paginate(5);
        $content['articles']    = Articles::where('category','penyelesaian_sengketa')->orderBy('id','DESC')->paginate(5);
        return view('portal.portal-berita')->with($content);
    }

    function hasil_pengawasan() {
        $content = $this->content();
        $content['title']       = 'Hasil Pengawasan';
        $content['sidebar']     = Articles::where('category','hasil_pengawasan')->orderBy('id','DESC')->paginate(5);
        $content['articles']    = Articles::where('category','hasil_pengawasan')->orderBy('id','DESC')->paginate(5);
        return view('portal.portal-berita')->with($content);
    }

    function jadwal_tahapan() {
        $content = $this->content();
        $content['title']       = 'Jadwal Tahapan';
        $content['sidebar']     = Articles::where('category','jadwal_tahapan')->orderBy('id','DESC')->paginate(5);
        $content['articles']    = Articles::where('category','jadwal_tahapan')->orderBy('id','DESC')->paginate(5);
        return view('portal.portal-berita')->with($content);
    }

    function tugas() {
        $content = $this->content();
        return view('portal.tugas')->with($content);
    }


    
}
