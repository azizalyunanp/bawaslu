<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PortalPasswordReset;
use App\User;
use Mail;
use App\Mail\PasswordPortal;
use Hash;
use Auth;
use Validator;

class ResetPasswordPortal extends Controller
{
    public function sendPasswordResetToken(Request $request) {
        
        $user = User::where('email', $request->email)->first();
        if ( !$user ) return redirect()->back()->with('error','Email belum terdaftar');

        $usertoken = PortalPasswordReset::where('email', $request->email);
        if($usertoken->count() > 0) :
            PortalPasswordReset::where('email',$usertoken->first()->email);
        endif;
        //create a new token to be sent to the user. 
        PortalPasswordReset::insert([
            'email' => $request->email,
            'token' => str_random(30), //change 60 to any length you want
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        $tokenData = PortalPasswordReset::where('email', $request->email)->first();
        
        $data = [
            'url'       => url('portal/password-reset/'.$tokenData->token),
            'email'     => $request->email
        ];
        
        Mail::to($tokenData->email)->send(new PasswordPortal($data));

        return redirect()->back()->with([
            'notifikasi' => 'Silahkan cek email / spam anda untuk reset password',
            'alert'     => 'success'
            ]);
      
 
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email_portal');
    }


    public function showPasswordResetForm($token) {
        $tokenData = PortalPasswordReset::where('token', $token)->first();

        if ( !$tokenData ) return redirect()->to('home');  
        return view('auth.passwords.reset_portal')->with([
            'email'     => $tokenData->email
        ]);
    }

    public function resetPassword(Request $request, $token) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $password = $request->password;
            $tokenData =  PortalPasswordReset::where('token', $token)->first();
            $user = User::where('email', $tokenData->email)->first();
            if ( !$user ) return redirect()->to('portal');  
            $user->password = Hash::make($password);
            $user->update();  
            PortalPasswordReset::where('email', $user->email)->delete();
            Auth::guard('admin')->logout();
            return redirect('login/portal')->with([
                'messages' => 'Password berhasil diganti. Silahkan login terlebih dahulu',
                'alert'     => 'success'
            ]);
        }
     
    }
}
