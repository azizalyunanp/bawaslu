<?php
############# USER KECAMATAN ##################
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Kecamatan;
use App\MasterKec;
use Storage;
use Hash;
use Mail;
use App\Mail\RegisterKecamatan;

class UsersKController extends Controller
{
    private $photos_path;

    function __construct() {
        $this->photos_path = public_path('/uploaded/kecamatan/profile');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.user-kecamatan.create',
            'breadcrumb'=> 'User Kecamatan',
            'users'     => Kecamatan::select('kecamatans.*','master_kecs.kecamatan')->join('master_kecs','master_kecs.id','=','kecamatans.id_kec')->orderBy('kecamatans.updated_at','DESC')->paginate(10),
           
        ];
        return view('admin.user_kecamatan')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'User Kecamatan',
            'action'    => 'portal.user-kecamatan.store',
            'breadcrumb'=> 'User Kecamatan',
            'kecamatan' => MasterKec::orderBy('kecamatan','ASC')->get()
        ];
        return view('form.add_usersk')->with($data);
    }

    function auth_user($nip,$email) {
        $users = Kecamatan::where('nip',$nip)->orWhere('email',$email)->count();
        
        return $users;
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'nip'               => 'required',
            'nama'              => 'required',
            'email'             => 'required',
            'password'          => 'required|string|min:6|confirmed',
            'photo'             => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('portal/user-kecamatan/create')->withErrors($validator)->withInput();
        } else {
            
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) {
                    if($this->auth_user($request->nip,$request->email)):
                        return redirect('portal/user-kecamatan/create')->with('notifikasi','NIP atau Email sudah diinput');
                    endif;

                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                    
                    $sends = [
                        'username'  => $request->nama,
                        'password'  => $request->password,
                        'url'       => url('kecamatan')
                    ];
                    Mail::to($request->email)->send(new RegisterKecamatan($sends));

                    Kecamatan::insert([
                        'nip'           => $request->nip,
                        'name'          => $request->nama,
                        'email'         => $request->email,
                        'id_kec'        => $request->kecamatan,
                        'images'        =>  $photos,
                        'active'        => 'T',
                        'password'      => Hash::make($request->password),
                        'updated_at'    => date('Y-m-d')
                    ]); 
                        

                    return redirect('portal/user-kecamatan')->with('notifikasi','Sukses Menambah User');
                }

            } 
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'User Kecamatan',
            'id'        => $id,
            'action'    => 'portal.user-kecamatan.update',
            'breadcrumb'=> 'User Kecamatan',
            'users'     =>  Kecamatan::select('kecamatans.*','master_kecs.kecamatan')->join('master_kecs','master_kecs.id','=','kecamatans.id_kec')->where(['kecamatans.id' => $id])->get(),
        ];
        return view('form.edit.edit_usersk')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           
            'nama'      => 'required',
            'email'     => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('portal/user-kecamatan/'.$id.'/edit')->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                // if($this->auth_user($request->nip,$request->email)):
                //     return redirect('portal/user-kecamatan/'.$id.'/edit')->with('notifikasi','NIP atau Email sudah diinput');
                // endif;


                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);


                    ### GET FIRST FOTO
                    $cusersk = Kecamatan::where('id',$id)->first();
                    $gfoto = $cusersk->images;
                    $uploaded = $this->photos_path.'/'.$gfoto;

                    if (file_exists($uploaded)) {
                        ## HAPUS FOTO JIKA ADA FILE NYA
                        unlink($uploaded);
                    }
                }

            } else {
                $cusersk = Kecamatan::where('id',$id)->first();
                $photos = $cusersk->images;

            }
            
            Kecamatan::where('id',$id)->update([
                 
                'name'          => $request->nama,
                'email'         => $request->email,
                'id_kec'        => $request->kecamatan,
                'images'        =>  $photos,
                'active'        => $request->active,
                'updated_at'    => date('Y-m-d')
            ]);

            return redirect('portal/user-kecamatan')->with('notifikasi','Sukses Mengupdate Data' );
        }
    }

  
    public function destroy($id)
    {

         ### GET FIRST FOTO
         $cusersk = Kecamatan::where('id',$id)->first();
         $gfoto = $cusersk->images;
         $uploaded = $this->photos_path.'/'.$gfoto;

        if (file_exists($uploaded)) {
            ## HAPUS FOTO JIKA ADA FILE NYA
            unlink($uploaded);
        }

        Kecamatan::where('id',$id)->delete();
        
        return redirect('portal/user-kecamatan')->with('notifikasi','Sukses Menghapus Data');
    }

    function change_password(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect('user-kecamatan/'.$id.'/edit')->withErrors($validator)->withInput();
        } else {
             
            Kecamatan::where('id',$id)->update([
                'password' => Hash::make($request->password) 
            ]);

            return redirect('portal/user-kecamatan')->with('notifikasi','Password Berhasil Dirubah');

        }
    }
}
