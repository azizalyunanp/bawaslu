<?php

namespace App\Http\Controllers;
use App\Sejarah;
use Validator;
use Illuminate\Http\Request;

class SejarahController extends Controller
{
    public function index()
    {
        return redirect('portal/sejarah/1/edit');
    }

    public function edit($id)
    {
        $data = [
            'pages'     => 'Sejarah',
            'id'        => $id,
            'action'    => 'portal.sejarah.update',
            'breadcrumb'=> 'Sejarah',
            'sejarah'   => Sejarah::where('id',1)->get()
        ];
        return view('form.edit.sejarah')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'body'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('visimisi/create')->withErrors($validator)->withInput();
        } else {

            Sejarah::where('id',$id)->update([
                'content'       => $request->body,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/sejarah')->with('notifikasi','Sukses Menambah Data');
        }
    }
}
