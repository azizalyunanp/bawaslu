<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Storage;
use Hash;
use Auth;
use Mail;
use App\Mail\RegisterPanel;


class UsersPController extends Controller
{
    private $photos_path;

    function __construct() {
        $this->photos_path = public_path('/uploaded/admin/profile');
    }

    function get_roles() {
        $users = Auth::user()->roles;
        return $users;
    }

    public function index()
    {
        if($this->get_roles() == 'superadmin' || $this->get_roles() == 'developer') {
            $pages = User::where('roles','superadmin')->orWhere('roles','komisioner')->paginate(10);
        } else {
            $pages = User::where('roles','komisioner')->paginate(10);
        }

        $data = [
            'add_url'   => 'portal.user-panel.create',
            'breadcrumb'=> 'User Admin Panel',
            'users'     =>  $pages
        ];
        return view('admin.user_panel')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'User Admin Panel',
            'action'    => 'portal.user-panel.store',
            'breadcrumb'=> 'User Admin Panel'
        ];
        return view('form.add_usersp')->with($data);
    }

    function auth_user($nip,$email) {
        $users = User::where('nip',$nip)->orWhere('email',$email)->count();
        
        return $users;
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'nip'               => 'required',
            'nama'              => 'required',
            'email'             => 'required',
            'password'          => 'required|string|min:6|confirmed',
            'photo'             => 'required',
            'role'              => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('portal/user-panel/create')->withErrors($validator)->withInput();
        } else {
            
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) { 
                    if($this->auth_user($request->nip,$request->email)):
                        return redirect('portal/user-panel/create')->with('notifikasi','NIP atau Email sudah diinput');
                    endif;

                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
  
                    $photo->move($this->photos_path, $photos);
                    
                    $sends = [
                        'email' => $request->email,
                        'username'  => $request->nama,
                        'password'  => $request->password,
                        'url'   => url('portal')

                    ];
                    Mail::to($request->email)->send(new RegisterPanel($sends));
                    
                    User::insert([
                        'nip'           => $request->nip,
                        'name'          => $request->nama,
                        'email'         => $request->email,
                        'images'        =>  $photos,
                        'active'        => 'T',
                        'roles'          => $request->role,
                        'password'      => Hash::make($request->password),
                        'updated_at'    => date('Y-m-d')
                    ]);


    
                    return redirect('portal/user-panel')->with('notifikasi','Sukses Menambah User');
                }

            } 
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {

       

        $data = [
            'pages'     => 'User Admin Panel',
            'id'        => $id,
            'action'    => 'portal.user-panel.update',
            'breadcrumb'=> 'User Admin Panel',
            'users'     => User::where(['id' => $id ])->get()
        ];
        return view('form.edit.edit_usersp')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            
            'nama'      => 'required',
            'email'     => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('portal/user-panel/'.$id.'/edit')->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) {
                    // if($this->auth_user($request->nip,$request->email)):
                    //     return redirect('portal/user-panel/'.$id.'/edit')->with('notifikasi','NIP atau Email sudah diinput');
                    // endif;

                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                    ### GET FIRST FOTO
                    $cusersk = User::where('id',$id)->first();
                    $gfoto = $cusersk->images;
                    $uploaded = $this->photos_path.'/'.$gfoto;

                    if (file_exists($uploaded)) {
                        ## HAPUS FOTO JIKA ADA FILE NYA
                        unlink($uploaded);
                    }
                }

            } else {
                $cusersk = User::where('id',$id)->first();
                $photos = $cusersk->images;

            }
            
            User::where('id',$id)->update([
                
                'name'          => $request->nama,
                'email'         => $request->email,
                'images'        =>  $photos,
                'roles'          => $request->role,
                'active'        => $request->active,
                'updated_at'    => date('Y-m-d')
            ]);

            return redirect('portal/user-panel')->with('notifikasi','Sukses Mengupdate Data' );
        }
    }

  
    public function destroy($id)
    {

         ### GET FIRST FOTO
         $cusersk = User::where('id',$id)->first();
         $gfoto = $cusersk->images;
         $uploaded = $this->photos_path.'/'.$gfoto;

        if (file_exists($uploaded)) {
            ## HAPUS FOTO JIKA ADA FILE NYA
            unlink($uploaded);
        }

        User::where('id',$id)->delete();
        
        return redirect('portal/user-panel')->with('notifikasi','Sukses Menghapus Data');
    }

    function change_password(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect('portal/user-panel/'.$id.'/edit')->withErrors($validator)->withInput();
        } else {
            $users  = User::where('id',$id)->first();

            if (Hash::check($request->password_old,$users->password)) {
                User::where('id',$id)->update([
                    'password' => Hash::make($request->password) 
                ]);
    
                return redirect('portal/user-panel')->with('notifikasi','Password Berhasil Dirubah');
            } else {
                return redirect('portal/user-panel/'.$id.'/edit')->with('notifikasi','Password lama tidak cocok');
            }


            

        }
    }
}
