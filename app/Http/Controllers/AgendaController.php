<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use Validator;

class AgendaController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.agenda.create',
            'breadcrumb'=> 'Halaman',
            'cagenda'    => Agenda::orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate(10)
        ];
        return view('admin.agenda')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Agenda',
            'action'    => 'portal.agenda.store',
            'breadcrumb'=> 'Agenda'
        ];
        return view('form.add_new')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required',
            'photo'     => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
            $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

            $photo->move($this->photos_path, $photos);
  
            Agenda::insert([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'images'        => $photos,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/agenda')->with('notifikasi','Sukses Menambah Data');
                
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Agenda',
            'id'        => $id,
            'action'    => 'portal.agenda.update',
            'breadcrumb'=> 'Agenda',
            'cagenda'    => Agenda::where('id',$id)->get()
        ];
        return view('form.edit.agenda')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('agenda')->back()->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                Validator::make($request->all(), [
                    'photo'     => 'required|mimes:jpeg,bmp,png,jpg'
                ])->validate();
                
                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                }
            } else {
                $photos = Agenda::where('id',$id)->first()->images;
            }

                            
            Agenda::where('id',$id)->update([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'images'        => $photos,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date
            ]);
            return redirect('portal/agenda')->with('notifikasi','Sukses Menghapus Data');
        }
    }

  
    public function destroy($id)
    {
        Agenda::where('id',$id)->delete();
        return redirect('portal/agenda')->with('notifikasi','Sukses Menghapus Data');
    }
}
