<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use Validator;

class ArticlesController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.articles.create',
            'breadcrumb'=> 'Artikel',
            'carticles'    => Articles::orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate(10)
        ];
        return view('admin.articles')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Artikel',
            'action'    => 'portal.articles.store',
            'breadcrumb'=> 'Artikel'
        ];
        return view('form.add_new')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required',
            'photo'     => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
            $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

            $photo->move($this->photos_path, $photos);
  
            Articles::insert([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'images'        => $photos,
                'category'      => $request->kategori,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date,
                'created_at'    => $request->date
            ]);

            return redirect('portal/articles')->with('notifikasi','Sukses Menambah Data');
                
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Artikel',
            'id'        => $id,
            'action'    => 'portal.articles.update',
            'breadcrumb'=> 'Artikel',
            'carticles'    => Articles::where('id',$id)->get()
        ];
        return view('form.edit.articles')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('articles/create')->withErrors($validator)->withInput();
        } else {

            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                Validator::make($request->all(), [
                    'photo'     => 'required|mimes:jpeg,bmp,png,jpg'
                ])->validate();


                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                }
            } else {
                $photos = Articles::where('id',$id)->first()->images;
            }

                            
            Articles::where('id',$id)->update([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'images'        => $photos,
                'category'      => $request->kategori,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date,
                'created_at'    => $request->date
            ]);


            return redirect('portal/articles')->with('notifikasi','Sukses Mengupdate Data');
        }
    }

  
    public function destroy($id)
    {
        Articles::where('id',$id)->delete();
        return redirect('portal/articles')->with('notifikasi','Sukses Menghapus Data');
    }
}
