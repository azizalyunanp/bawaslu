<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materi;
use Auth;
use Validator;

class MateriController extends Controller
{
    function __construct() {
        $this->upload_path = public_path('/uploaded/admin/materi');
    }

    function users_id() {
        return Auth::user()->id;
    }
    public function index()
    {
        $data = [
            'breadcrumb'    => 'List Materi',
            'materi'        => Materi::orderBy('updated_at','DESC')->paginate(10),
            'add_url'       => 'portal.materi.create'
        ];

        return view('admin.materi')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb'    => 'List Materi',
            'action'        => 'portal.materi.store',
            'pages'         => 'Materi'
        ];
        return view('form.add_materi')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!is_dir($this->upload_path )) {
            mkdir($this->upload_path , 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'upload'      => 'required|mimes:jpeg,bmp,png,jpg,doc,docx,zip,rar,ppt,pptx,pdf',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $uploaded = $request->upload;
            if($request->file('upload')->isValid()):
                $upload_name = date('Y-m-d').str_random(10).'.'.$uploaded->getClientOriginalExtension();

                $uploaded->move($this->upload_path,$upload_name);

                Materi::insert([
                    'nama'          => $request->title,
                    'deskripsi'     => $request->body,
                    'file_upload'   => $upload_name,
                    'uploaded_by'   => $this->users_id(),
                    'created_at'    => date('Y-m-d H:i:s') ,
                    'updated_at'    => date('Y-m-d H:i:s') 
                ]);


                return redirect('portal/materi')->with('notifikasi','Materi berhasil di upload');
            endif;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'breadcrumb'    => 'List Materi',
            'id'            => $id,
            'materi'        => Materi::where('id',$id)->get(),
            'pages'         => 'List Materi'
        ];
        
        return view('admin.lihat-materi')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'breadcrumb'    => 'List Materi',
            'id'            => $id,
            'materi'        => Materi::where('id',$id)->get(),
            'action'        => 'portal.materi.update',
            'pages'         => 'List Materi'
        ];
        
        return view('form.edit.materi')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
            $uploaded = $request->upload;
            if ($request->hasFile('upload')) {
                Validator::make($request->all(), [
                    'photo'     => 'required|mimes:jpeg,bmp,png,jpg,doc,docx,zip,rar,ppt,pptx,pdf',
                ])->validate();

                if($request->file('upload')->isValid()):
                    $upload_name = date('Y-m-d').str_random(10).'.'.$uploaded->getClientOriginalExtension();
                    $uploaded->move($this->upload_path,$upload_name);
                     
                endif;
            } else {
                $materi = Materi::where('id',$id)->first()->file_upload;
                $upload_name = $materi;
            }


            Materi::where('id',$id)->update([
                'nama'          => $request->title,
                'deskripsi'     => $request->body,
                'file_upload'   => $upload_name,
                'uploaded_by'   => $this->users_id(),
                'updated_at'    => date('Y-m-d H:i:s') 
            ]);

            return redirect('portal/materi')->with('notifikasi','Materi berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file_upload = Materi::where('id',$id)->first()->file_upload;

        if(file_exists($this->upload_path.'/'.$file_upload)) {
            unlink($this->upload_path.'/'.$file_upload);
        }
        Materi::where('id',$id)->delete();
        
        return redirect()->back()->with('notifikasi','Sukses Menghapus Data');
    }

    public function pdf($id) {
        $data = [
            'materi'    => Materi::where('id',$id)->first()
        ];

        return view('pdf_view')->with($data);
    }
}
