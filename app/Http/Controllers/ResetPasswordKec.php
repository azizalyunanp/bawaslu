<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KecPasswordReset;
use App\Kecamatan;
use Mail;
use App\Mail\PasswordKec;
use Hash;
use Auth;
use Validator;

class ResetPasswordKec extends Controller
{
    public function sendPasswordResetToken(Request $request) {
        
        $user = Kecamatan::where('email', $request->email)->first();
        if ( !$user ) return redirect()->back()->with('error','Email belum terdaftar');

        $usertoken = KecPasswordReset::where('email', $request->email);
        if($usertoken->count() > 0) :
            KecPasswordReset::where('email',$usertoken->first()->email);
        endif;
        //create a new token to be sent to the user. 
        KecPasswordReset::insert([
            'email' => $request->email,
            'token' => str_random(30), //change 60 to any length you want
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        $tokenData = KecPasswordReset::where('email', $request->email)->first();
        
        $data = [
            'url'       => url('kecamatan/password-reset/'.$tokenData->token),
            'email'     => $request->email
        ];
        
        Mail::to($tokenData->email)->send(new PasswordKec($data));

        return redirect()->back()->with([
            'notifikasi' => 'Silahkan cek email / spam anda untuk reset password',
            'alert'     => 'success'
            ]);
      
 
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email_kec');
    }


    public function showPasswordResetForm($token) {
        $tokenData = KecPasswordReset::where('token', $token)->first();

        if ( !$tokenData ) return redirect()->to('home');  
        return view('auth.passwords.reset_kec')->with([
            'email'     => $tokenData->email
        ]);
    }

    public function resetPassword(Request $request, $token) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $password = $request->password;
            $tokenData =  KecPasswordReset::where('token', $token)->first();
            $user = Kecamatan::where('email', $tokenData->email)->first();
            if ( !$user ) return redirect()->to('kecamatan');  
            $user->password = Hash::make($password);
            $user->update();  
            KecPasswordReset::where('email', $user->email)->delete();
            Auth::guard('kecamatan')->logout();
            return redirect('login/kecamatan')->with([
                'messages' => 'Password berhasil diganti. Silahkan login terlebih dahulu',
                'alert'     => 'success'
            ]);
        }
     
    }
}
