<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aboutus;
use App\HeadFoto;
use Validator;

class AboutusController extends Controller
{

    public function __construct()
    {
        $this->photos_path = public_path('/uploaded/header');
    }

    public function index()
    {
        $data = [
            'pages'         => 'Tentang Kami',
            'id'            => Aboutus::first()->id,
            'action'        => 'portal.aboutus.update',
            'breadcrumb'    => 'About us',
            'caboutus'      => Aboutus::all(),
            'headfoto'      => HeadFoto::latest()->first()
        ];
        return view('form.edit.aboutus')->with($data);
    }


    public function update(Request $request, $id){
 
        Aboutus::where('id',$id)->update([
                'email'         => $request->email,
                'telephone'     => $request->telephone,
                'alamat'        => $request->alamat,
                'body'          => $request->body,
                'updated_at'    => date('Y-m-d')
            ]);

            return redirect('portal/aboutus')->with('notifikasi','Sukses Merubah Data');
        }

    function upload_header(Request $request) {
            $photo = $request->file('header');
    
            $name = sha1(date('YmdHis') . str_random(30));
            $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
    
            $photo->move($this->photos_path, $photos);
    
            $count = HeadFoto::count();
    
            if($count == 0):
                HeadFoto::insert([
                    'header' => $resize_name,
                    'updated_at' => date('Y-m-d')
                ]);
            else:
                HeadFoto::where('id',HeadFoto::first()->id)->update([
                    'header' => $resize_name,
                    'updated_at' => date('Y-m-d')
                ]);
            endif;
    
        
            return redirect()->back()->with('notifikasi','Berhasil mengganti header foto');
        }
    
}


