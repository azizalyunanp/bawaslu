<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faqs;
use Validator;


class FaqsController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.faqs.create',
            'breadcrumb'=> 'FAQ',
            'cfaqs'    => Faqs::orderBy('updated_at','DESC')->get()
        ];
        return view('admin.faqs')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'FAQ',
            'action'    => 'portal.faqs.store',
            'breadcrumb'=> 'FAQ'
        ];
        return view('form.add_faqs')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('portal/faqs/create')->withErrors($validator)->withInput();
        } else {
  
                Faqs::insert([
                    'question'    => $request->title,
                    'answer'       => $request->body,
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);

                return redirect('portal/faqs')->with('notifikasi','Sukses Menambah Data');

        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'FAQ',
            'id'        => $id,
            'action'    => 'portal.faqs.update',
            'breadcrumb'=> 'Pengumuman',
            'cfaqs'    => Faqs::where('id',$id)->get()
        ];
        return view('form.edit.faqs')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('faqs/create')->withErrors($validator)->withInput();
        } else {
          
            Faqs::where('id',$id)->update([
                'question'    => $request->title,
                'answer'       => $request->body,
                'updated_at'  => date('Y-m-d H:i:s')
            ]);

            return redirect('portal/faqs')->with('notifikasi','Sukses Menambah Data');
        }
    }

  
    public function destroy($id)
    {
        Faqs::where('id',$id)->delete();
        return redirect('portal/faqs')->with('notifikasi','Sukses Menambah Data');
    }
}
