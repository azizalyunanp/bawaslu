<?php

namespace App\Http\Controllers;
use App\Pimpinan;
use Validator;
use Illuminate\Http\Request;

class PimpinanController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.pimpinan.create',
            'breadcrumb'=> 'Pimpinan',
            'cpimpinan'    =>Pimpinan::orderBy('updated_at','DESC')->paginate(10)
        ];
        return view('admin.pimpinan')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Pimpinan',
            'action'    => 'portal.pimpinan.store',
            'breadcrumb'=> 'Pimpinan'
        ];
        return view('form.add_pimpinan')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'nama'     => 'required',
            'ttl'      => 'required',
            'jabatan'      => 'required',
            'agama'     => 'required',
            'kantor'    => 'required',
            'photo'    => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('portal/pimpinan/create')->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
            $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

            $photo->move($this->photos_path, $photos);
  
           Pimpinan::insert([
                'nama'     => $request->nama,
                'ttl'      => $request->ttl,
                'jabatan'      => $request->jabatan,
                'agama'     => $request->agama,
                'kantor'    => $request->kantor,
                'telp'      => $request->telp,
                'images'        => $photos,
                'email'         => $request->email,
                'created_at'    => date('Y-m-d'),
                'updated_at'    => date('Y-m-d'),
               
            ]);

            return redirect('portal/pimpinan')->with('notifikasi','Sukses Menambah Data');
                
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Pimpinan',
            'id'        => $id,
            'action'    => 'portal.pimpinan.update',
            'breadcrumb'=> 'Pimpinan',
            'pimpinan'    =>Pimpinan::where('id',$id)->get()
        ];
        return view('form.edit.pimpinan')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama'     => 'required',
            'ttl'      => 'required',
            'jabatan'      => 'required',
            'agama'     => 'required',
            'kantor'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('pimpinan/create')->withErrors($validator)->withInput();
        } else {

            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                }
            } else {
                $photos =Pimpinan::where('id',$id)->first()->images;
            }

                            
           Pimpinan::where('id',$id)->update([
            'nama'     => $request->nama,
            'ttl'      => $request->ttl,
            'jabatan'      => $request->jabatan,
            'agama'     => $request->agama,
            'telp'      => $request->telp,
            'kantor'    => $request->kantor,
            'images'        => $photos,
            'email'         => $request->email,
            'created_at'    => date('Y-m-d'),
            'updated_at'    => date('Y-m-d'),
            ]);


            return redirect('portal/pimpinan')->with('notifikasi','Sukses Mengupdate Data');
        }
    }

  
    public function destroy($id)
    {
       Pimpinan::where('id',$id)->delete();
        return redirect('portal/pimpinan')->with('notifikasi','Sukses Menghapus Data');
    }
}
