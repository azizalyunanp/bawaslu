<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormA;
use App\MasterKec;
use App\FileUploadFormA;
use App\AttachmentFormA;
use App\AttachScanFormA;
use App\VerifA;
use Mail;
use App\Mail\ApproveKecamatan;
use App\Mail\RejectKecamatan;
use Validator;
use Auth;
class FormAController extends Controller
{
    public function index()
    {
        $data = [
            'breadcrumb' => 'Rekap List Laporan A Kecamatan',
            'kecamatan'  => MasterKec::orderBy('kecamatan','ASC')->get(),
            'form'       => FormA::select('form_as.no_laporan','nip','form_as.status',
            'form_as.updated_at','kecamatans.nip','kecamatans.name','master_kecs.kecamatan')->join('kecamatans','form_as.users_id','=','kecamatans.id')->join('master_kecs','kecamatans.id_kec',
            '=','master_kecs.id')->where('form_as.status','!=','P')->orderBy('form_as.id','DESC')->paginate(10)
        ];
           
        return view('admin.form_a')->with($data);
    }

    public function filter(Request $request) {
        #################### GET REQUEST ALL INPUT #######################
        $from    = date('Y-m-d',strtotime($request->from));
        $to      = date('Y-m-d',strtotime($request->to));
        $no_laporan = $request->no_laporan;
        $status = $request->status;
        if($status =='T') {
            $s = 'Diterima';
        } elseif($status== 'F'){
            $s = 'Ditolak';
        } elseif($status== 'P'){ 
            $s = 'Pending';
        } else {
            $s = 'Semua';
        }
        $id = $request->kecamatan;
         
        if($no_laporan == '') :
            $no_laporans = 'Semua No Laporan';
        else: 
            $no_laporans = $request->no_laporan;
        endif;
        if($id == '%') :
            $kecamatan = 'Semua Kecamatan';
        else: 
            $kecamatan  = MasterKec::where('id',$id)->first()->kecamatan;
        endif;


        $data = [
            'form'          => FormA::select('form_as.no_laporan','nip','form_as.status',
            'form_as.updated_at','kecamatans.nip','kecamatans.name','master_kecs.kecamatan')->join('kecamatans','form_as.users_id','=','kecamatans.id')->join('master_kecs','kecamatans.id_kec',
            '=','master_kecs.id')->where('form_as.updated_at','>=',date('Y-m-d',strtotime($from)))->where('form_as.updated_at','<=',date('Y-m-d',strtotime($to)))->
            where('form_as.no_laporan','like','%'.$no_laporan.'%')->where('status','like','%'.$status.'%')->where('kecamatans.id_kec','like','%'.$id.'%')->where('status','!=','P')->
            orderBy('form_as.id','DESC')->paginate(10),
            'breadcrumb'    => 'Rekap List Laporan A Kecamatan',
            'kecamatan'  => MasterKec::orderBy('kecamatan','ASC')->get(),
            'notifikasi'    => 'Filter Berdasarkan pada tanggal= '.date('d M Y',strtotime($request->from)).' sampai pada tanggal= '.date('d M Y',strtotime($request->to)).' dan
            no laporan= '. $no_laporans . ' dan status= '. $s.' dan kecamatan= '.  $kecamatan
        ];

       
       return view('admin.form_a')->with($data);
    }



    public function status_inb(Request $request) {
        $status = $request->status;
        if($status =='T') {
            $s = 'Diterima';
        } elseif($status== 'F'){
            $s = 'Ditolak';
        } else {
            $s = 'Pending';
        }
        $data = [
            'form'          => FormA::where('status',$status)->orderBy('id','DESC')->paginate(10),
            'breadcrumb'    => 'Inbox List Laporan A Kecamatan',
            'notifikasi'    => 'Filter Berdasarkan status= '. $s
        ];

        return view('admin.filter_form_a')->with($data);
    }

    function inbox() {
        $data = [
            'breadcrumb' => 'Inbox List Laporan A Kecamatan',
            'kecamatan'  => MasterKec::orderBy('kecamatan','ASC')->get(),
            'form'       => FormA::select('form_as.no_laporan','nip','form_as.status',
            'form_as.updated_at','kecamatans.nip','kecamatans.name','master_kecs.kecamatan')->join('kecamatans','form_as.users_id','=','kecamatans.id')->join('master_kecs','kecamatans.id_kec',
            '=','master_kecs.id')->where('status','P')->orderBy('form_as.updated_at','DESC')->paginate(10)
        ];
           
        return view('admin.filter_form_a')->with($data);
    }

    function inbox_detail($id){
        return view('report.print-forma',[
            'form'  => FormA::where('no_laporan',$id)->first(),     
            'lampiran'  => FileUploadFormA::where('no_laporan',$id)->get(),     
            'attachment'=> AttachmentFormA::where('no_laporan',$id)->get(),
            'scan'        => AttachScanFormA::where('no_laporan',$id)->get()  
        ]
        );
    }

    function verifikasi(Request $request) {
        ######### UPDATE STATUS 
        FormA::where('no_laporan',$request->no_laporan)->update([
            'status'        => $request->verifikasi,
            'users_confirm' => Auth::guard('admin')->user()->id,
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        VerifA::insert([
            'no_laporan'    => $request->no_laporan,
            'status'        => $request->verifikasi,
            'keterangan'=> $request->keterangan,
            'users_verif'   => Auth::guard('admin')->user()->id,
            'users_id'      => $request->users_id,
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
        
        $user = FormA::join('kecamatans','form_as.users_id','=','kecamatans.id')->where('no_laporan',$request->no_laporan)->first();

        $sends = [
            'name'      => $user->name,
            'no_laporan'=> $request->no_laporan,
            'keterangan'=> $request->keterangan
        ];

        if($request->verifikasi == 'T') : 
            $status = 'Diterima';
            Mail::to( $user->email)->send(new ApproveKecamatan($sends));
        else: 
            $status = 'Ditolak';
            Mail::to( $user->email)->send(new RejectKecamatan($sends));
        
        endif;
        return redirect('portal/rekap/laporanforma')->with('verifikasi','No Laporan '.$request->no_laporan.' telah ' .$status);

    }
}
