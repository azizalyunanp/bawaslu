<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormB1;
use App\Masyarakat;
use App\Materi;
use App\FileUploadFormB1;
use Illuminate\Support\Facades\Auth;
use Hash;
use Validator;
use App\Mail\FormMasyarakat;
use App\Mail\FormMasyarakatUser;
use Mail;
use App\User;

class MasyarakatController extends Controller
{
    private $no_laporan;
    public function __construct()
    {
        $this->upload_path = public_path('/uploaded/masyarakat/laporan');
        $this->photos_path = public_path('/uploaded/masyarakat/foto');
        
    }

    public function index() {
        
        return redirect('user/profil');
    }

    function no_laporan() {
        $count = FormB1::count();
        if($count == 0) :
            $auto_number    = "bawaslu-klaten-".date('d-m-Y').'-000001';
        else :
            $latest = FormB1::latest()->first()->no_laporan;
            $len    = strlen($latest);
            $last   = substr($latest,26,6);
            $auto_number    =  "bawaslu-klaten-".date('d-m-Y').'-'.str_pad($last  + 1, 6, 0, STR_PAD_LEFT);
        endif;

        return $auto_number;
    }

    function check_auth() {
        if(Auth::guard('masyarakat')->check()) :
            return redirect('user/profil');
        endif;
       
    }

    function ajukan_form(Request $request) {
        $files     = $request->file('file_upload');
        $c_upload   = count((array)$files);

        $exist      = FileUploadFormB1::where('no_laporan',$request->no_laporan)->count();
        $exist2     = FormB1::where('no_laporan',$this->no_laporan())->count();
        if($exist > 0 || $exist2 > 0): 
            FileUploadFormB1::where('no_laporan',$request->no_laporan)->delete();
            FormB1::where('no_laporan',$this->no_laporan())->delete();
        endif;


        $validator = Validator::make($request->all(), [
            "file_upload"    => "required|array",
            "file_upload.*"  => "required|mimes:jpg,jpeg,png,bmp,ppt,pptx,doc,docx,xls,xlsx,pdf",
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            if($c_upload == 0) {
                
                $filename   = $this->no_laporan().'-1'.$files->getClientOriginalExtension();
                    $files->move($this->upload_path,$filename);

                    
                    FileUploadFormB1::insert([
                        'no_laporan'    => $request->no_laporan,
                        'file_upload'   => $filename,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ]);
            } else { 
                $i = 1;
                foreach($files as $file) { 
                    $filename   = $this->no_laporan().'-'.$i++.'.'.$file->getClientOriginalExtension();
                    $file->move($this->upload_path,$filename);

                    FileUploadFormB1::insert([
                        'no_laporan'    => $request->no_laporan,
                        'file_upload'   => $filename,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ]);
                }
            }

            $data = [
                'no_laporan'    => $request->no_laporan,
                'nama'=> Auth::guard('masyarakat')->user()->name,
                'no_identitas' => Auth::guard('masyarakat')->user()->nik,
                'tempat_lahir'=> $request->tempat_lahir,
                'tanggal_lahir'=> $request->tanggal_lahir,
                'jk'=>$request->jk,
                'pekerjaan'=>$request->pekerjaan,
                'kewarganegaraan'=>$request->kewarganegaraan,
                'alamat'=>$request->alamat,
                'no_telp'=>$request->no_telp,
                'fax'=>$request->fax,
                'email'=>Auth::guard('masyarakat')->user()->email,
                'peristiwa'=>$request->peristiwa,
                'tempat_kejadian'=>$request->tempat_kejadian,
                'waktu_kejadian'=>date('H:i',strtotime($request->waktu_kejadian)),
                'hari'=>$request->hari,
                'terlapor'=>$request->terlapor,
                'alamat_terlapor'=>$request->alamat_terlapor,
                'no_telp_terlapor'=>$request->no_telp_terlapor,
                'saksi_nama_1'=>$request->saksi_nama_1,
                'saksi_alamat_1'=>$request->saksi_alamat_1,
                'saksi_telp_1'=>$request->saksi_telp_1,
                'saksi_nama_2'=>$request->saksi_nama_2,
                'saksi_alamat_2'=>$request->saksi_alamat_2,
                'saksi_telp_2'=>$request->saksi_telp_2,
                'saksi_nama_3'=>$request->saksi_nama_3,
                'saksi_alamat_3'=>$request->saksi_alamat_3,
                'saksi_telp_3'=>$request->saksi_telp_3,
                'bukti_1'=>$request->bukti_1,
                'bukti_2'=>$request->bukti_2,
                'bukti_3'=>$request->bukti_3,
                'bukti_4'=>$request->bukti_4,
                'bukti_5'=>$request->bukti_5,
                'uraian'=>$request->uraian,
                'dilaporkan_di'=>$request->dilaporkan_di,
                'dilaporkan_hari'=>$request->dilaporkan_hari,
                'waktu_jam'=>date('H:i',strtotime($request->waktu_jam)),
                'users_id'=> Auth::guard('masyarakat')->user()->id,
                'status'    => 'P',
                'updated_at'    =>date('Y-m-d H:i:s'),
                'created_at'    =>date('Y-m-d H:i:s')
            ];

            Mail::to( Auth::guard('masyarakat')->user()->email)->send(new FormMasyarakatUser($this->no_laporan(),
            Auth::guard('masyarakat')->user()->name));

            foreach(User::all() as $user):
                Mail::to($user->email)->send(new FormMasyarakat($this->no_laporan()));
            endforeach;

            FormB1::insert($data);

            return redirect()->back()->with('notifikasi',"Pengajuan Laporan Berhasil.Silahkan cek spam/inbox di email anda. Terimakasih");
        }
    }

    function auth_user($nip,$email) {
        $users = Masyarakat::where('nik',$nip)->orWhere('email',$email)->count();
        
        return $users;
    }

    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'nik'               => 'required',
            'nama'              => 'required',
            'email'             => 'required',
            'password'          => 'required|string|min:6|confirmed',
            'photo'             => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) {
                    if($this->auth_user($request->nik,$request->email)):
                        return redirect()->back()->with('notifikasi','NIP atau Email sudah diinput');
                    endif;

                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);

                    Masyarakat::insert([
                        'nik'           => $request->nik,
                        'name'          => $request->nama,
                        'email'         => $request->email,
                        'images'        =>  $photos,
                        'password'      => Hash::make($request->password),
                        'updated_at'    => date('Y-m-d'),
                        'created_at'    => date('Y-m-d')
                    ]);
    
                    return redirect('user')->with('notifikasi','Sukses Menambah User');
                }

            } 
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'User Masyarakat',
            'id'        => $id,
            'action'    => 'portal.user-Masyarakat.update',
            'breadcrumb'=> 'User Masyarakat',
            'users'     => Masyarakat::where(['id' => $id])->get()
        ];
        return view('form.edit.edit_usersk')->with($data);
    }


    public function update(Request $request) {
        $photo =  $request->photo;
        $id = Auth::guard('masyarakat')->user()->id;

        $validator = Validator::make($request->all(), [
            'photo'             => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {


            if ($request->hasFile('photo')) {
                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);

                    ### GET FIRST FOTO
                    $cusersk = Masyarakat::where('id',$id)->first();
                    $gfoto = $cusersk->images;
                    $uploaded = $this->photos_path.'/'.$gfoto;

                    if (file_exists($uploaded)) {
                        
                        unlink($uploaded);
                    } else {
                        Masyarakat::where('id', $id)->update([
                
                            'name'          => $request->nama,
                            'images'        =>  $photos,
                            'updated_at'    => date('Y-m-d')
                        ]);
                    }
                }

            } else {
                $cusersk = Masyarakat::where('id',  $id)->first();
                $photos = $cusersk->images;

            }
        
            Masyarakat::where('id', $id)->update([
                
                'name'          => $request->nama,
                'images'        =>  $photos,
                'updated_at'    => date('Y-m-d')
            ]);

            return redirect('user/profil')->with(
                [
                    'notifikasi' => 'Sukses Mengupdate Data',
                    'alert'      => 'success'
                ]
                
            );
        }
    }

    function change_password(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $old_password   = Masyarakat::where('id',$id)->first()->password;
  
            if (Hash::check($request->password_old, $old_password )) {
                Masyarakat::where('id',$id)->update([
                    'password'  => Hash::make($request->password),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
    
                return redirect()->back()->with([
                    'notifikasi'    => 'Password Berhasil Dirubah',
                    'alert'         => 'success' 
                ]);
            } else {
                return redirect()->back()->with([
                    'notifikasi'    => 'Password lama tidak cocok',
                    'alert'         => 'danger' 
                ]);
            }

        }
    }


    function register() {
        return view('users/register');
    }

    function ajukan_laporan() {
        return view('users/ajukan-laporan',['breadcrumb' => 'Ajukan Laporan', 'no_laporan' => $this->no_laporan()]);
    }

    function profile() {
        $data = [
            'breadcrumb'    => 'Profil',
            'users'         => Masyarakat::where('id',Auth::guard('masyarakat')->user()->id)->first()
        ];
        return view('users/profile')->with($data);
    }


    function histori_laporan() {
        return view('users/histori-laporan',[
            'breadcrumb'    => 'Histori Laporan',
            'form'          => FormB1::where('users_id',Auth::guard('masyarakat')->user()->id)->orderBy('id','DESC')->paginate(10)
        ]
        );
    }


    function report($id) {
        return view('report.print-form-pelaporan',[
            'form'      => FormB1::where('no_laporan',$id)->first(),
            'lampiran'  => FileUploadFormB1::where('no_laporan',$id)->get()
        ]);
    }


}
