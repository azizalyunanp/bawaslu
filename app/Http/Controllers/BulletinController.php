<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bulletin;
use Validator;

class BulletinController extends Controller
{
    function __construct() {
        $this->photos_path = public_path('/uploaded/portal');
    }

    public function index()
    {
        $data = [
            'add_url'   => 'portal.bulletin.create',
            'breadcrumb'=> 'Pengumuman',
            'cbulletin'    => Bulletin::orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate(10)
        ];
        return view('admin.bulletin')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Pengumuman',
            'action'    => 'portal.bulletin.store',
            'breadcrumb'=> 'Pengumuman'
        ];
        return view('form.add_new')->with($data);
    }

  
    public function store(Request $request)
    {
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required',
            'photo'     => 'required|mimes:jpeg,bmp,png,jpg,doc,docx,zip,rar,ppt,pptx,pdf',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            $name = sha1(date('YmdHis') . str_random(30));
            $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
            $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

            $photo->move($this->photos_path, $photos);

            Bulletin::insert([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                 'images'        => $photos,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/bulletin')->with('notifikasi','Sukses Menambah Data');
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Pengumuman',
            'id'        => $id,
            'action'    => 'portal.bulletin.update',
            'breadcrumb'=> 'Pengumuman',
            'cbulletin'    => Bulletin::where('id',$id)->get()
        ];
        return view('form.edit.bulletin')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'body'      => 'required',
            'date'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('bulletin/create')->withErrors($validator)->withInput();
        } else {
            $photo =  $request->photo;
            if ($request->hasFile('photo')) {
                Validator::make($request->all(), [
                    'photo'     => 'required|mimes:jpeg,bmp,png,jpg,doc,docx,zip,rar,ppt,pptx,pdf',
                ])->validate();

                if ($request->file('photo')->isValid()) {
                    $name = sha1(date('YmdHis') . str_random(30));
                    $resize_name  = $name . '.' . $photo->getClientOriginalExtension();
                    $photos = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    $photo->move($this->photos_path, $photos);
                }
            } else {
                $photos = Bulletin::where('id',$id)->first()->images;
            }

            Bulletin::where('id',$id)->update([
                'title'         => $request->title,
                'content'       => $request->body,
                'link'          => date('Y').'-'.date('m').'-'.date('d').'-'.strtolower(str_replace(" ","-",substr($request->title,0,25))),
                'images'        => $photos,
                // 'draft'         => isset($request->draft) ? "1" : "0",
                'tags'          => $request->tags,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/bulletin')->with('notifikasi','Sukses Mengupdate Data');
        }
    }

  
    public function destroy($id)
    {
        Bulletin::where('id',$id)->delete();
        return redirect('portal/bulletin')->with('notifikasi','Sukses Menghapus Data');
    }
}
