<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPasswordReset;
use App\Masyarakat;
use Mail;
use App\Mail\PasswordUser;
use Hash;
use Auth;
use Validator;

class ResetPasswordUser extends Controller
{
    public function sendPasswordResetToken(Request $request) {
        
        $user = Masyarakat::where('email', $request->email)->first();
        if ( !$user ) return redirect()->back()->with('error','Email belum terdaftar');

        $usertoken = UserPasswordReset::where('email', $request->email);
        if($usertoken->count() > 0) :
            UserPasswordReset::where('email',$usertoken->first()->email);
        endif;
        //create a new token to be sent to the user. 
        UserPasswordReset::insert([
            'email' => $request->email,
            'token' => str_random(30), //change 60 to any length you want
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        $tokenData = UserPasswordReset::where('email', $request->email)->first();
        
        $data = [
            'url'       => url('user/password-reset/'.$tokenData->token),
            'email'     => $request->email
        ];
        
        Mail::to($tokenData->email)->send(new PasswordUser($data));

        return redirect()->back()->with([
            'notifikasi'    => 'Silahkan cek email / spam anda untuk reset password',
            'alert'         => 'success'
        ]);
      
 
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email_user');
    }


    public function showPasswordResetForm($token) {
        $tokenData = UserPasswordReset::where('token', $token)->first();

        if ( !$tokenData ) return redirect()->to('home');  
        return view('auth.passwords.reset_user')->with([
            'email'     => $tokenData->email
        ]);
    }

    public function resetPassword(Request $request, $token) {
        $validator = Validator::make($request->all(), [
            
            'password'      => 'required|string|min:6|confirmed',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $password = $request->password;
            $tokenData =  UserPasswordReset::where('token', $token)->first();

            $user = Masyarakat::where('email', $tokenData->email)->first();
            if ( !$user ) return redirect()->to('user');  

            $user->password = Hash::make($password);
            $user->update();  
            UserPasswordReset::where('email', $user->email)->delete();
            Auth::guard('masyarakat')->logout();
            return redirect('login/masyarakat')->with([
                'messages' => 'Password berhasil diganti. Silahkan login terlebih dahulu',
                'alert'     => 'success'
            ]);
        }
     
    }
}
