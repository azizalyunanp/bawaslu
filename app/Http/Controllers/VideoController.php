<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('portal/video/1/edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'pages'     => 'Iklan Pengawasan',
            'id'        => $id,
            'action'    => 'portal.video.update',
            'breadcrumb'=> 'Iklan Pengawasan',
            'video'    => Video::where('id',$id)->get()
        ];
        return view('form.edit.video')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Video::where('id',$id)->update([
            'video_1'   => $request->video_1,
            'video_2'   => $request->video_2,
            'video_3'   => $request->video_3,
            'updated_at'=> date('Y-m-d H:i:s'),
            'created_at'=> date('Y-m-d H:i:s')
        ]);

        return redirect()->back()->with([
            'notifikasi' => 'Berhasil merubah data',
            'alert'     => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
