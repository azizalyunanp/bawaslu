<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterKec;
use Validator;

class MasterKecController extends Controller
{
    public function index()
    {
        $data = [
            'add_url'   => 'portal.masterkec.create',
            'breadcrumb'=> 'Master Kecamatan',
            'master'    => MasterKec::orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate(10)
        ];
        return view('admin.masterkec')->with($data);
    }

 
    public function create()
    {
        //
        $data = [
            'pages'     => 'Master Kecamatan',
            'action'    => 'portal.masterkec.store',
            'breadcrumb'=> 'Master Kecamatan'
        ];
        return view('form.add_masterkec')->with($data);
    }

  
    public function store(Request $request)
    {
   

        $validator = Validator::make($request->all(), [
            'kecamatan'     => 'required'
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            MasterKec::insert([
                'kecamatan'      =>$request->kecamatan,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
                ]);
            switch($request->post) {
                case 'post': 
                    return redirect('portal/masterkec')->with('notifikasi','Sukses Menambah Data');
                break;
                case 'new': 
                    return redirect()->back()->with('notifikasi','Sukses Menambah Data');
                break;
            }
        }
    }

  
    public function show($id)
    {
        //
    }

 
   
    public function edit($id)
    {
        $data = [
            'pages'     => 'Master Kecamatan',
            'id'        => $id,
            'action'    => 'portal.masterkec.update',
            'breadcrumb'=> 'Master Kecamatan',
            'master'    => MasterKec::where('id',$id)->get()
        ];
        return view('form.edit.masterkec')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'kecamatan'     => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('masterkec/create')->withErrors($validator)->withInput();
        } else {
            MasterKec::where('id',$id)->update([
                'kecamatan'      =>$request->kecamatan,
                'updated_at'    => $request->date
            ]);

            return redirect('portal/masterkec')->with('notifikasi','Sukses Mengupdate Data');
        }
    }

  
    public function destroy($id)
    {
        MasterKec::where('id',$id)->delete();
        return redirect('portal/masterkec')->with('notifikasi','Sukses Menghapus Data');
    }
}
