<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class FormMasyarakatUser extends Mailable
{
    use Queueable, SerializesModels;

    public $no_laporan,$name;
    public function __construct($no_laporan,$name)
    {
        $this->no_laporan = $no_laporan;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // $this->withSwiftMessage(function ($message) {
        //     $message->getHeaders()
        //             ->addTextHeader('Custom-Header', 'Laporan Form A Telah Terkirim');
        // });


        return $this->from('no-reply@klaten.bawaslu.go.id')->subject('Laporan Form B1 Telah Terkirim')->markdown('email.form_masyarakat_user')->with([
             
            'no_laporan'    => $this->no_laporan,
            'name'          => $this->name,
            'url'           => url('user/report/'.$this->no_laporan)
        ]);
    }
}
