<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormApprove extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $no_laporan,$name;
    
    public function __construct($no_laporan,$name)
    {
        $this->no_laporan = $no_laporan;
        $this->name     = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@klaten.bawaslu.go.id')->subject('Laporan #'.$this->no_laporan.' Telah Di Setujui')->markdown('email.form_approve')->with([
             
            'no_laporan'    => $this->no_laporan,
            'name'          => $this->name,
             
        ]);
    }
}
