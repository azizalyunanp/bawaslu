<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormReject extends Mailable
{
    use Queueable, SerializesModels;

 
    public function __construct($data)
    {
         $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@klaten.bawaslu.go.id')->subject('Laporan #'.$this->data['no_laporan'].'  Anda Ditolak')->markdown('email.form_reject')->with([
             
            'no_laporan'    => $this->data['no_laporan'],
            'name'          => $this->data['name'],
            'alasan'        => $this->data['keterangan']
             
        ]);
    }
}
