<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormKecamatan extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $no_laporan;
    public function __construct($no_laporan)
    {
        $this->no_laporan = $no_laporan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // $this->withSwiftMessage(function ($message) {
        //     $message->getHeaders()
        //             ->addTextHeader('Custom-Header', 'Laporan Baru Form A oleh Kecamatan');
        // });


        return $this->from('no-reply@klaten.bawaslu.go.id')->subject('Laporan Baru Form A oleh Kecamatan')->markdown('email.form_kecamatan')->with([
             
            'no_laporan'    => $this->no_laporan,
            'url'           => url('kecamatan/report/'.$this->no_laporan)
        ]);

        
    }
}
