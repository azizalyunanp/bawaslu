<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormMasyarakat extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $no_laporan;
    public function __construct($no_laporan)
    {
        $this->no_laporan = $no_laporan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // $this->withSwiftMessage(function ($message) {
        //     $message->getHeaders()
        //             ->addTextHeader('Custom-Header', 'Laporan Baru Form A oleh Kecamatan');
        // });


        return $this->from('no-reply@klaten.bawaslu.go.id')->subject('Laporan Baru Form B1 oleh Masyarakat')->markdown('email.form_masyarakat')->with([
             
            'no_laporan'    => $this->no_laporan,
            'url'           => url('user/report/'.$this->no_laporan)
        ]);

        
    }
}
