<?php

namespace App;

 
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Kecamatan extends Authenticatable
{
    use Notifiable;

    protected $guard = 'kecamatan';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
